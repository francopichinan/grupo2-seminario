package ar.edu.unrn.seminario.modelo;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import ar.edu.unrn.seminario.exception.DataEmptyException;

public class Pedido {
	private Long id;
	private LocalDate fechaDeCreacion;
	private String estado; // Informa si ya la orden del pedido fue generada. ORDEN GENERADA U ORDEN NO
							// GENERADA
	private boolean esCargaPesada;
	private List<Residuo> residuos = new ArrayList<Residuo>();
	private String observacion;
	private Punto punto;
	private final String ORDEN_GENERADA = "ORDEN GENERADA";
	private final String ORDEN_NO_GENERADA = "ORDEN NO GENERADA";

	public Pedido(Long id, boolean cargaPesada, String observacion) {
		this.id = id;
		this.fechaDeCreacion = LocalDate.now();
		this.observacion = observacion;
		this.estado = ORDEN_NO_GENERADA;
		this.esCargaPesada = cargaPesada;
	}

	public Pedido(Long id, LocalDate fecha, boolean cargaPesada, String observacion) {
		this.id = id;
		this.fechaDeCreacion = fecha;
		this.observacion = observacion;
		this.estado = ORDEN_NO_GENERADA;
		this.esCargaPesada = cargaPesada;
	}

	public Pedido(Long id, LocalDate fecha, boolean cargaPesada, String observacion, String estado) {
		this.id = id;
		this.fechaDeCreacion = fecha;
		this.observacion = observacion;
		this.estado = estado;
		this.esCargaPesada = cargaPesada;
	}

	public Pedido(Long id, boolean cargaPesada, String observacion, List<Residuo> residuos, Punto punto) {
		this.id = id;
		this.fechaDeCreacion = LocalDate.now();
		this.observacion = observacion;
		this.estado = ORDEN_NO_GENERADA;
		this.esCargaPesada = cargaPesada;
		this.residuos = residuos;
	}

	public Pedido(boolean cargaPesada, String observacion, Punto punto, List<Residuo> residuos)
			throws DataEmptyException {
		this.fechaDeCreacion = LocalDate.now();
		if (observacion.isEmpty())
			throw new DataEmptyException("Observacion");
		this.observacion = observacion;
		this.estado = ORDEN_NO_GENERADA;
		this.esCargaPesada = cargaPesada;
		this.punto = punto;
		this.residuos = residuos;
	}

	public Pedido(Long id) {
		this.id = id;
	}

	public Pedido(boolean cargaPesada, String observacion, Punto punto) {
		this.esCargaPesada = cargaPesada;
		this.observacion = observacion;
		this.fechaDeCreacion = LocalDate.now();
		this.punto = punto;
		this.estado = this.ORDEN_NO_GENERADA;
	}

	public Pedido(Long id, boolean cargaPesada, String observacion, Punto punto) throws DataEmptyException {
		this.id = id;
		this.fechaDeCreacion = LocalDate.now();
		if (observacion.isEmpty())
			throw new DataEmptyException("Observacion");
		this.observacion = observacion;
		this.estado = ORDEN_NO_GENERADA;
		this.esCargaPesada = cargaPesada;
		this.punto = punto;
		this.residuos = residuos;
	}

	public LocalDate getFechaDeCreacion() {
		return fechaDeCreacion;
	}

	public boolean esCargaPesada() {
		return esCargaPesada;
	}

	public String getObservacion() {
		return observacion;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public void setPunto(Punto punto) {
		this.punto = punto;
	}

	public Punto getPunto() {
		return punto;
	}

	public String getDirecion() {
		return punto.getDireccion();
	}

	public void ordenGenerada() {
		this.estado = ORDEN_GENERADA;
	}

	public String obtenerEstado() {
		return this.estado;
	}

	public List<Residuo> obtenerListaResiduos() {
		return this.residuos;
	}

	public boolean poseeOrdenGenerada() {
		return this.estado.equals(ORDEN_GENERADA);
	}
}
