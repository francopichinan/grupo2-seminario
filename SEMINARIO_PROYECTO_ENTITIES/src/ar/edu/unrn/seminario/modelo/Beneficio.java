package ar.edu.unrn.seminario.modelo;

import ar.edu.unrn.seminario.exception.DataEmptyException;

public class Beneficio {
	private int id_beneficio;
	private String nombre;
	private String descripcion;
	private boolean activado;
	private int costoDelBeneficio;
	
	public Beneficio(int id_beneficio, String unNombre, String unaDescripcion, boolean estado, int unCosto) throws DataEmptyException {
		this.id_beneficio = id_beneficio;
		setNombre(unNombre);
		setDescripcion(unaDescripcion);
		setCostoDelBeneficio(unCosto);
		this.activado = estado;
	}
	
	public Beneficio(int id, String unNombre, String unaDescripcion, int unCosto, boolean activado){
		this.id_beneficio = id;
		this.nombre = unNombre;
		this.descripcion = unaDescripcion;
		this.costoDelBeneficio = unCosto;
		this.activado = activado;
	}
	
	public int devolverIdBeneficio() {
		return this.id_beneficio;
	}
	
	public void cambiarNombre(String nuevoNombre) {
		this.nombre = nuevoNombre;
	}

	public void setId_beneficio(int id_beneficio) {
		this.id_beneficio = id_beneficio;
	}
	
	public int getId_beneficio() {
		return this.id_beneficio;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) throws DataEmptyException {
		if (nombre != null) {
			if (!nombre.isEmpty()) {
				this.nombre = nombre;
			} else {
				throw new DataEmptyException();
			}
		} else {
			throw new DataEmptyException();
		}
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) throws DataEmptyException {
		if (descripcion != null) {
			if (!descripcion.isEmpty()) {
				this.descripcion = descripcion;
			} else {
				throw new DataEmptyException();
			}
		} else {
			throw new DataEmptyException();
		}
	}

	public boolean isActivado() {
		return activado;
	}

	public void setActivado(boolean activado) {
		this.activado = activado;
	}

	public int getCostoDelBeneficio() {
		return costoDelBeneficio;
	}

	public void setCostoDelBeneficio(int costoDelBeneficio) throws DataEmptyException {
		if(costoDelBeneficio > 0) {
			this.costoDelBeneficio = costoDelBeneficio;
		}
		else {
			throw new DataEmptyException();
		}
	}

	@Override
	public String toString() {
		return "Beneficio [id_beneficio=" + id_beneficio + ", nombre=" + nombre + ", descripcion=" + descripcion
				+ ", activado=" + activado + ", costoDelBeneficio=" + costoDelBeneficio + "]";
	}
	
	
}