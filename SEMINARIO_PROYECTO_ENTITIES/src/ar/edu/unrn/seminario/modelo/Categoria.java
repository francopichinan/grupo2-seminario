package ar.edu.unrn.seminario.modelo;

import ar.edu.unrn.seminario.exception.DataEmptyException;

public class Categoria {
	private Long id;
	private String nombre;
	private int puntaje;
	private String tipo;
	private static String RSU;
	private static String RAEE;
	
	
	public Categoria(String unNombre, int unPuntaje) throws DataEmptyException {
		this.asignarNombreCategoria(unNombre);
		this.asignarPuntajeCategoria(unPuntaje);
	}
	
	public Categoria(Long id, String unNombre, int unPuntaje) throws DataEmptyException{
		this.asignarIDCategoria(id);
		this.asignarNombreCategoria(unNombre);
		this.asignarPuntajeCategoria(unPuntaje);
	}
	
	public Categoria(Long id, String nombre, int puntaje, String tipo) {
		this.id = id;
		this.nombre = nombre;
		this.puntaje = puntaje;
		this.tipo = tipo;
	}
	
	public Categoria() {
	}
	
	public void asignarNombreCategoria(String unNombre) throws DataEmptyException {
		dispararException(unNombre);
		this.nombre = unNombre;
	}
	
	public void asignarPuntajeCategoria(int puntaje) throws DataEmptyException {
		dispararException(puntaje);
		
		this.puntaje = puntaje;
	}
	
	public void asignarIDCategoria(Long unID) throws DataEmptyException{
		dispararException(unID);
		this.id = unID;
	}
	
	public String devolverNombreCategoria() {
		return this.nombre;
	}
	
	public int devolverPuntajeCategoria() {
		return this.puntaje;
	}
	
	public Long devolverIdCategoria() {
		return id;
	}
	
	
	public void informacionCategoria() {
		this.toString();
	}
	
	//Excepciones
	
	private void dispararException(String unNombre) throws DataEmptyException {
		if(unNombre == null) {
			throw new DataEmptyException("El campo de nombre se encuentra vacio");
		}
	}
	
	private void dispararException(int unPuntaje) throws DataEmptyException {
		if(unPuntaje == 0) {
			throw new DataEmptyException("No se ingreso un puntaje");
		}
	}
	
	private void dispararException(Long unID) throws DataEmptyException {
		if(unID == null) {
			throw new DataEmptyException ("El campo ID esta vacio");
		}
	}

	public String obtenerTipo() {
		return tipo;
	}
	
}