package ar.edu.unrn.seminario.modelo;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

public class Responsable {
	
	private long id;
	private LocalDate inicio;
	private LocalDate  fin;
	@SuppressWarnings("unused")
	private List<MovimientoPunto> puntosGanados;
	
	PuntoInteligente punto;
	
	public Responsable() {
		this.id = 0L;
	}
	
	public Responsable(LocalDate inicio, LocalDate fin) {
		this.inicio = inicio;
		this.fin = fin;
		this.puntosGanados = new ArrayList<MovimientoPunto>();
	}
	
	public Responsable(long id, LocalDate inicio, LocalDate fin) {
		this(inicio, fin);
		this.id = id;
	}

	public LocalDate getFin() {
		return fin;
	}

	public void setFin(LocalDate fin) {
		this.fin = fin;
	}

	public long getId() {
		return id;
	}

	public LocalDate getInicio() {
		return inicio;
	}

	public PuntoInteligente getPunto() {
		return punto;
	}
	
	//public boolean generarPedido();

}
