package ar.edu.unrn.seminario.modelo;

import java.util.ArrayList;
import java.util.List;

import ar.edu.unrn.seminario.exception.DataEmptyException;
import ar.edu.unrn.seminario.exception.DniInvalidException;

public class Recolector {
	private Long id;
	private String dni;
	private String nombre;
	private String apellido;
	private List<Orden> listaOrdenes;

	public Recolector(Long id, String dni, String nombre, String apellido) throws DataEmptyException, DniInvalidException {
		this.id = id;
		if (nombre.isEmpty()) 
			throw new DataEmptyException("Nombre");
		if (apellido.isEmpty()) 
			throw new DataEmptyException("Apellido");
		if (dni.isEmpty())
			throw new DataEmptyException("Dni");
		if (!dni.matches("[0-9]{8}"))
			throw new DniInvalidException("Debe ingresar 8 digitos");
		this.nombre = nombre;
		this.apellido = apellido;
		this.dni = dni;
	}
	
	public Recolector(String dni, String nombre, String apellido) throws DataEmptyException, DniInvalidException {
		if (nombre.isEmpty()) 
			throw new DataEmptyException("Nombre");
		if (apellido.isEmpty()) 
			throw new DataEmptyException("Apellido");
		if (dni.isEmpty())
			throw new DataEmptyException("Dni");
		if (!dni.matches("[0-9]{8}"))
			throw new DniInvalidException("Debe ingresar 8 digitos");
		this.dni = dni;
		this.nombre = nombre;
		this.apellido = apellido;
		this.listaOrdenes = new ArrayList<>();
	}
	
	public Recolector() {
	}

	public void asignarOrden(Orden orden) {
		this.listaOrdenes.add(orden);
	}
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getApellido() {
		return apellido;
	}

	public void setApellido(String apellido) {
		this.apellido = apellido;
	}

	public String getDni() {
		return dni;
	}

	public void setDni(String dni) {
		this.dni = dni;
	}

	public Integer consultarCantidadDeOrdenes() {
		return 0;
	}

	@Override
	public String toString() {
		return "Recolector [dni=" + dni + ", nombre=" + nombre + ", apellido=" + apellido + "]";
	}
}
