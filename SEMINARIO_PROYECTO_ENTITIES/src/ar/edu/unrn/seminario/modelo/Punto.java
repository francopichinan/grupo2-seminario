package ar.edu.unrn.seminario.modelo;

import java.util.Date;

import javax.swing.Icon;

import ar.edu.unrn.seminario.exception.NotNullException;

public class Punto {

	protected float latitud;
	protected String titulo;
	protected float longitud;
	protected Icon icono;
	protected String descripcion;
	protected Date fecha;
	protected String color;
	protected Boolean estado;
	protected Long id;

	// NUEVOS DATOS
	protected String calle;
	protected Integer altura;
	protected Mapa unMapa;

	public static String PuntoInteligente = "Punto Inteligente";
	public static String PuntoFijo = "Punto Fijo";

	public Punto() {

	}

	// METODO GENERADO POR EL GRUPO 4
	public Punto(/* float latitud, */ String titulo, /* float longitud, */ String calle,
			Integer altura/* , Icon icono */, String descripcion, Date fecha, String color, Boolean estado, Mapa unMapa)
			throws NotNullException {
		if (validacion(/* latitud, */titulo, /* longitud, */calle, altura, icono, descripcion, fecha, color, estado))
			throw new NotNullException("Algun valor nulo");
		else {
//			this.latitud = latitud;
//			this.longitud = longitud;
			this.titulo = titulo;
			this.calle = calle;
			this.altura = altura;
//			this.icono = icono;
			this.descripcion = descripcion;
			this.fecha = fecha;
			this.color = color;
			this.estado = estado;
			this.unMapa = unMapa;
		}
	}

	public Punto(Long id, String titulo, String descripcion, Date fecha, Boolean estado)
			throws NotNullException {
		this.id = id;
		this.titulo = titulo;
		this.descripcion = descripcion;
		this.fecha = fecha;
		this.estado = estado;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getId() {
		return this.id;
	}

	public String getTitulo() {
		return titulo;
	}

//	protected float getLongitud() {
//		return longitud;
//	}

	public Icon getIcono() {
		return icono;
	}

	public String getCalle() {
		return (this.calle);
	}

	public Integer getAltura() {
		return (this.altura);
	}

	public String nombreMapa() {
		return this.unMapa.getNombre();
	}

	public String getDescripcion() {
		return descripcion;
	}

	public Date getFecha() {
		return fecha;
	}

	public String getColor() {
		return color;
	}

	public Boolean getEstado() {
		return estado;
	}

//	protected void setLatitud(float latitud) {
//		this.latitud = latitud;
//	}

	public void setTitulo(String titulo) {
		this.titulo = titulo;
	}

//	protected void setLongitud(float longitud) {
//		this.longitud = longitud;
//	}

	public void setIcono(Icon icono) {
		this.icono = icono;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public void setFecha(Date fecha) {
		this.fecha = fecha;
	}

	public void setColor(String color) {
		this.color = color;
	}

	public void setEstado(Boolean estado) {
		this.estado = estado;
	}

//	protected void setCodigo(int codigo) {
//		this.codigo = codigo;
//	}

	public Boolean equals(String obj) {
		if (this.titulo == obj) {
			return (true);
		} else {
			return (false);
		}
	}

	public Boolean equals(Punto obj) {
		if (this.titulo == obj.titulo) {
			return (true);
		} else {
			return (false);
		}
	}

	////////////////////////////////////////////////// VERIFICACIONES////////////////////////////////////////////////////
	protected Boolean esNull(Object var) {
		if (var == null) {
			return (true);
		} else {
			return (false);
		}
	}

	protected Boolean validacion(/* float latitud */ String titulo, /* float longitud */String calle, Integer altura,
			Icon icono, String descripcion, Date fecha, String color, Boolean estado) {
		if (/* esNull(latitud)&& */(esNull(titulo)) && /* (esNull(longitud))&& */(esNull(calle)) && (esNull(altura))
				&& (esNull(icono)) && (esNull(descripcion)) && (esNull(fecha)) && (esNull(color)) && (esNull(estado))) {
			return (true);
		} else {
			return (false);
		}
	}

	public String getDireccion() {
		return this.unMapa.getNombre() + " - " + this.calle + " : " + this.altura;
	}

	public Mapa getMapa() {
		return unMapa;
	}
}
