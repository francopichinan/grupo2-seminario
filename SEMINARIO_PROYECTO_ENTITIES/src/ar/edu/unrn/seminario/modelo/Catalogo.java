package ar.edu.unrn.seminario.modelo;

import java.util.*;

import ar.edu.unrn.seminario.exception.DataEmptyException;

public class Catalogo {
	private int id_catalogo;
	private String nombre;
	private String temporada;
	private boolean estado;
//	private ArrayList<Beneficio> listaDeBeneficios;

	public Catalogo(int id_catalogo, String nombre, String temporada, boolean estado) throws DataEmptyException {
		this.id_catalogo = id_catalogo;
		setNombre(nombre);
		setTemporada(temporada);
		this.estado = estado;
//		this.listaDeBeneficios = new ArrayList<Beneficio>();
	}

	public int getId_catalogo() {
		return id_catalogo;
	}

	public void setId_catalogo(int id_catalogo) {
		this.id_catalogo = id_catalogo;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) throws DataEmptyException {
		if (nombre != null) {
			if (!nombre.isEmpty()) {
				this.nombre = nombre;
			} else {
				throw new DataEmptyException();
			}
		} else {
			throw new DataEmptyException();
		}
	}

	public String getTemporada() {
		return temporada;
	}

	public void setTemporada(String temporada) throws DataEmptyException {
		if (temporada != null) {
			if (!temporada.isEmpty()) {
				if ((temporada.equals("Invierno") ) || (temporada.equals("Primavera")) || (temporada.equals("Verano"))
						|| (temporada.equals("Otoño")) || (temporada.equals("Anual")) || (temporada.equals("Navidad"))
						|| (temporada.equals("Pascuas"))) {
					this.temporada = temporada;
				}
			} else {
				throw new DataEmptyException();
			}
		} else {
			throw new DataEmptyException();
		}
	}

	@Override
	public int hashCode() {
		return Objects.hash(id_catalogo, temporada);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Catalogo other = (Catalogo) obj;
		return id_catalogo == other.id_catalogo && Objects.equals(temporada, other.temporada);
	}

	public boolean isEstado() {
		return estado;
	}

	public void setEstado(boolean estado) {
		this.estado = estado;
	}

	public void asignarTemporadaCatalogo(String unaTemporada) {
		this.temporada = unaTemporada;
	}

	public boolean verEstadoCatalogo() {
		return this.estado;
	}

	public void cambiarEstadoDelCatalogo() {
		if (this.estado) {
			this.estado = false;
		} else {
			this.estado = true;
		}
	}

//	public void agregarBeneficio(Beneficio nuevoBenefico) {
//		listaDeBeneficios.add(nuevoBenefico);
//	}
//	
//	public void eliminarBeneficio(Beneficio unBeneficio) {
//		listaDeBeneficios.remove(unBeneficio);
//	}

	public String devolverNombreCatalogo() {
		return this.temporada;
	}

	public String devolverTemporadaCatalogo() {
		return this.temporada;
	}

	@Override
	public String toString() {
		return "Catalogo [id_catalogo=" + id_catalogo + ", nombre=" + nombre + ", temporada=" + temporada + ", estado="
				+ estado + "]";
	}

}