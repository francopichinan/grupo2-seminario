package ar.edu.unrn.seminario.modelo;

import java.time.LocalDate;

public class Canje {
	
	private long id;
	private String nombre;
	private int puntos;
	private String descripcion;
	private LocalDate fecha;	
	private Beneficio beneficio;
	
	public Beneficio getBeneficio() {
		return beneficio;
	}

	public Canje() {
		this.id = 0L;
	}

	public void setBeneficio(Beneficio beneficio) {
		this.beneficio = beneficio;
	}


	public Canje(String nombre, int puntos, String descripcion, LocalDate fecha) {
		this.nombre = nombre;
		this.puntos = puntos;
		this.descripcion = descripcion;
		this.fecha = fecha;
	}
	
	
	public Canje(long id,String nombre, int puntos, String descripcion, LocalDate fecha) {
		
		this(nombre, puntos, descripcion, fecha);
		this.id = id;
	}


	public String getNombre() {
		return nombre;
	}


	public int getPuntos() {
		return puntos;
	}


	public String getDescripcion() {
		return descripcion;
	}


	public LocalDate getFecha() {
		return fecha;
	}


	public long getId() {
		return id;
	}
	
	public int getIDBeneficio() {
		return this.beneficio.getId_beneficio();
	}
	
	
	
	
	
	

}
