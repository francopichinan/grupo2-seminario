package ar.edu.unrn.seminario.modelo;

import java.time.LocalDate;

public class MovimientoPunto {
	
	private LocalDate fecha;
	private int puntos;
	private String descripcion;
	
	public MovimientoPunto(int puntos, String descripcion) {
		this.fecha = LocalDate.now();
		this.puntos = puntos;
		this.descripcion = descripcion;
	}
	
	public MovimientoPunto(int puntos, String descripcion, LocalDate fecha) {
		this(puntos, descripcion);
		this.fecha = fecha;
	}

	public LocalDate getFecha() {
		return fecha;
	}

	public int getPuntos() {
		return puntos;
	}

	public String getDescripcion() {
		return descripcion;
	}
	
	

}
