package ar.edu.unrn.seminario.modelo;

import java.util.Date;
import java.util.Set;

import javax.swing.Icon;

//import javax.swing.Icon;
import ar.edu.unrn.seminario.exception.NotNullException;

public class PuntoFijo extends Punto {

	private Long id;
	private Set<Residuo> residuosAlojados;
	private float porcentajeOcupacion;
	private Integer nivel;
	private Icon codigoQR;

	public PuntoFijo(/* float latitud, */ String titulo, /* float longitud, */String calle, Integer altura, /*Icon icono,*/
			String descripcion, Date fecha, String color, Boolean estado, Integer nivel, float porcentajeOcupacion,
			/*Icon codigoQR,*/ Mapa unMapa) throws NotNullException {
		super(/* latitud, */titulo, /* longitud, */calle, altura, descripcion, fecha, color, estado, unMapa);

		this.porcentajeOcupacion = porcentajeOcupacion;
		this.nivel = nivel;
//		this.codigoQR = codigoQR;
	}
	
	public PuntoFijo(long id, float porcentajeOcupacion, Integer nivel) {
		this.id=id;
		this.porcentajeOcupacion=porcentajeOcupacion;
		this.nivel=nivel;
	}

	public void actualizarPorcentaje(float nuevoPorcentaje) {
		this.porcentajeOcupacion = nuevoPorcentaje;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getId() {
		return this.id;
	}

	public void cambiarNivel(Integer nuevoNivel) {
		this.nivel = nuevoNivel;
	}

	// *** Getters y Seters ***//

	public Set<Residuo> getResiduosAlojados() {
		return residuosAlojados;
	}

	public void setResiduosAlojados(Set<Residuo> residuosAlojados) {
		this.residuosAlojados = residuosAlojados;
	}

	public float getPorcentajeOcupacion() {
		return porcentajeOcupacion;
	}

	public void setPorcentajeOcupacion(float porcentajeOcupacion) {
		this.porcentajeOcupacion = porcentajeOcupacion;
	}

	public Integer getNivel() {
		return nivel;
	}

	public void setNivel(Integer nivel) {
		this.nivel = nivel;
	}

	public Icon getCodigoQR() {
		return codigoQR;
	}

	public void setCodigoQR(Icon codigoQR) {
		this.codigoQR = codigoQR;
	}

}