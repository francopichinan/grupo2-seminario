package ar.edu.unrn.seminario.modelo;

public class RSU extends Residuo {
	private float peso;
	private int valorEnPuntosPorKg;
	
	public RSU(String nombre, String descripcion, Categoria unaCategoria, float peso, int puntosPorKg) {
		super(nombre, descripcion, unaCategoria);
		this.peso = peso;
		this.valorEnPuntosPorKg = puntosPorKg; // Hacer un metodo calcular puntos en base a la categoria del residuo y devuelva el valor de los puntos (?
	}
	
	public void modificarPeso(float nuevoPeso) {
		this.peso = nuevoPeso;
	}
	
	public void modificarPuntosPorKg(int nuevoValor) {
		this.valorEnPuntosPorKg = nuevoValor;
	}
	
	public float devolverPesoRSU() {
		return this.peso;
	}
	
	public int devolverPuntosPorKgRSU() {
		return this.valorEnPuntosPorKg;
	}
	
	public void informacionRSU() { //¿Eliminar de aca ya que esta en residuo?. En caso si, modificar ToString
		this.toString();
	}
}
