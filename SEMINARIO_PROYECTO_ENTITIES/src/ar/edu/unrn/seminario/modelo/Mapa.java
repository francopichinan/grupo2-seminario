package ar.edu.unrn.seminario.modelo;

import java.util.HashSet;
import java.util.Set;

import ar.edu.unrn.seminario.exception.NotNullException;
import ar.edu.unrn.seminario.exception.StateException;

public class Mapa {

	private String nombre;
	private Integer Id;
	private Integer codigo_postal;
	private Set<Punto> listaDePuntos;

	public Mapa(Punto nuevoPunto) throws Exception {
		if (isNull(nuevoPunto)) {
			throw new Exception("Punto Nulo");

		} else {
			listaDePuntos = new HashSet<Punto>();
			this.agregarPunto(nuevoPunto);
		}
	}

//	public Mapa(String nombre, Set<Punto> listaDePuntos) {
//		this.nombre = nombre;
//		this.listaDePuntos = listaDePuntos;
//	}

	public Mapa(String nombre) {
		this.nombre = nombre;
		this.codigo_postal = null;
		this.listaDePuntos = new HashSet<Punto>(0);
	}

	
	public Mapa(String nombre, Integer Id) {
		this.nombre = nombre;
		this.Id = Id;
	}
	
	
	public String getNombre() {
		return (this.nombre);
	}

	public Mapa(Integer codigo, String nombre) throws Exception {

		if (isNull(nombre) || isNull(codigo)) {
			throw new Exception("El nombre o el codigo estan vacio!");

		} else {
			this.nombre = nombre;
			this.codigo_postal = codigo;
		}
	}

	public void agregarPunto(Punto nuevoPunto) {
		listaDePuntos.add(nuevoPunto);
	}

	public void eliminarPunto(String titulo) throws StateException {
		Punto auxiliar = buscarPunto(titulo);
		if (isNull(auxiliar)) {
			throw new StateException("Punto no existe!");
		}
	}

	public Punto buscarPunto(String titulo) {
		for (Punto cadena : listaDePuntos) {
			if (cadena.equals(titulo)) {
				return (cadena);
			}
		}
		return (null);
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public Integer getCodigoPostal() {
		return codigo_postal;
	}

	public void setCodigoPostal(Integer codigo) {
		this.codigo_postal = codigo;
	}

	public Integer getId() {
		return (this.Id);
	}

	public void setId(Integer Id) {
		this.Id = Id;
	}

	protected Boolean isNull(Object unObjeto) {
		if (unObjeto == null) {
			return (true);
		} else {
			return (false);
		}
	}

	public Boolean equals(String nombre) {
		
		if(this.nombre == nombre) {
			return true;
		
		}else {
			return false;
		}
	}
	
}
