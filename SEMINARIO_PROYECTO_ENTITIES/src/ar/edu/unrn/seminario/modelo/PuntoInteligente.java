package ar.edu.unrn.seminario.modelo;

import java.util.Date;

import javax.swing.Icon;

import ar.edu.unrn.seminario.exception.NotNullException;

public class PuntoInteligente extends Punto{
	public PuntoInteligente()
	{
		
	}
	public PuntoInteligente(float latitud, String titulo, float longitud, Icon icono, String descripcion, Date fecha, String color,Boolean estado) throws NotNullException
	{
		super(latitud,titulo,longitud,icono,descripcion,fecha,color,estado);
	}
}