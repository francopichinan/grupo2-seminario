package ar.edu.unrn.seminario.modelo;

public class RAEE extends Residuo {
	private String estado;
	private String identificador;
	
	public RAEE(String nombre, String descripcion, Categoria unaCategoria, String estado, String unIdentificador) {
		super(nombre, descripcion, unaCategoria);
		this.estado = estado;
		this.identificador = unIdentificador;
	}
	
	public RAEE(String nombre, String descripcion, Categoria unaCategoria, String unIdentificador) {
		super(nombre, descripcion, unaCategoria);
		this.identificador=unIdentificador;
	}
	
	public void modificarEstado(String nuevoEstado) {
		this.estado = nuevoEstado;
	}
	
	public void modificarIdentificador(String nuevoIdentificador) {
		this.identificador = nuevoIdentificador;
	}
	
	public String devolverIdentificador() {
		return this.identificador;
	}
	
	public String devolverEstadoRAEE() {
		return this.estado;
	}
	
	public void informacionRAEE() { //¿Eliminar de aca ya que esta en residuo?. En caso si, modificar ToString
		this.toString();
	}
}
