package ar.edu.unrn.seminario.modelo;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import ar.edu.unrn.seminario.exception.RegistroVisitaException;
import ar.edu.unrn.seminario.exception.StateException;

public class Orden {
	private Long id_orden;
	private Recolector recolector;
	private Pedido pedido;
	private List<Visita> listaVisitas = new ArrayList<>();
	private String estado;
	private LocalDate fecha;
	private final String PENDIENTE = "PENDIENTE";
	private final String EN_EJECUCION = "EN_EJECUCION";
	private final String CONCRETADO = "CONCRETADO";
	private final String CANCELADO = "CANCELADO";

	public Orden(Pedido pedido, Recolector recolector) {
		this.pedido = pedido;
		this.recolector = recolector;
		this.listaVisitas = new ArrayList<>();
		this.estado = PENDIENTE;
		this.fecha = LocalDate.now();
	}

	public Orden(Long id_orden, String estado, LocalDate fecha) {
		this.id_orden = id_orden;
		this.estado = estado;
		this.fecha = fecha;
	}

	public Long getId_orden() {
		return id_orden;
	}

	public void setId_orden(Long id_orden) {
		this.id_orden = id_orden;
	}

	public Recolector getRecolector() {
		return recolector;
	}

	public void setRecolector(Recolector recolector) {
		this.recolector = recolector;
	}

	private boolean noEstaCanceladaNiConcretada() {
		return !estado.equals(CANCELADO) && !estado.equals(CONCRETADO);
	}

	public void finalizar() throws StateException {
		if (this.estado.equals(EN_EJECUCION) && noEstaCanceladaNiConcretada())
			this.estado = CONCRETADO;
		else
			throw new StateException("No se puede finalizar la orden!");
	}

	public void cancelar() throws StateException {
		if (noEstaCanceladaNiConcretada())
			this.estado = CANCELADO;
		else
			throw new StateException("No se puede cancelar la orden");
	}

	public Pedido getPedido() {
		return pedido;
	}

	public void setPedido(Pedido pedido) {
		this.pedido = pedido;
	}

	public void agregarVisita(Visita visita) throws RegistroVisitaException {
		if (noEstaCanceladaNiConcretada()) {
			if (this.estado.equals(PENDIENTE))
				this.estado = EN_EJECUCION;
			this.listaVisitas.add(visita);
		} else
			throw new RegistroVisitaException("No se puede agregar visita a la orden");
	}

	public String getEstado() {
		return this.estado;
	}

	public LocalDate getFecha() {
		return this.fecha;
	}

	public boolean esFinalizada() {
		return this.estado.equals(CONCRETADO);
	}

	public boolean esCancelada() {
		return this.estado.equals(CANCELADO);
	}

	public boolean esPendiente() {
		return this.estado.equals(PENDIENTE);
	}

	public boolean esEnEjecucion() {
		return this.estado.equals(EN_EJECUCION);
	}
}
