package ar.edu.unrn.seminario.modelo;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import ar.edu.unrn.seminario.exception.DataEmptyException;
import ar.edu.unrn.seminario.exception.NotNullException;
import ar.edu.unrn.seminario.exception.NotValidDNIException;

//El Abstract es hasta que se haga la implementacion.
public class Ciudadano {

	private long id;
	private int puntos;

	@SuppressWarnings("unused")
	private List<Canje> puntosGastados;
	@SuppressWarnings("unused")
	private List<Responsable> responsables;
	private Usuario usuario;
	private String nombre;
	private String apellido;
	private String dni;
	private String email;
	private LocalDate fechaNacimiento;

	public Ciudadano() {
		this.puntos = 0;
		this.puntosGastados = new ArrayList<Canje>();
		this.responsables = new ArrayList<Responsable>();
	}

	public Ciudadano(String nombre, String apellido, String dni, int puntos, String email, LocalDate fNacimiento,
			Usuario user) throws NotValidDNIException, DataEmptyException, NotNullException {
		this();

		// Excepciones
		try {
			if (Integer.parseInt(dni) < 0) {
				throw new NotValidDNIException("Solo Enteros Positivos");
			}
		} catch (NumberFormatException e) {
			throw new NotValidDNIException("Solo Numeros Enteros");
		}

		if (this.esVacio(nombre)) {
			throw new DataEmptyException("Nombre");
		}
		if (this.esVacio(apellido)) {
			throw new DataEmptyException("Apellido");
		}
		if (this.esVacio(dni)) {
			throw new DataEmptyException("DNI");
		}
		if (this.esVacio(email)) {
			throw new DataEmptyException("Email");
		}
		if (this.esNulo(nombre)) {
			throw new NotNullException("Nombre");
		}
		if (this.esNulo(apellido)) {
			throw new NotNullException("Apellido");
		}
		if (this.esNulo(dni)) {
			throw new NotNullException("DNI");
		}
		if (this.esNulo(email)) {
			throw new NotNullException("Email");
		}
		if (this.esNulo(user)) {
			throw new NotNullException("Usuario");
		}

		// Fin de Excepciones

		this.usuario = user;
		this.nombre = nombre;
		this.apellido = apellido;
		this.dni = dni;
		this.puntos = puntos;
		this.email = email;
		this.fechaNacimiento = fNacimiento;
	}

	public Ciudadano(long id, String nombre, String apellido, String dni, int puntos, String email,
			LocalDate fNacimiento, Usuario user) throws NotValidDNIException, DataEmptyException, NotNullException {

		this(nombre, apellido, dni, puntos, email, fNacimiento, user);
		this.id = id;
	}

	public LocalDate getFechaNacimiento() {
		return fechaNacimiento;
	}

	public String getDNI() {
		return this.dni;
	}

	public String getNombre() {
		return this.nombre;
	}

	public String getApellido() {
		return this.apellido;
	}

	public int getPuntos() {
		return this.puntos;
	}

	public long getID() {
		return this.id;
	}

	public void setPuntosGastados(List<Canje> puntosGastados) {
		this.puntosGastados = puntosGastados;
	}

	public void setResponsables(List<Responsable> responsables) {
		this.responsables = responsables;
	}

	public String getUsuario() {
		return this.usuario.getUsuario();
	}

	@Override
	public int hashCode() {
		return Objects.hash(dni, id);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Ciudadano other = (Ciudadano) obj;
		return Objects.equals(dni, other.dni) && id == other.id;
	}

	public String getContrasena() {
		return this.usuario.getContrasena();
	}

	public Rol getRol() {
		return this.usuario.getRol();
	}

	public long getRolID() {
		return this.usuario.getRolID();
	}

	public boolean isActivado() {
		return this.usuario.isActivo();
	}

	public String getUsername() {
		return this.usuario.getUsuario();
	}

	@Override
	public String toString() {
		return "\nPuntos: " + puntos;
	}

	public void setDni(String dni) {
		this.dni = dni;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	private boolean esVacio(String cadena) {
		return (cadena.equals(""));
	}

	private boolean esNulo(Object objeto) {
		return objeto.equals(null);
	}

	public Ciudadano getDefaultInstance() {

		Ciudadano n = new Ciudadano();
		n.setDni("1");
		return n;
	}

	public int getCantidadResponsable() {
		return this.responsables.size();
	}

	public int getCantidadCanjes() {
		return this.puntosGastados.size();
	}

	public void restarPuntos(int puntos) {

		if (puntos > -1) {
			if (this.puntos >= puntos) {
				this.puntos -= puntos;
			} else {
				System.out.println("Inserte Excepcion Aqui 1");
			}
		} else {
			System.out.println("Inserte Excepcion Aqui 2");
		}

	}

	public void addCanje(Canje canje) {
		this.puntosGastados.add(canje);
	}

	public void sumarPuntos(int puntos) {
		if (puntos > -1) {
			this.puntos += puntos;
		} else {
			System.out.println("Inserte Excepcion Aqui");
		}
	}

}
