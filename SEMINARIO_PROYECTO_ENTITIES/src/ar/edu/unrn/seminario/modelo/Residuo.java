package ar.edu.unrn.seminario.modelo;

public class Residuo {
	protected String nombre;
	protected String descripcion;
	protected Categoria categoriaResiduo;
	protected int cantidad;

	public Residuo(String nombre, String descripcion, Categoria unaCategoria) {
		this.nombre = nombre;
		this.descripcion = descripcion;
		this.categoriaResiduo = unaCategoria;
	}

	public Residuo(int cantidad, Categoria unaCategoria) {
		this.cantidad = cantidad;
		this.categoriaResiduo = unaCategoria;
	}

	public void asignarNombreResiduo(String unNombre) {
		this.nombre = unNombre;
	}

	public void asignarDescripcionResiduo(String unaDescripcion) {
		this.descripcion = unaDescripcion;
	}

	public void asignarCategoria(Categoria unaCategoria) {
		this.categoriaResiduo = unaCategoria;
	}

	public void informacionResiduo() {
		this.toString();
	}

	public Categoria verCategoria() {
		return this.categoriaResiduo;
	}

	public int verCantidad() {
		return this.cantidad;
	}
	
	public String verNombre() {
		return this.nombre;
	}
	
	public String verDescripcion() {
		return this.descripcion;
	}
}