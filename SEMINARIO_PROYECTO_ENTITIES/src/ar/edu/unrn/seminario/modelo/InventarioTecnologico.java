package ar.edu.unrn.seminario.modelo;

import java.util.*;

public class InventarioTecnologico {
	private ArrayList <RAEE> listaObjetos; //Historial de todos los objetos que estuvieron en el inventario
	private Map<String, RAEE> objetosDelInventario; //Lista de objetos actuales en el inventario
	
	public InventarioTecnologico() {
		this.listaObjetos = new ArrayList<>();
		this.objetosDelInventario = new HashMap();
	}
	
	public void agregarObjetoAlInventario(RAEE nuevoObjeto) { //En duda de si pasarle un objeto o que lo cree directamente en el metodo
		this.listaObjetos.add(nuevoObjeto);
		this.objetosDelInventario.put(nuevoObjeto.devolverIdentificador(), nuevoObjeto);
	}
	
	public void eliminarObjetoDelInventario(String identificadorObjeto) { //Por medio del identificador eliminara el objeto del inventario
		this.objetosDelInventario.remove(identificadorObjeto, objetosDelInventario.get(identificadorObjeto));
	}	
	
	public void modificarObjetoDelInventario(String identificadorObjeto, RAEE objetoModificado) { //En duda de los parametros
		this.objetosDelInventario.replace(identificadorObjeto, objetoModificado);
	}
	
	public ArrayList<RAEE> listarObjetos() {//Devuelve la lista de objetos actuales del inventario (Map)
											//Agregar un nuevo metodo "ListarHistorialObjetos" que liste todos los objetos que estuvieron/estan en el inventario
		ArrayList<RAEE> lista = new ArrayList<>();
		
		//Falta terminar
		
		return lista;
	}
	
	public ArrayList<RAEE> listarObjetosEnEstadoReparado() { //Devuelve una lista de los objetos en estado "Reparado"
		ArrayList<RAEE> lista = new ArrayList<>();
		this.objetosDelInventario.values();
		for (RAEE raee : listaObjetos) {
			
			//Falta terminar
			
		}
		
		return lista;
	}
	public ArrayList<RAEE> listarObjetosEnBuenEstado() {//Devuelve una lista de los objetos un estado diferente a "Reparado"
		ArrayList<RAEE> lista = new ArrayList<>();
		
		//Falta terminar
		
		return lista;
	}
	
	public RAEE devolverObjeto(String identificadorObjeto) { //Por medio del map buscara al objeto por su identificador y lo devolvera
		return this.objetosDelInventario.get(identificadorObjeto);
	}
}
