package ar.edu.unrn.seminario.modelo;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import ar.edu.unrn.seminario.exception.DataEmptyException;

public class Visita {
	private Long id;
	private LocalDate fecha;
	private String observacion;
	private List<RAEE> listaResiduosRAEE;
	private List<RSU> listaResiduosRSU;
	private List<Residuo> listaResiduos;
	private Orden orden;

	public Visita(LocalDate fecha, String observacion, ArrayList<Residuo> listaResiduos) {
		this.fecha = fecha;
		this.observacion = observacion;
		this.listaResiduos = listaResiduos;
	}

	public Visita(LocalDate fecha, String observacion, List<RSU> listaResiduosRSU, List<RAEE> listaResiduosRAEE, Orden orden) {
		this.fecha = fecha;
		this.observacion = observacion;
		this.listaResiduosRAEE = listaResiduosRAEE;
		this.listaResiduosRSU = listaResiduosRSU;
		this.orden = orden;
	}
	
	public Visita(Long id, LocalDate fecha, String observacion) {
		this.id = id;
		this.fecha = fecha;
		this.observacion = observacion;
	}

	public Visita(String observacion) throws DataEmptyException {
		if (observacion.isEmpty()) 
			throw new DataEmptyException("Observacion");
		this.fecha = LocalDate.now();
		this.observacion = observacion;
		this.listaResiduos = new ArrayList<>();
	}
	
	public Visita(String observacion, Orden orden) throws DataEmptyException {
		if (observacion.isEmpty()) 
			throw new DataEmptyException("Observacion");
		this.fecha = LocalDate.now();
		this.observacion = observacion;
		this.listaResiduos = new ArrayList<>();
		this.orden = orden;
	}
	
	public Long getId() {
		return id;
	}

	public LocalDate getFecha() {
		return fecha;
	}

	public void setFecha(LocalDate fecha) {
		this.fecha = fecha;
	}

	public String getObservacion() {
		return observacion;
	}

	public void setObservacion(String observacion) {
		this.observacion = observacion;
	}

	public Orden getOrden() {
		return orden;
	}

	public void setOrden(Orden orden) {
		this.orden = orden;
	}

	@Override
	public String toString() {
		return "Visita [id=" + id + ", fecha=" + fecha + ", observacion=" + observacion + "]";
	}

	public List<RAEE> listarResiduosRAEE() {
		return listaResiduosRAEE;
	}

	public List<RSU> listarResiduosRSU() {
		return listaResiduosRSU;
	}
}