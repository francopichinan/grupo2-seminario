package ar.edu.unrn.seminario.exception;

public class NotNullException extends Exception {

	private static final long serialVersionUID = 1L;

	public NotNullException() {
		super();
	}

	public NotNullException(String message) {
		super(message);
	}
}
