package ar.edu.unrn.seminario.exception;

public class RegistroVisitaException extends Exception {
	
	private static final long serialVersionUID = 1L;

	public RegistroVisitaException(String message) {
		super(message);
	}
}
