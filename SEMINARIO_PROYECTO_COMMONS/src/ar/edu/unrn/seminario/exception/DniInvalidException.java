package ar.edu.unrn.seminario.exception;

public class DniInvalidException extends Exception {

	private static final long serialVersionUID = 1L;
	
	public DniInvalidException(String message) {
		super(message);
	}
}
