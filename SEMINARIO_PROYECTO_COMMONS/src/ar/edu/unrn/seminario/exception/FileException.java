package ar.edu.unrn.seminario.exception;

public class FileException extends RuntimeException {

	private static final long serialVersionUID = 1L;

	public FileException(String message) {
		super("No existe un archivo con el nombre especificado." + "\nMensaje:" + message);
	}
}
