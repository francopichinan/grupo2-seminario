package ar.edu.unrn.seminario.exception;

public class DataEmptyException extends Exception {

	private static final long serialVersionUID = 1L;

	public DataEmptyException() {
		super();
	}

	public DataEmptyException(String message) {
		super(message);
	}
}
