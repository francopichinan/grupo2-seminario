package ar.edu.unrn.seminario.exception;

public class BaseDeDatosException extends Exception {

	private static final long serialVersionUID = 1L;

	/*public BaseDeDatosException(String tabla, String sqlState , String message){
        super("Error accediendo a la base de datos, accediendo a la tabla " + tabla
                + ".\nError SQL ANSI-92: " + sqlState + ".\nSQL Exception: " + message);
    }*/

	public BaseDeDatosException(String message){
        super(message);
    }
	
	/*public BaseDeDatosException(String cause, String message) {
		super("Error de conectividad." + "\nCausa: " + cause + "\nMensaje: " + message);
	}*/
}
