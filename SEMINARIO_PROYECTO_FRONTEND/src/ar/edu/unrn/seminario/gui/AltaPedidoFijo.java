package ar.edu.unrn.seminario.gui;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;

import ar.edu.unrn.seminario.api.IApi;
import ar.edu.unrn.seminario.dto.CategoriaDTO;
import ar.edu.unrn.seminario.dto.PuntoDTO;
import ar.edu.unrn.seminario.dto.PuntoFijoDTO;
import ar.edu.unrn.seminario.exception.BaseDeDatosException;
import ar.edu.unrn.seminario.exception.DataEmptyException;
import java.awt.Font;

public class AltaPedidoFijo extends JFrame {

	private static final long serialVersionUID = 1L;
	// Tabla en la que se visualizaran los Puntos Fijos
	private JTable tablePF;
	private JButton btnGenerarPedido = new JButton("Generar Pedido");
	private Object[] titulos = { "Tipo", "Direccion", "Titulo", "% ocupacion" };
	private DefaultTableModel modelo = new DefaultTableModel(titulos, 0) {
		private static final long serialVersionUID = 1L;

		@Override
		public boolean isCellEditable(int row, int column) {
			return false;
		}
	};

	private JTable tableResiduos;
	private Object[] tituloResiduos = { "Residuos del punto:" };
	private DefaultTableModel modeloResiduos = new DefaultTableModel(tituloResiduos, 0);

	private List<PuntoFijoDTO> puntos = new ArrayList<>();
	private List<CategoriaDTO> categorias = new ArrayList<>();

	public AltaPedidoFijo(IApi api) {
		setTitle("Alta Pedido - Punto Fijo");
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setBounds(100, 100, 1039, 517);
		getContentPane().setLayout(null);

		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setBounds(32, 48, 801, 409);
		getContentPane().add(scrollPane);

		// Inicializando tabla de puntos fijos y poniendo titulos
		tablePF = new JTable();
		scrollPane.setViewportView(tablePF);
		tablePF.setModel(modelo);
		btnGenerarPedido.setEnabled(false);
		tablePF.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent arg0) {
				btnGenerarPedido.setEnabled(true);
				cargarResiduos(tablePF.getSelectedRow());
			}
		});

		// LABELS Y BOTONES
		JLabel lblNewLabel = new JLabel("Listado de Puntos Fijos. Seleccione uno para generar el Pedido.");
		lblNewLabel.setFont(new Font("Tahoma", Font.PLAIN, 12));
		lblNewLabel.setBounds(22, 10, 549, 28);
		getContentPane().add(lblNewLabel);

		JScrollPane scrollPane_1 = new JScrollPane();
		scrollPane_1.setBounds(834, 48, 175, 344);
		getContentPane().add(scrollPane_1);

		tableResiduos = new JTable();
		scrollPane_1.setViewportView(tableResiduos);
		tableResiduos.setModel(modeloResiduos);

		JCheckBox checkCarga = new JCheckBox("Requiere carga pesada");
		checkCarga.setBounds(846, 398, 175, 21);
		getContentPane().add(checkCarga);

		
		btnGenerarPedido.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				try {
					api.registrarPedido(checkCarga.isSelected(), puntos.get(tablePF.getSelectedRow()));
					JOptionPane.showMessageDialog(null, "Pedido generado con Exito!", "Info",
							JOptionPane.INFORMATION_MESSAGE);
					cargarPuntos(api);
					modeloResiduos.setRowCount(0);
				} catch (BaseDeDatosException | DataEmptyException e1) {
					e1.printStackTrace();
				}
			}
		});
		btnGenerarPedido.setBounds(844, 429, 165, 28);
		getContentPane().add(btnGenerarPedido);

		// CARGANDO PUNTOS
		this.cargarPuntos(api);
//		this.cargarResiduos(0);
	}

	private void obtenerPuntosFijos(IApi api) {
		// TODO mejorar las excepciones
		try {
			this.puntos = api.obtenerPuntosFijosNuevoPedido();
		} catch (BaseDeDatosException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	private void cargarPuntos(IApi api) {
		modelo = new DefaultTableModel(titulos, 0);
		obtenerPuntosFijos(api);

		for (PuntoFijoDTO punto : puntos) {
			modelo.addRow(new Object[] { punto.getTipo(), punto.getDireccion(), punto.getTitulo(),
					punto.getPorcentajeOcupacion() });
		}
		tablePF.setModel(modelo);
	}

	private void cargarResiduos(int index) {
		this.categorias = this.puntos.get(index).getTipoDeResiduo();

		modeloResiduos = new DefaultTableModel(tituloResiduos, 0) {
			private static final long serialVersionUID = 1L;

			@Override
			public boolean isCellEditable(int row, int column) {
				return false;
			}
		};
		for (CategoriaDTO cat : categorias) {
			this.modeloResiduos.addRow(new Object[] { cat.toString() });
		}
		tableResiduos.setModel(modeloResiduos);
	}
}
