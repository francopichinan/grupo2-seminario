package ar.edu.unrn.seminario.gui;

import java.awt.BorderLayout;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.WindowEvent;
import java.awt.event.WindowFocusListener;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.border.BevelBorder;
import javax.swing.border.EtchedBorder;
import javax.swing.border.SoftBevelBorder;
import javax.swing.table.DefaultTableModel;

import ar.edu.unrn.seminario.api.IApi;
import ar.edu.unrn.seminario.dto.OrdenDTO;
import ar.edu.unrn.seminario.dto.PedidoDTO;
import ar.edu.unrn.seminario.exception.BaseDeDatosException;
import ar.edu.unrn.seminario.exception.DataEmptyException;
import ar.edu.unrn.seminario.exception.DniInvalidException;
import ar.edu.unrn.seminario.exception.StateException;

public class ListadoPedidos extends JFrame {

	private static final long serialVersionUID = 1L;
	private JScrollPane scrollPane = new JScrollPane();
	private JTable tabla = new JTable();
	private Object[] titulos = { "Fecha", "Direccion", "Carga Pesada", "Observacion" };
	private DefaultTableModel modelo = new DefaultTableModel(titulos, 0) {
		private static final long serialVersionUID = 1L;

		public Class<?> getColumnClass(int column) {
			return column == 2 ? Boolean.class : super.getColumnClass(column);
		}
		
		@Override
		public boolean isCellEditable(int row, int column) {
			return false;
		}
	};
	private List<PedidoDTO> pedidos = new ArrayList<>();

	private JPanel panelBotones = new JPanel();
	//private JButton btnEditar = new JButton("Editar");
	//private JButton btnEliminar = new JButton("Eliminar");

	public ListadoPedidos(IApi api) {
		setTitle("Listado Pedidos");
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setBounds(100, 100, 653, 300);
		getContentPane().setLayout(new BorderLayout());

		addWindowFocusListener(new WindowFocusListener() {
			public void windowGainedFocus(WindowEvent e) {
				reloadGrid(api);
			}
			public void windowLostFocus(WindowEvent e) {
			}
		});
		
		try {
			pedidos = api.obtenerPedidos();
		}  catch (BaseDeDatosException | DataEmptyException e) {
			JOptionPane.showMessageDialog(this, e.getMessage());
		}
		cargarPedidos();
		tabla.setBorder(new SoftBevelBorder(BevelBorder.LOWERED, null, null, null, null));

		tabla.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent arg0) {
				habilitarBotones(true);
			}
		});

		panelBotones.setBorder(new EtchedBorder(EtchedBorder.LOWERED, null, null));
		getContentPane().add(panelBotones, BorderLayout.SOUTH);
		//panelBotones.add(btnEditar);
		//panelBotones.add(btnEliminar);


		habilitarBotones(false);

		getContentPane().add(scrollPane, BorderLayout.CENTER);
		tabla.setModel(modelo);
		scrollPane.setViewportView(tabla);
	}

	private void habilitarBotones(boolean b) {
		//btnEditar.setEnabled(b);
		//btnEliminar.setEnabled(b);
	}

	private void cargarPedidos() {
		for (int i = 0; i < pedidos.size(); i++) {
			modelo.addRow(new Object[] { pedidos.get(i).getFecha(), pedidos.get(i).getDireccion(),
					pedidos.get(i).getCargaPesada(), pedidos.get(i).getObservacion() });
		}
	}
	
	private void reloadGrid(IApi api) {
		DefaultTableModel modelo = (DefaultTableModel) tabla.getModel();
		try {
			pedidos = api.obtenerPedidos();
		} catch (BaseDeDatosException | DataEmptyException  e1) {
			JOptionPane.showMessageDialog(this, e1.getMessage());
		}
		modelo.setRowCount(0);
		cargarPedidos();
	}
}
