package ar.edu.unrn.seminario.gui;

import java.awt.BorderLayout;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.border.EmptyBorder;
import javax.swing.table.DefaultTableModel;

import ar.edu.unrn.seminario.api.IApi;
import ar.edu.unrn.seminario.dto.ResiduoDTO;
import ar.edu.unrn.seminario.dto.VisitaDTO;
import ar.edu.unrn.seminario.exception.BaseDeDatosException;
import ar.edu.unrn.seminario.exception.StateException;

public class ListarResiduosRetirados extends JFrame {

	private JPanel contentPane = new JPanel();
	private static final long serialVersionUID = 1L;
	private Object[] titulos = {"Tipo", "Cantidad"};
	private DefaultTableModel modelo = new DefaultTableModel(titulos, 0);
	private List<ResiduoDTO> residuos = new ArrayList<>();
	
	private JPanel panelBotones = new JPanel();
	private JButton btnEditar = new JButton("Editar");
	private JButton btnEliminar = new JButton("Eliminar");
	private JTable tabla;
	
	public ListarResiduosRetirados(VisitaDTO visita) {
		setTitle("Lista residuos");
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setBounds(100, 100, 450, 300);
		getContentPane().setLayout(new BorderLayout());
		
		JScrollPane scrollPane = new JScrollPane();
		getContentPane().add(scrollPane, BorderLayout.CENTER);
		
		tabla = new JTable();
		scrollPane.setViewportView(tabla);
		
		JPanel panel = new JPanel();
		getContentPane().add(panel, BorderLayout.SOUTH);
		
		JButton btnNewButton = new JButton("Salir");
		panel.add(btnNewButton);
		
		btnNewButton.addActionListener(e -> {
			dispose();
		});
		
//		try {
//			residuos = ;
//		} catch (BaseDeDatosException e) {
//			JOptionPane.showMessageDialog(this, e.getMessage());
//		}
//		cargarResiduos();
		
		getContentPane().add(scrollPane, BorderLayout.CENTER);
		tabla.setModel(modelo);
		scrollPane.setViewportView(tabla);
		
		setContentPane(contentPane);
	}
	
	private void cargarResiduos() {
		for (int i=0; i<residuos.size(); i++) {
//			modelo.addRow(new Object[] {residuos.get(i).getFecha(), residuos.get(i).getObservacion() } );
		}
	}
	
	private void reloadGrid(IApi api, VisitaDTO visita) throws BaseDeDatosException {
		DefaultTableModel modelo = (DefaultTableModel) tabla.getModel();
		residuos = visita.getResiduos();
		modelo.setRowCount(0);
		cargarResiduos();
	}

}
