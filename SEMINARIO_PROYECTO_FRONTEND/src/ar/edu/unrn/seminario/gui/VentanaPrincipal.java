package ar.edu.unrn.seminario.gui;

import java.awt.EventQueue;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.border.EtchedBorder;

import ar.edu.unrn.seminario.api.IApi;
import ar.edu.unrn.seminario.api.PersistenceApi;

public class VentanaPrincipal extends JFrame {

	private static final long serialVersionUID = 1L;
	private final String ALTA = "Alta";
	private final String ALTAInteligente = "Alta Pedido PI";
	private final String ALTAFijo = "Alta Pedido PF";
	private final String LIST = "Listado";
	// private String modif = "Listado/Modificacion";
	// private String alta_orden = "Alta";

	private JMenuBar menuBar = new JMenuBar();

	private JMenu usuarioMenu = new JMenu("Usuarios");
	private JMenuItem altaUsuarioMenuItem = new JMenuItem(ALTA);
	private JMenuItem listadoUsuarioMenuItem = new JMenuItem(LIST);

	private JMenu ordenMenu = new JMenu("Ordenes");
	private JMenuItem altaOrden = new JMenuItem(ALTA);
	private JMenuItem listadoOrden = new JMenuItem(LIST);

	private JMenu pedidoMenu = new JMenu("Pedidos");
	private JMenuItem altaPedidoInteligente = new JMenuItem(ALTAInteligente);
	private JMenuItem altaPedidoFijo = new JMenuItem(ALTAFijo);
	private JMenuItem listadoPedido = new JMenuItem(LIST);

	private JMenu recolectorMenu = new JMenu("Recolectores");
	private JMenuItem altaRecolector = new JMenuItem(ALTA);
	private JMenuItem listadoRecolector = new JMenuItem(LIST);

	private JMenu configuracionMenu = new JMenu("Configuraciones");
	private JMenuItem salirMenuItem = new JMenuItem("Salir");

	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
//					UIManager.setLookAndFeel(new FlatDarkLaf());
					IApi api = new PersistenceApi();
					VentanaPrincipal frame = new VentanaPrincipal(api);
					frame.setLocationRelativeTo(null);
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	public VentanaPrincipal(IApi api) {
		getContentPane().setLayout(null);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 600, 300);

		JLabel lblPuntoInteligente = new JLabel("PI = Punto Inteligente");
		lblPuntoInteligente.setBounds(430, 177, 144, 14);
		getContentPane().add(lblPuntoInteligente);

		JLabel lblPuntoFijo = new JLabel("PF = Punto Fijo");
		lblPuntoFijo.setBounds(430, 202, 144, 14);
		getContentPane().add(lblPuntoFijo);

		JPanel panel = new JPanel();
		panel.setBounds(404, 157, 160, 71);
		getContentPane().add(panel);
		panel.setBorder(new EtchedBorder(EtchedBorder.LOWERED, null, null));

		setJMenuBar(menuBar);

		// Usuarios
		menuBar.add(usuarioMenu);

		altaUsuarioMenuItem.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				AltaUsuario alta = new AltaUsuario(api);
				alta.setLocationRelativeTo(null);
				alta.setVisible(true);
			}
		});
		usuarioMenu.add(altaUsuarioMenuItem);

		listadoUsuarioMenuItem.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				ListadoUsuario listado = new ListadoUsuario(api);
				listado.setLocationRelativeTo(null);
				listado.setVisible(true);
			}
		});
		usuarioMenu.add(listadoUsuarioMenuItem);
		
				// Recolectores
				menuBar.add(recolectorMenu);
				
						altaRecolector.addActionListener(e -> {
							AltaRecolector alta = new AltaRecolector(api);
							alta.setLocationRelativeTo(null);
							alta.setVisible(true);
						});
						recolectorMenu.add(altaRecolector);
						
								listadoRecolector.addActionListener(e -> {
									ListadoRecolector listado = new ListadoRecolector(api);
									listado.setLocationRelativeTo(null);
									listado.setVisible(true);
								});
								recolectorMenu.add(listadoRecolector);
		
				// Pedidos
				menuBar.add(pedidoMenu);
				
						altaPedidoInteligente.addActionListener(e -> {
							AltaPedidoInteligente alta = new AltaPedidoInteligente(api);
							alta.setLocationRelativeTo(null);
							alta.setVisible(true);
						});
						pedidoMenu.add(altaPedidoInteligente);
						
								altaPedidoFijo.addActionListener(e -> {
						
									AltaPedidoFijo alta = new AltaPedidoFijo(api);
									alta.setLocationRelativeTo(null);
									alta.setVisible(true);
						
								});
								pedidoMenu.add(altaPedidoFijo);
								
								listadoPedido.addActionListener(e -> {
									ListadoPedidos listado = new ListadoPedidos(api);
									listado.setLocationRelativeTo(null);
									listado.setVisible(true);
								});
								pedidoMenu.add(listadoPedido);

		// Ordenes
		menuBar.add(ordenMenu);

		altaOrden.addActionListener(e -> {
			AltaOrden alta = new AltaOrden(api);
			alta.setLocationRelativeTo(null);
			alta.setVisible(true);
		});
		ordenMenu.add(altaOrden);

		listadoOrden.addActionListener(e -> {
			ListadoOrdenes listado = new ListadoOrdenes(api);
			listado.setLocationRelativeTo(null);
			listado.setVisible(true);
		});
		ordenMenu.add(listadoOrden);

		// Configuraciones
		menuBar.add(configuracionMenu);

		configuracionMenu.add(salirMenuItem);
		salirMenuItem.addActionListener(e -> System.exit(0));
	}
}
