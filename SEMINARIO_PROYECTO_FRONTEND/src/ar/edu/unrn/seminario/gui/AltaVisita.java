package ar.edu.unrn.seminario.gui;
 
import java.awt.Color;
import java.awt.Font;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.border.LineBorder;
import javax.swing.table.DefaultTableModel;

import com.toedter.calendar.JDateChooser;

import ar.edu.unrn.seminario.api.IApi;
import ar.edu.unrn.seminario.dto.CategoriaDTO;
import ar.edu.unrn.seminario.dto.RAEEDTO;
import ar.edu.unrn.seminario.dto.RSUDTO;
import ar.edu.unrn.seminario.dto.ResiduoDTO;
import ar.edu.unrn.seminario.dto.VisitaDTO;
import ar.edu.unrn.seminario.exception.BaseDeDatosException;
import ar.edu.unrn.seminario.exception.DataEmptyException;
import ar.edu.unrn.seminario.exception.RegistroVisitaException;

public class AltaVisita extends JFrame {

	private static final long serialVersionUID = 1L;
	private JLabel lblObservacion = new JLabel("Observacion:");

	private JButton btnCancelar = new JButton("Cancelar");
	private JButton btnAceptar = new JButton("Registrar visita");
	private JTextField txtRAEEId;
	private JTextField txtRAEENombre;
	private JTextField txtRAEEDesc;
	private JTextField txtRSUNombre;
	private JTextField txtRSUDesc;
	private JTextField txtRSUPeso;
	JDateChooser fecha = new JDateChooser();
	JTextArea txtObservacion = new JTextArea();
	// Tabla de Residuos

	private Object[] titulos = { "Tipo de Residuo", "Categoria", "Nombre", "Descripcion" };
	private DefaultTableModel modelo = new DefaultTableModel(titulos, 0);
	private JTable table;
	List<CategoriaDTO> categorias = new ArrayList<CategoriaDTO>();
	JComboBox<CategoriaDTO> comboCategorias = new JComboBox<CategoriaDTO>();

	List<RAEEDTO> lisResiduosRAEE = new ArrayList<RAEEDTO>();
	List<RSUDTO> lisResiduosRSU = new ArrayList<RSUDTO>();
	List<ResiduoDTO> lisResiduos = new ArrayList<ResiduoDTO>();

	JPanel panelRAEE = new JPanel();
	JPanel panelRSU = new JPanel();

	// TODO llenar las listas de residuosDTO e invocar al metodo crearVisita de API
	// para q efectue el guardado

	public AltaVisita(IApi api, Long id_orden) {

		setTitle("Alta visitas");
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setBounds(100, 100, 956, 529);
		getContentPane().setLayout(null);

		lblObservacion.setBounds(314, 45, 80, 14);
		getContentPane().add(lblObservacion);

		btnCancelar.setBounds(621, 419, 89, 23);
		getContentPane().add(btnCancelar);
		btnCancelar.addActionListener(e -> {
			setVisible(false);
			this.dispose();
		});

		btnAceptar.setBounds(720, 419, 144, 23);
		getContentPane().add(btnAceptar);

		JLabel lblFecha = new JLabel("Fecha de la visita:");
		lblFecha.setBounds(42, 45, 115, 14);
		getContentPane().add(lblFecha);

		fecha.setBounds(163, 43, 121, 20);
		fecha.setDateFormatString("dd-MM-yyyy");
		getContentPane().add(fecha);

		// Buscar las categorias
		// TODO mejorar el manejo de excepciones
		try {
			this.categorias = api.obtenerCategorias();
		} catch (BaseDeDatosException e2) {
			e2.printStackTrace();
		} catch (DataEmptyException e2) {
			e2.printStackTrace();
		}
		for (CategoriaDTO c : categorias)
			comboCategorias.addItem(c);

		txtObservacion.setBounds(404, 40, 402, 57);
		getContentPane().add(txtObservacion);

		JPanel panel = new JPanel();
		panel.setBorder(new LineBorder(new Color(0, 0, 0)));
		panel.setBounds(42, 140, 832, 269);
		getContentPane().add(panel);
		panel.setLayout(null);
		comboCategorias.setBounds(127, 31, 121, 21);
		panel.add(comboCategorias);

		JLabel lblCategoria = new JLabel("Categoria:");
		lblCategoria.setBounds(37, 35, 80, 14);
		panel.add(lblCategoria);
		panelRAEE.setBounds(17, 62, 288, 144);
		panel.add(panelRAEE);
		panelRAEE.setLayout(null);

		JLabel lblRAEEIdentificador = new JLabel("Identificador:");
		lblRAEEIdentificador.setBounds(22, 12, 80, 14);
		panelRAEE.add(lblRAEEIdentificador);

		txtRAEEId = new JTextField();
		txtRAEEId.setBounds(112, 10, 121, 20);
		panelRAEE.add(txtRAEEId);

		JLabel lblRAEENombre = new JLabel("Nombre:");
		lblRAEENombre.setBounds(22, 38, 80, 14);
		panelRAEE.add(lblRAEENombre);

		txtRAEENombre = new JTextField();
		txtRAEENombre.setBounds(112, 36, 121, 20);
		panelRAEE.add(txtRAEENombre);

		JLabel lblRAEEDescripcion = new JLabel("Descripcion:");
		lblRAEEDescripcion.setBounds(22, 64, 80, 14);
		panelRAEE.add(lblRAEEDescripcion);

		txtRAEEDesc = new JTextField();
		txtRAEEDesc.setBounds(112, 62, 121, 20);
		panelRAEE.add(txtRAEEDesc);

		JButton btnAgregarRAEE = new JButton("Agregar Residuo");
		btnAgregarRAEE.setBounds(96, 87, 137, 21);
		panelRAEE.add(btnAgregarRAEE);

		JScrollPane scrollResiduos = new JScrollPane();
		scrollResiduos.setBounds(315, 32, 507, 210);
		panel.add(scrollResiduos);

		table = new JTable();
		scrollResiduos.setViewportView(table);
		table.setModel(modelo);

		JLabel lblNewLabel = new JLabel("Residuos recolectados en la visita:");
		lblNewLabel.setBounds(315, 10, 507, 13);
		panel.add(lblNewLabel);
		lblNewLabel.setFont(new Font("Tahoma", Font.PLAIN, 12));
		panelRSU.setBounds(17, 62, 265, 144);
		panel.add(panelRSU);
		panelRSU.setLayout(null);

		JLabel lblRSUENombre = new JLabel("Nombre:");
		lblRSUENombre.setBounds(21, 12, 80, 14);
		panelRSU.add(lblRSUENombre);

		JLabel lblRSUDescripcion = new JLabel("Descripcion:");
		lblRSUDescripcion.setBounds(21, 38, 80, 14);
		panelRSU.add(lblRSUDescripcion);

		JLabel lblRSUPeso = new JLabel("Peso:");
		lblRSUPeso.setBounds(21, 64, 80, 14);
		panelRSU.add(lblRSUPeso);

		JButton btnAgregarRSU = new JButton("Agregar Residuo");
		btnAgregarRSU.setBounds(96, 87, 137, 21);
		panelRSU.add(btnAgregarRSU);

		txtRSUNombre = new JTextField();
		txtRSUNombre.setBounds(112, 10, 121, 20);
		panelRSU.add(txtRSUNombre);

		txtRSUDesc = new JTextField();
		txtRSUDesc.setBounds(112, 36, 121, 20);
		panelRSU.add(txtRSUDesc);

		txtRSUPeso = new JTextField();
		txtRSUPeso.setBounds(112, 62, 121, 20);
		panelRSU.add(txtRSUPeso);

		JLabel lblInsertarResiduoRecolectado = new JLabel("Insertar Residuo recolectado:");
		lblInsertarResiduoRecolectado.setBounds(15, 11, 290, 13);
		panel.add(lblInsertarResiduoRecolectado);
		lblInsertarResiduoRecolectado.setFont(new Font("Tahoma", Font.PLAIN, 12));
		this.panelRSU.setVisible(false);

		btnAgregarRSU.addActionListener(e -> {
			try {
				cargarRSU();
				limpiarCamposRSU();
				reloadGrid(api);
			} catch (NumberFormatException n) {
				JOptionPane.showMessageDialog(this, "El peso no debe estar vacio y debe contener solo n�meros.");
			} catch (DataEmptyException e1) {
				JOptionPane.showMessageDialog(this, e1.getMessage(), "Error", JOptionPane.ERROR_MESSAGE);
			}
		});

		this.panelRAEE.setVisible(false);

		btnAgregarRAEE.addActionListener(e -> {
			try {
				cargarRAEE();
				limpiarCamposRAEE();
				reloadGrid(api);
			} catch (DataEmptyException e1) {
				JOptionPane.showMessageDialog(this, e1.getMessage(), "Error", JOptionPane.ERROR_MESSAGE);
			}
		});

		comboCategorias.addItemListener(new ItemListener() {
			public void itemStateChanged(ItemEvent e) {
				habilitarPanel();
			}
		});

		btnAceptar.addActionListener(e -> {
			try {
				// Genero una nueva visita
				api.registrarVisita(cargarVisita(), id_orden);
				JOptionPane.showMessageDialog(null, "Visita registrada con exito!", "Info",
						JOptionPane.INFORMATION_MESSAGE);
				setVisible(false);
				dispose();
			} catch (BaseDeDatosException | RegistroVisitaException e1) {
				JOptionPane.showMessageDialog(this, e1.getMessage());
			} catch (DataEmptyException e1) {
				JOptionPane.showMessageDialog(null, e1.getMessage(), "Error", JOptionPane.ERROR_MESSAGE);
			}
		});
	}

	private void habilitarPanel() {
		if (((CategoriaDTO) comboCategorias.getSelectedItem()).esRAEE()) {
			this.panelRAEE.setVisible(true);
			this.panelRSU.setVisible(false);
		} else {
			if (((CategoriaDTO) comboCategorias.getSelectedItem()).esRSU()) {
				this.panelRAEE.setVisible(false);
				this.panelRSU.setVisible(true);
			} else {
				this.panelRAEE.setVisible(false);
				this.panelRSU.setVisible(false);
			}
		}

	}

	private void cargarResiduos() {
		for (ResiduoDTO residuo : lisResiduos) {
			modelo.addRow(new Object[] { residuo.verCategoria().obtenerTipo(), residuo.verCategoria(),
					residuo.getNombre(), residuo.getDescripcion() });
		}
	}

	private VisitaDTO cargarVisita() throws DataEmptyException {
		Date fecha = this.fecha.getDate();
		LocalDate fechalocal = fecha.toInstant().atZone(ZoneId.systemDefault()).toLocalDate();

		VisitaDTO visita = new VisitaDTO(fechalocal, this.txtObservacion.getText(), this.lisResiduosRAEE,
				this.lisResiduosRSU);
		return visita;
	}

	private void cargarRSU() throws DataEmptyException {
		String nombre = txtRSUNombre.getText();
		CategoriaDTO categoria = (CategoriaDTO) comboCategorias.getSelectedItem();
		String descripcion = txtRSUDesc.getText();
		int peso = Integer.parseInt(txtRSUPeso.getText());
		lisResiduosRSU.add(new RSUDTO(nombre, descripcion, categoria, peso));
		lisResiduos.add(new ResiduoDTO(nombre, descripcion, categoria));
	}

	private void cargarRAEE() throws DataEmptyException {
		String nombre = txtRAEENombre.getText();
		String descripcion = txtRAEEDesc.getText();
		CategoriaDTO categoria = (CategoriaDTO) comboCategorias.getSelectedItem();
		String identificador = txtRAEEId.getText();

		lisResiduosRAEE.add(new RAEEDTO(identificador, nombre, descripcion, categoria));
		lisResiduos.add(// nombre, observ, cat,
				new ResiduoDTO(txtRAEENombre.getText(), txtRAEEDesc.getText(),
						(CategoriaDTO) comboCategorias.getSelectedItem()));
	}

	private void limpiarCamposRAEE() {
		this.txtRAEEDesc.setText("");
		this.txtRAEEId.setText("");
		this.txtRAEENombre.setText("");
	}

	private void limpiarCamposRSU() {
		this.txtRSUDesc.setText("");
		this.txtRSUNombre.setText("");
		this.txtRSUPeso.setText("");
	}

	private void reloadGrid(IApi api) {
		DefaultTableModel modelo = (DefaultTableModel) table.getModel();
		modelo.setRowCount(0);
		cargarResiduos();
	}
}
