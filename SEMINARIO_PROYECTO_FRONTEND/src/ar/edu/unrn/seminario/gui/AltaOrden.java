package ar.edu.unrn.seminario.gui;

import java.awt.Color;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextArea;
import javax.swing.border.EtchedBorder;
import javax.swing.table.DefaultTableModel;

import ar.edu.unrn.seminario.api.IApi;
import ar.edu.unrn.seminario.dto.PedidoDTO;
import ar.edu.unrn.seminario.dto.RecolectorDTO;
import ar.edu.unrn.seminario.exception.BaseDeDatosException;
import ar.edu.unrn.seminario.exception.DataEmptyException;
import ar.edu.unrn.seminario.exception.DniInvalidException;
import ar.edu.unrn.seminario.exception.StateException;

import javax.swing.JComboBox;
import java.awt.Font;

public class AltaOrden extends JFrame {

	private static final long serialVersionUID = 1L;
	private JTable tabla = new JTable();
	private boolean seleccionPedidos= false;
	private boolean seleccionRecolectores= false;
	private Object[] titulos = { "Fecha", "Direccion", "Carga Pesada", "Observacion" };
	private DefaultTableModel modelo = new DefaultTableModel(titulos, 0) {
		private static final long serialVersionUID = 1L;

		public Class<?> getColumnClass(int column) {
			return column == 2 ? Boolean.class : super.getColumnClass(column);
		}
		
		@Override
		public boolean isCellEditable(int row, int column) {
			return false;
		}
	};
	private List<RecolectorDTO> recolectores = new ArrayList<>();
	private List<PedidoDTO> pedidos = new ArrayList<>();

	private JPanel panelBotones = new JPanel();
	private JButton btnCrear = new JButton("Generar Orden");
	private JButton btnCancelar = new JButton("Cancelar");
	private final JScrollPane scrollPane = new JScrollPane();
	private final JScrollPane scrollPane_1 = new JScrollPane();

	public AltaOrden(IApi api) {
		setTitle("Alta de Orden");
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setBounds(100, 100, 850, 437);
		setResizable(false);
		try {
			recolectores = api.obtenerRecolectores();
		} catch (BaseDeDatosException | DataEmptyException | DniInvalidException e) {
			JOptionPane.showMessageDialog(this, e.getMessage());
		}
		
		try {
			pedidos = api.obtenerPedidosSinOrden();
		} catch (BaseDeDatosException | DataEmptyException e) {
			JOptionPane.showMessageDialog(this, e.getMessage());
		}


		cargarPedidos();
		getContentPane().setLayout(null);
		panelBotones.setBounds(10, 349, 803, 42);

		panelBotones.setBorder(new EtchedBorder(EtchedBorder.LOWERED, null, null));
		getContentPane().add(panelBotones);
		panelBotones.setLayout(null);
		btnCrear.setBounds(497, 11, 182, 23);
		panelBotones.add(btnCrear);
		btnCancelar.setBounds(689, 11, 89, 23);
		panelBotones.add(btnCancelar);
		btnCancelar.addActionListener(e -> {
			setVisible(false);
			this.dispose();
		});
		scrollPane.setBounds(0, 24, 618, 319);

		getContentPane().add(scrollPane);
		tabla.setModel(modelo);
		scrollPane.setViewportView(tabla);

		scrollPane_1.setBounds(171, 223, 263, -221);
		getContentPane().add(scrollPane_1);
		JLabel lblRecolectores = new JLabel("Asignar a Recolector:");
		lblRecolectores.setFont(new Font("Tahoma", Font.PLAIN, 12));
		getContentPane().add(lblRecolectores);
		lblRecolectores.setBackground(new Color(240, 240, 240));
		lblRecolectores.setBounds(628, 11, 163, 14);

		/*JTextArea textlistapedidos = new JTextArea();
		textlistapedidos.setText("Lista Pedidos");
		textlistapedidos.setBounds(0, 2, 505, 22);
		getContentPane().add(textlistapedidos);*/
		
		JLabel lblPedidos = new JLabel("Lista de Pedidos. Seleccione uno para generar la Orden:");
		lblPedidos.setFont(new Font("Tahoma", Font.PLAIN, 12));
		lblPedidos.setBounds(26, -1, 395, 25);
		getContentPane().add(lblPedidos);

		JComboBox comboBox = new JComboBox();
		comboBox.setBounds(623, 31, 201, 22);
		getContentPane().add(comboBox);
		
		for (RecolectorDTO r : recolectores) {
			comboBox.addItem(r);
		}

		tabla.addMouseListener(new MouseAdapter() {
			public void mouseClicked(MouseEvent arg0) {
				//habilitarBotones(true);
				seleccionRecolectores();
				seleccionPedidoYRecolectores();
			}
		});
		
		comboBox.addMouseListener(new MouseAdapter() {
			public void mouseClicked(MouseEvent arg0) {				
				//habilitarBotones(true);
				seleccionPedido();
				seleccionPedidoYRecolectores();
			}	
		});
		
		btnCrear.addActionListener(e -> {
			try {				
				api.registrarOrden( pedidos.get(tabla.getSelectedRow()).getIdPedido() , recolectores.get(comboBox.getSelectedIndex()).getId());
				api.pedidoEstadoGenerada ( pedidos.get(tabla.getSelectedRow()).getIdPedido());
				JOptionPane.showMessageDialog(null, "Orden registrada con exito!", "Info",
						JOptionPane.INFORMATION_MESSAGE);
				setVisible(false);
				dispose();
			} catch (BaseDeDatosException  | StateException e1) {
				JOptionPane.showMessageDialog(this, e1.getMessage());
			} catch (DataEmptyException e1) {
				JOptionPane.showMessageDialog(null, "La Orden debe tener " + e1.getMessage(), "Error",
						JOptionPane.ERROR_MESSAGE);
			}
		});
		
		habilitarBotones(false);
	}

	
	private void habilitarBotones(boolean b) {
		btnCrear.setEnabled(b);
		btnCancelar.setEnabled(b);
	}

	private void seleccionPedidoYRecolectores() {
		tabla.getRowSelectionAllowed();
		
		if(seleccionPedidos==true && seleccionRecolectores==true) {
			habilitarBotones(true);
		}
	}
	
	private void seleccionPedido() {
		seleccionPedidos=true;		
	}
	
	private void seleccionRecolectores() {
		seleccionRecolectores=true;		
	}
	
	private void cargarPedidos() {
		for (int i = 0; i < pedidos.size(); i++) {
			modelo.addRow(new Object[] { pedidos.get(i).getFecha(), pedidos.get(i).getDireccion(),
					pedidos.get(i).getCargaPesada(), pedidos.get(i).getObservacion() });
		}
	}
}

