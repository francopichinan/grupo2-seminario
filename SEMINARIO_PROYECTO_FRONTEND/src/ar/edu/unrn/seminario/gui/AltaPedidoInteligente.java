package ar.edu.unrn.seminario.gui;

import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;
import javax.swing.table.DefaultTableModel;

import ar.edu.unrn.seminario.api.IApi;
import ar.edu.unrn.seminario.dto.CategoriaDTO;
import ar.edu.unrn.seminario.dto.PuntoDTO;
import ar.edu.unrn.seminario.dto.RecolectorDTO;
import ar.edu.unrn.seminario.dto.ResiduoDTO;
import ar.edu.unrn.seminario.exception.BaseDeDatosException;
import ar.edu.unrn.seminario.exception.DataEmptyException;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JPanel;
import javax.swing.SwingConstants;
import javax.swing.border.EtchedBorder;
import javax.swing.JTextArea;
import javax.swing.JSpinner;

public class AltaPedidoInteligente extends JFrame {

	private static final long serialVersionUID = 1L;
	private JComboBox comboBoxResiduos = new JComboBox();

	private JLabel lblCargaPesada = new JLabel("Requiere vehiculo para carga pesada.");
	private JCheckBox checkRequiereCarga = new JCheckBox();

	private JLabel lblObservacion = new JLabel("Observacion:");

	private JButton btnCancelar = new JButton("Cancelar");
	private JButton btnAceptar = new JButton("Aceptar");
	private JTextField txtCantidad;
	private List<PuntoDTO> puntos = new ArrayList<>();
	List<ResiduoDTO> lisResiduos = new ArrayList<ResiduoDTO>();

	private JTable tabla = new JTable();
	private Object[] titulos = { "Tipo", "Cantidad" };
	private DefaultTableModel modelo = new DefaultTableModel(titulos, 0) {
		private static final long serialVersionUID = 1L;
		
		@Override
		public boolean isCellEditable(int row, int column) {
			return false;
		}
	};
	private final JPanel panelDatos = new JPanel();
	private final JPanel panelResiduos = new JPanel();
	private final JPanel panelPunto = new JPanel();

	public AltaPedidoInteligente(IApi api) {

		setTitle("Alta Pedido - Punto Inteligente");
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setBounds(100, 100, 659, 501);
		getContentPane().setLayout(null);
		try {
			puntos = api.obtenerPuntosInteligentes();
		} catch (BaseDeDatosException e2) {
			JOptionPane.showMessageDialog(this, e2.getMessage());
		}

		// Carga del comboBox
		try {
			List<CategoriaDTO> listaCategoriaDTO = api.obtenerCategorias();
			for (CategoriaDTO categoria : listaCategoriaDTO) {
				comboBoxResiduos.addItem(categoria);
			}
		} catch (BaseDeDatosException | DataEmptyException e1) {
			JOptionPane.showMessageDialog(this, e1.getMessage());
		}

		JPanel panelBotones = new JPanel();
		panelBotones.setBorder(new EtchedBorder(EtchedBorder.LOWERED, null, null));
		panelBotones.setBounds(10, 391, 623, 61);
		getContentPane().add(panelBotones);
		panelBotones.setLayout(null);
		btnCancelar.setBounds(403, 23, 89, 23);
		panelBotones.add(btnCancelar);
		btnAceptar.setBounds(502, 23, 89, 23);
		panelBotones.add(btnAceptar);
		panelDatos.setBounds(21, 286, 601, 105);

		getContentPane().add(panelDatos);
		panelDatos.setLayout(null);
		lblCargaPesada.setBounds(105, 10, 230, 29);
		panelDatos.add(lblCargaPesada);
		checkRequiereCarga.setBounds(62, 16, 28, 23);
		panelDatos.add(checkRequiereCarga);
		lblObservacion.setBounds(30, 46, 92, 14);
		panelDatos.add(lblObservacion);

		JTextArea textAreaObservacion = new JTextArea();
		textAreaObservacion.setBounds(132, 44, 390, 49);
		panelDatos.add(textAreaObservacion);
		textAreaObservacion.setLineWrap(true);
		textAreaObservacion.setWrapStyleWord(true);
		panelResiduos.setBounds(21, 59, 601, 228);

		getContentPane().add(panelResiduos);
		panelResiduos.setLayout(null);

		JScrollPane scrollResiduos = new JScrollPane();
		scrollResiduos.setBounds(293, 28, 229, 166);
		panelResiduos.add(scrollResiduos);

		tabla.setModel(modelo);
		scrollResiduos.setViewportView(tabla);

		JLabel lblCantidad = new JLabel("Cantidad:");
		lblCantidad.setBounds(27, 93, 85, 13);
		panelResiduos.add(lblCantidad);

		txtCantidad = new JTextField();
		txtCantidad.setBounds(142, 88, 121, 22);
		panelResiduos.add(txtCantidad);
		comboBoxResiduos.setBounds(142, 59, 122, 22);
		panelResiduos.add(comboBoxResiduos);

		JLabel lblResiduos = new JLabel("Residuo a retirar:");
		lblResiduos.setBounds(27, 64, 122, 13);
		panelResiduos.add(lblResiduos);

		JButton btnAgregarResiduo = new JButton("Agregar");
		btnAgregarResiduo.setBounds(178, 121, 85, 21);
		panelResiduos.add(btnAgregarResiduo);

		JLabel lblResiduosTabla = new JLabel("Residuos incluidos en el pedido:");
		lblResiduosTabla.setBounds(293, 5, 229, 13);
		panelResiduos.add(lblResiduosTabla);
		
		getContentPane().add(panelPunto);
		panelPunto.setLayout(null);

		JComboBox<PuntoDTO> comboBoxPunto = new JComboBox<PuntoDTO>();
		comboBoxPunto.setBounds(161, 17, 400, 22);
		panelPunto.add(comboBoxPunto);

		for (PuntoDTO r : puntos) {
			comboBoxPunto.addItem(r);
		}

		comboBoxPunto.addMouseListener(new MouseAdapter() {
			public void mouseClicked(MouseEvent arg0) {
			}
		});

		JLabel lblDireccion = new JLabel("Direccion de punto: ");
		lblDireccion.setBounds(10, 22, 130, 13);
		panelPunto.add(lblDireccion);
		btnAgregarResiduo.addActionListener(e -> {
			try {
				String cantidad = txtCantidad.getText();
				txtCantidad.setText("");
				lisResiduos.add(
						new ResiduoDTO(Integer.parseInt(cantidad), (CategoriaDTO) comboBoxResiduos.getSelectedItem()));
				reloadGrid(api);
			} catch (NumberFormatException e1) {
				JOptionPane.showMessageDialog(null,
						"Formato inválido en campo Cantidad, no debe estar vacio y debe ser positivo.", "Error",
						JOptionPane.ERROR_MESSAGE);
			}
		});

		JButton btnDelete = new JButton("Eliminar");
		btnDelete.setBounds(433, 194, 89, 23);
		panelResiduos.add(btnDelete);
		panelPunto.setBounds(21, 10, 601, 49);
		
		btnDelete.addActionListener(e -> {
			lisResiduos.remove(tabla.getSelectedRow());
			reloadGrid(api);
		});
		
		btnAceptar.addActionListener(e -> {
			try {
				api.registrarPedido(checkRequiereCarga.isSelected(), textAreaObservacion.getText(), lisResiduos,
						puntos.get(comboBoxPunto.getSelectedIndex()));
				JOptionPane.showMessageDialog(null, "Pedido inteligente registrado con exito!", "Info",
						JOptionPane.INFORMATION_MESSAGE);
				setVisible(false);
				dispose();
			} catch (BaseDeDatosException e1) {
				JOptionPane.showMessageDialog(this, e1.getMessage());
			} catch (DataEmptyException e1) {
				JOptionPane.showMessageDialog(null, "El pedido debe tener " + e1.getMessage(), "Error",
						JOptionPane.ERROR_MESSAGE);
			}
		});
		btnCancelar.addActionListener(e -> {
			setVisible(false);
			this.dispose();
		});
	}

	private void cargarResiduos() {
		for (ResiduoDTO residuo : lisResiduos) {
			modelo.addRow(new Object[] { residuo.verCategoria(), residuo.verCantidad() });
		}
	}

	private void reloadGrid(IApi api) {
		DefaultTableModel modelo = (DefaultTableModel) tabla.getModel();
		modelo.setRowCount(0);
		cargarResiduos();
	}
}
