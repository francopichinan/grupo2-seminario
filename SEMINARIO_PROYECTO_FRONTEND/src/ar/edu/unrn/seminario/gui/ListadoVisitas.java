package ar.edu.unrn.seminario.gui;

import java.awt.BorderLayout;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.border.EtchedBorder;
import javax.swing.table.DefaultTableModel;

import ar.edu.unrn.seminario.api.IApi;
import ar.edu.unrn.seminario.dto.OrdenDTO;
import ar.edu.unrn.seminario.dto.VisitaDTO;
import ar.edu.unrn.seminario.exception.BaseDeDatosException;

public class ListadoVisitas extends JFrame {

	private static final long serialVersionUID = 1L;
	private JScrollPane scrollPane = new JScrollPane();
	private JTable tabla = new JTable();
	private Object[] titulos = {"Fecha", "Observacion"};
	private DefaultTableModel modelo = new DefaultTableModel(titulos, 0);
	private List<VisitaDTO> visitas = new ArrayList<>();
	
	private JPanel panelBotones = new JPanel();
	private JButton btnEditar = new JButton("Editar");
	private JButton btnEliminar = new JButton("Eliminar");
	private final JButton btnResiduos = new JButton("Listar residuos");
	
	public ListadoVisitas(IApi api, OrdenDTO orden) {
		setTitle("Listado visitas");
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setBounds(100, 100, 450, 300);
		getContentPane().setLayout(new BorderLayout());
		
		try {
			visitas = api.obtenerVisitasPorOrdenId(orden.getId_orden());
		} catch (BaseDeDatosException e) {
			JOptionPane.showMessageDialog(this, e.getMessage());
		}
		cargarVisitas();
		
		tabla.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent arg0) {
				habilitarBotones(true);
			}
		});
		
		panelBotones.setBorder(new EtchedBorder(EtchedBorder.LOWERED, null, null));
		getContentPane().add(panelBotones, BorderLayout.SOUTH);
		//panelBotones.add(btnEditar);
		//panelBotones.add(btnEliminar);
		
	//	panelBotones.add(btnResiduos);
		
		btnEditar.addActionListener(e -> {
			LocalDate fecha = (LocalDate) tabla.getModel().getValueAt(tabla.getSelectedRow(), 0);
			String observacion = (String) tabla.getModel().getValueAt(tabla.getSelectedRow(), 1);
			VisitaDTO visitaDTO = new VisitaDTO( fecha,  observacion);
			//EditarRecolector editar = new EditarVisita(api, visitaDTO);
			//editar.setLocationRelativeTo(null);
			//editar.setVisible(true);
		});
		
		btnEliminar.addActionListener(e -> {
			int reply = JOptionPane.showConfirmDialog(null,
					"Esta seguro que desea eliminar la visita?", "Confirmar eliminación.",
					JOptionPane.YES_NO_OPTION);
			if (reply == JOptionPane.YES_OPTION) {
				try {
					api.eliminarRecolector(visitas.get(tabla.getSelectedRow()).getId());
					//reloadGrid(api);
				} catch (BaseDeDatosException e1) {
					JOptionPane.showMessageDialog(this, e1.getMessage());
				}
			}
		});
		
		btnResiduos.addActionListener(e -> {
			ListarResiduosRetirados ventanaListarRR = new ListarResiduosRetirados(visitas.get(tabla.getSelectedRow()));
			ventanaListarRR.setVisible(true);
		});
		
		habilitarBotones(false);
		
		getContentPane().add(scrollPane, BorderLayout.CENTER);
		tabla.setModel(modelo);
		scrollPane.setViewportView(tabla);
		
	}
	
	private void habilitarBotones(boolean b) {
		btnEditar.setEnabled(b);
		btnEliminar.setEnabled(b);
	}
	
	private void cargarVisitas() {
		for (int i=0; i<visitas.size(); i++) {
			modelo.addRow(new Object[] {visitas.get(i).getFecha(), visitas.get(i).getObservacion() } );
		}
	}
	
	private void reloadGrid(IApi api) throws BaseDeDatosException {
		DefaultTableModel modelo = (DefaultTableModel) tabla.getModel();
		visitas = api.obtenerVisitas();
		modelo.setRowCount(0);
		cargarVisitas();
	}
}
