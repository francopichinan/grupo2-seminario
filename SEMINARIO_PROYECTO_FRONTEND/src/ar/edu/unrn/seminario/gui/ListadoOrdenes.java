package ar.edu.unrn.seminario.gui;

import java.awt.BorderLayout;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.WindowEvent;
import java.awt.event.WindowFocusListener;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.border.BevelBorder;
import javax.swing.border.EtchedBorder;
import javax.swing.border.SoftBevelBorder;
import javax.swing.table.DefaultTableModel;

import ar.edu.unrn.seminario.api.IApi;
import ar.edu.unrn.seminario.dto.OrdenDTO;
import ar.edu.unrn.seminario.exception.BaseDeDatosException;
import ar.edu.unrn.seminario.exception.DataEmptyException;
import ar.edu.unrn.seminario.exception.DniInvalidException;
import ar.edu.unrn.seminario.exception.StateException;

public class ListadoOrdenes extends JFrame {

	private static final long serialVersionUID = 1L;
	private JScrollPane scrollPane = new JScrollPane();
	private JTable tabla = new JTable();
	private Object[] titulos = {"Direccion de Punto", "Nombre Recolector", "Fecha", "Estado" };
	private DefaultTableModel modelo = new DefaultTableModel(titulos, 0) {
		private static final long serialVersionUID = 1L;
		
		@Override
		public boolean isCellEditable(int row, int column) {
			return false;
		}
	};
	private List<OrdenDTO> ordenes = new ArrayList<>();

	private JPanel panelBotones = new JPanel();
	//private JButton btnEditar = new JButton("Editar");
	//private JButton btnEliminar = new JButton("Eliminar");
	private JButton btnCrearVisita = new JButton("Crear Visita");
	private JButton btnListarVisitas = new JButton("Listar Visitas");
	private JButton btnFinalizarOrden = new JButton("Finalizar orden");
	private JButton btnCancelarOrden = new JButton("Cancelar orden");

	public ListadoOrdenes(IApi api) {
		setTitle("Listado Ordenes");
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setBounds(100, 100, 653, 300);
		getContentPane().setLayout(new BorderLayout());

		addWindowFocusListener(new WindowFocusListener() {
			public void windowGainedFocus(WindowEvent e) {
				reloadGrid(api);
			}
			public void windowLostFocus(WindowEvent e) {
			}
		});
		
		try {
			ordenes = api.obtenerOrdenes();
		}  catch (BaseDeDatosException | DataEmptyException | DniInvalidException e) {
			JOptionPane.showMessageDialog(this, e.getMessage());
		}
		cargarOrdenes();
		tabla.setBorder(new SoftBevelBorder(BevelBorder.LOWERED, null, null, null, null));

		tabla.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent arg0) {
				habilitarBotones(true);
			}
		});

		panelBotones.setBorder(new EtchedBorder(EtchedBorder.LOWERED, null, null));
		getContentPane().add(panelBotones, BorderLayout.SOUTH);
		//panelBotones.add(btnEditar);
		//panelBotones.add(btnEliminar);
		panelBotones.add(btnCrearVisita);
		panelBotones.add(btnListarVisitas);
		panelBotones.add(btnFinalizarOrden);
		panelBotones.add(btnCancelarOrden);

		btnCrearVisita.addActionListener(e -> {
			AltaVisita alta = new AltaVisita(api, ordenes.get(tabla.getSelectedRow()).getId_orden());
			alta.setLocationRelativeTo(null);
			alta.setVisible(true);
		});
		
		btnListarVisitas.addActionListener(e -> {
			ListadoVisitas listadoVisitas = new ListadoVisitas(api,ordenes.get(tabla.getSelectedRow()));
			listadoVisitas.setLocationRelativeTo(null);
			listadoVisitas.setVisible(true);
		});
		
		btnFinalizarOrden.addActionListener(e -> {
			try {
				api.finalizarOrden(ordenes.get(tabla.getSelectedRow()).getId_orden());
				reloadGrid(api);
			} catch (StateException | BaseDeDatosException e1) {
				JOptionPane.showMessageDialog(this, e1.getMessage());
			}
		});
		
		btnCancelarOrden.addActionListener(e -> {
			try {
				api.cancelarOrden(ordenes.get(tabla.getSelectedRow()).getId_orden());
				reloadGrid(api);
			} catch (StateException | BaseDeDatosException e1) {
				JOptionPane.showMessageDialog(this, e1.getMessage());
			}
		});
		
		habilitarBotones(false);

		getContentPane().add(scrollPane, BorderLayout.CENTER);
		tabla.setModel(modelo);
		scrollPane.setViewportView(tabla);
		//tabla.setEnabled(false);
	}

	private void habilitarBotones(boolean b) {
		//btnEditar.setEnabled(b);
		//btnEliminar.setEnabled(b);
		btnCrearVisita.setEnabled(b);
		btnListarVisitas.setEnabled(b);
		btnFinalizarOrden.setEnabled(b);
		btnCancelarOrden.setEnabled(b);
	}

	private void cargarOrdenes() {
		for (int i = 0; i < ordenes.size(); i++) {
			modelo.addRow(new Object[] {ordenes.get(i).getPedido().getDireccion(), ordenes.get(i).getRecolector().getNombre() + " " + ordenes.get(i).getRecolector().getApellido(), ordenes.get(i).getFecha(), ordenes.get(i).getEstado() });
		}
	}
	
	private void reloadGrid(IApi api) {
		DefaultTableModel modelo = (DefaultTableModel) tabla.getModel();
		try {
			ordenes = api.obtenerOrdenes();
		} catch (BaseDeDatosException | DataEmptyException | DniInvalidException e1) {
			JOptionPane.showMessageDialog(this, e1.getMessage());
		}
		modelo.setRowCount(0);
		cargarOrdenes();
	}
}
