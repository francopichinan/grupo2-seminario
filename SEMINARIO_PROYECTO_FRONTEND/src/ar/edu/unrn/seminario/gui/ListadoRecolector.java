package ar.edu.unrn.seminario.gui;

import java.awt.BorderLayout;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.border.EtchedBorder;
import javax.swing.table.DefaultTableModel;

import ar.edu.unrn.seminario.api.IApi;
import ar.edu.unrn.seminario.dto.RecolectorDTO;
import ar.edu.unrn.seminario.exception.BaseDeDatosException;
import ar.edu.unrn.seminario.exception.DataEmptyException;
import ar.edu.unrn.seminario.exception.DniInvalidException;
import java.awt.event.WindowFocusListener;
import java.awt.event.WindowEvent;

public class ListadoRecolector extends JFrame {

	private static final long serialVersionUID = 1L;
	private JScrollPane scrollPane = new JScrollPane();
	private JTable tabla = new JTable();
	private Object[] titulos = {"Nombre", "Apellido", "Dni"};
	private DefaultTableModel modelo = new DefaultTableModel(titulos, 0) {
		private static final long serialVersionUID = 1L;
		
		@Override
		public boolean isCellEditable(int row, int column) {
			return false;
		}
	};
	private List<RecolectorDTO> recolectores = new ArrayList<>();
	
	private JPanel panelBotones = new JPanel();
	private JButton btnEditar = new JButton("Editar");
	private JButton btnEliminar = new JButton("Eliminar");
	//private JButton btnRefrescar = new JButton("Refrescar");
	
	public ListadoRecolector(IApi api) {
		setTitle("Listado recolectores");
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setBounds(100, 100, 450, 300);
		getContentPane().setLayout(new BorderLayout());
		
		addWindowFocusListener(new WindowFocusListener() {
			public void windowGainedFocus(WindowEvent e) {
				reloadGrid(api);
			}
			public void windowLostFocus(WindowEvent e) {
			}
		});
		
		try {
			recolectores = api.obtenerRecolectores();
		} catch (BaseDeDatosException | DataEmptyException | DniInvalidException e) {
			JOptionPane.showMessageDialog(this, e.getMessage());
		}
		cargarRecolectores();
		
		tabla.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent arg0) {
				habilitarBotones(true);
			}
		});
		
		panelBotones.setBorder(new EtchedBorder(EtchedBorder.LOWERED, null, null));
		getContentPane().add(panelBotones, BorderLayout.SOUTH);
		panelBotones.add(btnEditar);
		panelBotones.add(btnEliminar);
		habilitarBotones(false);
		
		btnEditar.addActionListener(e -> {
			String nombre = (String) tabla.getModel().getValueAt(tabla.getSelectedRow(), 0);
			String apellido = (String) tabla.getModel().getValueAt(tabla.getSelectedRow(), 1);
			String dni = (String) tabla.getModel().getValueAt(tabla.getSelectedRow(), 2);
			RecolectorDTO recolectorDTO = new RecolectorDTO( recolectores.get(tabla.getSelectedRow()).getId() , dni, nombre, apellido);
			EditarRecolector editar = new EditarRecolector(api, recolectorDTO);
			editar.setLocationRelativeTo(null);
			editar.setVisible(true);
		});
		
		btnEliminar.addActionListener(e -> {
			int reply = JOptionPane.showConfirmDialog(null,
					"Esta seguro que desea eliminar el recolector?", "Confirmar eliminación.",
					JOptionPane.YES_NO_OPTION);
			if (reply == JOptionPane.YES_OPTION) {
				try {
					//Long id = (Long) tabla.getModel().getValueAt(tabla.getSelectedRow(), 0);
					api.eliminarRecolector(recolectores.get(tabla.getSelectedRow()).getId());
					reloadGrid(api);
				} catch (BaseDeDatosException e1) {
					JOptionPane.showMessageDialog(this, e1.getMessage());
				}
			}
		});
		
		getContentPane().add(scrollPane, BorderLayout.CENTER);
		tabla.setModel(modelo);
		scrollPane.setViewportView(tabla);
	}
	
	private void habilitarBotones(boolean b) {
		btnEditar.setEnabled(b);
		btnEliminar.setEnabled(b);
	}
	
	private void cargarRecolectores() {
		for (int i=0; i<recolectores.size(); i++) 
			modelo.addRow(new Object[] { recolectores.get(i).getNombre(), recolectores.get(i).getApellido(),recolectores.get(i).getDni() } );
	}
	
	private void reloadGrid(IApi api)  {
		DefaultTableModel modelo = (DefaultTableModel) tabla.getModel();
		try {
			recolectores = api.obtenerRecolectores();
		} catch (BaseDeDatosException | DataEmptyException | DniInvalidException e1) {
			JOptionPane.showMessageDialog(this, e1.getMessage());
		}
		modelo.setRowCount(0);
		cargarRecolectores();
		habilitarBotones(false);
	}
}
