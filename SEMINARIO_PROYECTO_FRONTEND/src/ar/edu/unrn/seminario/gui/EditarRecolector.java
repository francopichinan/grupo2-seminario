package ar.edu.unrn.seminario.gui;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;

import ar.edu.unrn.seminario.api.IApi;
import ar.edu.unrn.seminario.dto.RecolectorDTO;
import ar.edu.unrn.seminario.exception.BaseDeDatosException;
import ar.edu.unrn.seminario.exception.DataEmptyException;
import ar.edu.unrn.seminario.exception.DniInvalidException;

public class EditarRecolector extends JFrame {
	
	private static final long serialVersionUID = 1L;
	private JLabel lblNombre = new JLabel("Nombre:");
	private JLabel lblApellido = new JLabel("Apellido:");
	private JLabel lblDni = new JLabel("Dni:");
	
	private JTextField textNombre = new JTextField();
	private JTextField textApellido = new JTextField();
	private JTextField textDni = new JTextField();
	
	private JButton btnCancelar = new JButton("Cancelar");
	private JButton btnAceptar = new JButton("Aceptar");
	
	public EditarRecolector(IApi api, RecolectorDTO recolectorDTO) {
		setTitle("Editar Recolector");
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setBounds(100, 100, 450, 300);
		getContentPane().setLayout(null);
		
		lblNombre.setBounds(10, 25, 59, 14);
		getContentPane().add(lblNombre);
		
		textNombre.setBounds(102, 20, 100, 25);
		getContentPane().add(textNombre);
		textNombre.setText(recolectorDTO.getNombre());
		
		lblApellido.setBounds(10, 76, 59, 14);
		getContentPane().add(lblApellido);
		
		textApellido.setBounds(102, 71, 100, 25);
		getContentPane().add(textApellido);
		textApellido.setText(recolectorDTO.getApellido());
		
		lblDni.setBounds(10, 130, 46, 14);
		getContentPane().add(lblDni);
		
		textDni.setBounds(102, 125, 100, 25);
		getContentPane().add(textDni);
		textDni.setText(recolectorDTO.getDni());
		
		btnCancelar.setBounds(236, 227, 89, 23);
		getContentPane().add(btnCancelar);
		btnCancelar.addActionListener(e -> {
			setVisible(false);
			this.dispose();
		});

		btnAceptar.setBounds(335, 227, 89, 23);
		getContentPane().add(btnAceptar);
		btnAceptar.addActionListener(e -> {
			try {
				api.modificarRecolector(recolectorDTO.getId(), textDni.getText(), textNombre.getText(), textApellido.getText());
				JOptionPane.showMessageDialog(null, "Recolector modificado con exito!", "Info",
						JOptionPane.INFORMATION_MESSAGE);
				setVisible(false);
				dispose();
			} catch (BaseDeDatosException e1) {
				JOptionPane.showMessageDialog(this, e1.getMessage());
			} catch (DataEmptyException e1) {
				JOptionPane.showMessageDialog(null, "El recolector debe tener " + e1.getMessage(), "Error",
						JOptionPane.ERROR_MESSAGE);
			} catch (DniInvalidException e1) {
				JOptionPane.showMessageDialog(null, e1.getMessage(), "Error", JOptionPane.ERROR_MESSAGE);
			}
		});
	}
}
