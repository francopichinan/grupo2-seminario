package ar.edu.unrn.seminario.accesos;

import java.util.List;

import ar.edu.unrn.seminario.exception.BaseDeDatosException;
import ar.edu.unrn.seminario.modelo.Pedido;
import ar.edu.unrn.seminario.modelo.PuntoFijo;

public interface PedidoDAO {

	void create(Pedido pedido) throws BaseDeDatosException;

	void update(Pedido pedido) throws BaseDeDatosException;

	void remove(Long id) throws BaseDeDatosException;

	void remove(Pedido pedido) throws BaseDeDatosException;

	Pedido find(Integer id) throws BaseDeDatosException;

	List<Pedido> findAll() throws BaseDeDatosException;

	List<Pedido> findSinOrdenConPunto() throws BaseDeDatosException;

	List<Pedido> findAllByPuntofijo(PuntoFijo punto) throws BaseDeDatosException;
}
