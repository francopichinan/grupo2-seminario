package ar.edu.unrn.seminario.accesos;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

import ar.edu.unrn.seminario.exception.BaseDeDatosException;
import ar.edu.unrn.seminario.exception.DataEmptyException;
import ar.edu.unrn.seminario.exception.NotNullException;
import ar.edu.unrn.seminario.modelo.Categoria;

public class CategoriaDAOJDBC implements CategoriaDAO {

	@Override
	public void create(Categoria objeto) throws BaseDeDatosException {
		// TODO Auto-generated method stub

	}

	@Override
	public void update(Categoria objeto) throws BaseDeDatosException {
		// TODO Auto-generated method stub

	}

	@Override
	public void remove(Long id) throws BaseDeDatosException {
		// TODO Auto-generated method stub

	}

	@Override
	public void remove(Categoria objeto) throws BaseDeDatosException {
		// TODO Auto-generated method stub

	}

	@Override
	public Categoria find(Integer id) throws BaseDeDatosException {
		// TODO Auto-generated method stub
		return null;
	}

	// Long id, String unNombre, int unPuntaje

	private Categoria resultAEntidad(ResultSet rs) throws SQLException, NotNullException, DataEmptyException {
		return new Categoria(rs.getLong(1), rs.getString(2), rs.getInt(3), rs.getString(4));

	}

	@Override
	public List<Categoria> findAll() throws BaseDeDatosException, DataEmptyException {
		List<Categoria> listado = new ArrayList<Categoria>();

		Properties prop = ConnectionManager.getProperties();

		try (Connection conn = DriverManager.getConnection(prop.getProperty("connection"), prop.getProperty("username"),
				prop.getProperty("password"));
				Statement sentencia = conn.createStatement();
				ResultSet resultado = sentencia.executeQuery("select * from categoria")) {
			while (resultado.next()) {
				Categoria categoria;
				try {
					categoria = this.resultAEntidad(resultado);
				} catch (NotNullException e) {
					throw new BaseDeDatosException(e.getMessage());
				} catch (DataEmptyException e) {
					throw new DataEmptyException(e.getMessage());
				}
				listado.add(categoria);
			}
		} catch (SQLException e) {
			throw new BaseDeDatosException("Error accediendo a la base de datos. ");
		} catch (IllegalArgumentException e) {
			e.printStackTrace();
		}
		return listado;
	}

}
