package ar.edu.unrn.seminario.accesos;

import java.util.List;

import ar.edu.unrn.seminario.exception.BaseDeDatosException;
import ar.edu.unrn.seminario.modelo.Recolector;
import ar.edu.unrn.seminario.modelo.Rol;
import ar.edu.unrn.seminario.modelo.Usuario;
import ar.edu.unrn.seminario.modelo.Visita;

public class TestAcceso {

	public static void main(String[] args) throws BaseDeDatosException {
		/*RolDao rolDao = new RolDAOJDBC();
		List<Rol> roles = rolDao.findAll();

		System.out.println("Listado de roles");
		for (Rol rol : roles)
			System.out.println(rol);

		UsuarioDao usuarioDao = new UsuarioDAOJDBC();
		List<Usuario> usuarios = usuarioDao.findAll();

		System.out.println("\nListado de usuarios");
		for (Usuario u : usuarios)
			System.out.println(u);*/

		//Recolectores
		IGenericDao<Recolector> recolectorDao = new RecolectorDAOJDBC();
		List<Recolector> recolectores = recolectorDao.findAll();
		System.out.println("Listado de recolectores");
		for (Recolector r: recolectores)
			System.out.println(r);
		System.out.println("\nFind");
		System.out.println(recolectorDao.find(1));
		
		//recolectorDao.create(new Recolector(11111111, "Create", "Create"));
		
		//recolectorDao.update(new Recolector(2L, 39032123, "Update", "Ultimo"));
		recolectorDao.remove(2L);
		
		// Visitas
		/*IGenericDao<Visita> visitaDao = new VisitaDAOJDBC();
		// visitaDao.remove(22L);
		List<Visita> visitas = visitaDao.findAll();

		System.out.println("\nListado de visitas");
		for (Visita v : visitas)
			System.out.println(v);*(

		//System.out.println("\nBusqueda por id");
		//System.out.println(visitaDao.find(2));
		// visitaDao.create(new Visita(LocalDate.now(), "prueba"));
		// visitaDao.update(new Visita(LocalDate.now(), "update"));

		/*
		 * Usuario usuario = new Usuario("ldifabio", "1234", "Lucas",
		 * "ldifabio@unrn.edu.ar", new Rol(1, "")); usuarioDao.create(usuario);
		 * List<Usuario> usuarios = usuarioDao.findAll(); for (Usuario u: usuarios) {
		 * System.out.println(u); } System.out.println(usuarioDao.find("ldifabio"));
		 */

		/*
		 * SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy"); String fecha =
		 * sdf.format(new Date()); System.out.println(fecha);
		 * 
		 * DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd/MM/YYYY");
		 * LocalDate localDate = LocalDate.now();
		 * System.out.println(formatter.format(localDate));
		 * 
		 * Date date = new Date(); LocalDate ld =
		 * date.toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
		 * System.out.println(ld);
		 */

		/*
		 * IGenericDao<Recolector> recolectorDao = new RecolectorDAOJDBC();
		 * List<Recolector> recolectores = recolectorDao.findAll();
		 * 
		 * System.out.println("\nListado de recolectores"); for (Recolector r:
		 * recolectores) System.out.println(r);
		 */

	}
}
