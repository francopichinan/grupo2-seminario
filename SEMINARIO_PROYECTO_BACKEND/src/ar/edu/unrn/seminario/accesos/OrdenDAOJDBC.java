package ar.edu.unrn.seminario.accesos;

import java.sql.Connection;
import java.sql.Date;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

import ar.edu.unrn.seminario.exception.BaseDeDatosException;
import ar.edu.unrn.seminario.exception.DataEmptyException;
import ar.edu.unrn.seminario.exception.DniInvalidException;
import ar.edu.unrn.seminario.exception.NotNullException;
import ar.edu.unrn.seminario.modelo.Mapa;
import ar.edu.unrn.seminario.modelo.Orden;
import ar.edu.unrn.seminario.modelo.Pedido;
import ar.edu.unrn.seminario.modelo.Punto;
import ar.edu.unrn.seminario.modelo.PuntoFijo;
import ar.edu.unrn.seminario.modelo.Recolector;

public class OrdenDAOJDBC implements OrdenDao {

	private final String TABLA = "ordenes";
	private final String INSERT = "INSERT INTO " + TABLA + " ( id_pedido, id_recolector, estado, fecha)"
			+ "VALUES (?, ?, ?, ?)";
	private final String UPDATE = "UPDATE " + TABLA + " SET estado=? WHERE id_orden=?";
	private final String DELETE = "DELETE from " + TABLA + " WHERE id_orden=?";
	private final String GETONE = "SELECT * from ordenes o   join pedidos p on o.id_pedido = p.id_pedido join  punto pu on p.id_punto = pu.id_punto WHERE o.id_orden=? ";
	private final String GETALL = "SELECT * from " + TABLA;
	private final String GETALLRecolectores = "SELECT * from " + TABLA
			+ " o join recolectores r on o.id_recolector = r.id_recolector";
	private final String GETALLConRecolectoresYDireccionDePunto = "SELECT * from ordenes o join recolectores r on o.id_recolector = r.id_recolector join  pedidos p on o.id_pedido = p.id_pedido join  punto pu on p.id_punto = pu.id_punto join  mapa m on m.id_mapa = pu.mapa_id";


	private Orden resultAEntidad(ResultSet rs) throws SQLException {
		return new Orden(rs.getLong(1), rs.getString(4), rs.getDate(5).toLocalDate());
	}

	private Recolector resultAEntidadRecolector(ResultSet rs)
			throws SQLException, DataEmptyException, DniInvalidException {
		return new Recolector(rs.getLong(6), rs.getString(7), rs.getString(8), rs.getString(9));
	}

	private Pedido resultAEntidadPedido(ResultSet rs) throws SQLException {
		return new Pedido(rs.getLong(2), rs.getBoolean(13), rs.getString(14));
	}

	private Punto resultAEntidadPunto(ResultSet rs) throws SQLException, NotNullException {
		return new Punto(rs.getString(21), rs.getString(17), rs.getInt(18), rs.getString(22), rs.getDate(23),
				new String("COLOR"), rs.getBoolean(25), new Mapa(rs.getString(33), rs.getInt(31)));
	}
	
	
	private Pedido resultAEntidadPedidoFijo(ResultSet rs) throws SQLException {
		return new Pedido(rs.getLong(2), rs.getBoolean(9), rs.getString(10));
	}
	
	private PuntoFijo resultAEntidadPuntoFijo(ResultSet rs) throws SQLException, NotNullException {
		return new PuntoFijo(rs.getLong(12), rs.getFloat(22), rs.getInt(23));
	}

	@Override
	public void create(Orden orden) throws BaseDeDatosException {
		Properties prop = ConnectionManager.getProperties();
		try (Connection conn = DriverManager.getConnection(prop.getProperty("connection"), prop.getProperty("username"),
				prop.getProperty("password")); PreparedStatement statement = conn.prepareStatement(INSERT)) {
			statement.setLong(1, orden.getPedido().getId());
			statement.setLong(2, orden.getRecolector().getId());
			statement.setString(3, orden.getEstado());
			statement.setDate(4, new java.sql.Date(Date.valueOf(orden.getFecha()).getTime()));
			int cantidad = statement.executeUpdate();
			if (cantidad > 0) {

			} else {
				System.out.println("Error al actualizar");
			}
		} catch (SQLException e) {
			throw new BaseDeDatosException("Error accediendo a la base de datos.");
		} catch (Exception e) {
			throw new BaseDeDatosException("Error de conectividad.");
		}
	}

	@Override
	public void update(Orden orden) throws BaseDeDatosException {
		Properties prop = ConnectionManager.getProperties();
		try (Connection conn = DriverManager.getConnection(prop.getProperty("connection"), prop.getProperty("username"),
				prop.getProperty("password")); PreparedStatement statement = conn.prepareStatement(UPDATE)) {
			statement.setString(1, orden.getEstado());
			statement.setLong(2, orden.getId_orden());
			statement.executeUpdate();
		} catch (SQLException e) {
			throw new BaseDeDatosException("Error accediendo a la base de datos.");
		}
	}

	@Override
	public void remove(Long id) throws BaseDeDatosException {
		Properties prop = ConnectionManager.getProperties();
		try (Connection conn = DriverManager.getConnection(prop.getProperty("connection"), prop.getProperty("username"),
				prop.getProperty("password")); PreparedStatement statement = conn.prepareStatement(DELETE)) {
			statement.setLong(1, id);
			statement.executeUpdate();
		} catch (SQLException e) {
			throw new BaseDeDatosException("Error accediendo a la base de datos.");
		}
	}

	@Override
	public void remove(Orden orden) throws BaseDeDatosException {
		this.remove(orden.getId_orden());
	}

	@Override
	public Orden find(Integer id) throws BaseDeDatosException {
		Orden orden = null;
		Properties prop = ConnectionManager.getProperties();
		try (Connection conn = DriverManager.getConnection(prop.getProperty("connection"), prop.getProperty("username"),
				prop.getProperty("password")); PreparedStatement statement = conn.prepareStatement(GETONE)) {
			statement.setInt(1, id);
			ResultSet rs = statement.executeQuery();
			if (rs.next())
				orden = this.resultAEntidad(rs);
			
				Pedido pedido = this.resultAEntidadPedidoFijo(rs);
								
				Punto punto = this.resultAEntidadPuntoFijo(rs);
				orden.setPedido(new Pedido(pedido.esCargaPesada(), pedido.getObservacion(), punto));
			
		} catch (SQLException e) {
			throw new BaseDeDatosException("Error accediendo a la base de datos.");
		} catch (Exception e) {
			throw new BaseDeDatosException("Error de conectividad.");
		}
		return orden;
	}

	@Override
	public List<Orden> findAll() throws BaseDeDatosException {
		List<Orden> listado = new ArrayList<Orden>();
		Properties prop = ConnectionManager.getProperties();
		try (Connection conn = DriverManager.getConnection(prop.getProperty("connection"), prop.getProperty("username"),
				prop.getProperty("password"));
				Statement sentencia = conn.createStatement();
				ResultSet resultado = sentencia.executeQuery(GETALL)) {
			while (resultado.next()) {
				Orden orden = this.resultAEntidad(resultado);
				listado.add(orden);
			}
		} catch (SQLException e) {
			System.out.println(e.getSQLState());
			throw new BaseDeDatosException("Error accediendo a la base de datos.");
		} catch (IllegalArgumentException e) {
			e.printStackTrace();
		}
		return listado;
	}

	@Override
	public List<Orden> findAllRecolectores() throws BaseDeDatosException, DataEmptyException, DniInvalidException {
		List<Orden> listado = new ArrayList<Orden>();
		Properties prop = ConnectionManager.getProperties();
		try (Connection conn = DriverManager.getConnection(prop.getProperty("connection"), prop.getProperty("username"),
				prop.getProperty("password"));
				Statement sentencia = conn.createStatement();
				ResultSet resultado = sentencia.executeQuery(GETALLRecolectores)) {
			while (resultado.next()) {
				Recolector recolector = this.resultAEntidadRecolector(resultado);

				Orden orden = this.resultAEntidad(resultado);
				orden.setRecolector(recolector);
				listado.add(orden);
			}
		} catch (SQLException e) {
			throw new BaseDeDatosException("Error accediendo a la base de datos.");
		} catch (IllegalArgumentException e) {
			e.printStackTrace();
		}
		return listado;
	}

	public List<Orden> findAllConRecolectoresYDireccionDePunto()
			throws BaseDeDatosException, DataEmptyException, DniInvalidException {
		List<Orden> listado = new ArrayList<Orden>();
		Properties prop = ConnectionManager.getProperties();
		try (Connection conn = DriverManager.getConnection(prop.getProperty("connection"), prop.getProperty("username"),
				prop.getProperty("password"));
				Statement sentencia = conn.createStatement();
				ResultSet resultado = sentencia.executeQuery(GETALLConRecolectoresYDireccionDePunto)) {
			while (resultado.next()) {
				Pedido pedido = this.resultAEntidadPedido(resultado);
				Recolector recolector = this.resultAEntidadRecolector(resultado);
				Orden orden = this.resultAEntidad(resultado);
				orden.setRecolector(recolector);
				Punto punto = this.resultAEntidadPunto(resultado);
				orden.setPedido(new Pedido(pedido.esCargaPesada(), pedido.getObservacion(), punto));
				listado.add(orden);
			}
		} catch (SQLException e) {
			System.out.println(e.getSQLState());
			throw new BaseDeDatosException("Error accediendo a la base de datos." + e.getMessage());
		} catch (IllegalArgumentException e) {
			e.printStackTrace();
		} catch (NotNullException e) {
			e.printStackTrace();
		}
		return listado;
	}

	@Override
	public Orden findByPedido(Pedido pedido) throws BaseDeDatosException {
		Orden orden = null;
		Properties prop = ConnectionManager.getProperties();
		try (Connection conn = DriverManager.getConnection(prop.getProperty("connection"), prop.getProperty("username"),
				prop.getProperty("password"));
				PreparedStatement statement = conn.prepareStatement("select * from ordenes where id_pedido = ?")) {
			statement.setLong(1, pedido.getId());
			ResultSet rs = statement.executeQuery();
			if (rs.next())
				orden = this.resultAEntidad(rs);
		} catch (SQLException e) {
			throw new BaseDeDatosException("Error accediendo a la base de datos.");
		} catch (Exception e) {
			throw new BaseDeDatosException("Error de conectividad.");
		}
		return orden;
	}

}
