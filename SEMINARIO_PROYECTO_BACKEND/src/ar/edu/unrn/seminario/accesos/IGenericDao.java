package ar.edu.unrn.seminario.accesos;

import java.util.List;

import ar.edu.unrn.seminario.exception.BaseDeDatosException;
import ar.edu.unrn.seminario.exception.DataEmptyException;
import ar.edu.unrn.seminario.exception.DniInvalidException;

public interface IGenericDao<T> {

	void create(T objeto) throws BaseDeDatosException;

	void update(T objeto) throws BaseDeDatosException;
	
	void remove(Long id) throws BaseDeDatosException;

	void remove(T objeto) throws BaseDeDatosException;

	T find(Integer id) throws BaseDeDatosException;

	List<T> findAll() throws BaseDeDatosException, DataEmptyException, DniInvalidException;

}
