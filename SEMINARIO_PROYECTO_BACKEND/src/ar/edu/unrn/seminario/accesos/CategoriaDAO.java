package ar.edu.unrn.seminario.accesos;

import java.util.List;

import ar.edu.unrn.seminario.exception.BaseDeDatosException;
import ar.edu.unrn.seminario.exception.DataEmptyException;
import ar.edu.unrn.seminario.modelo.Categoria;

public interface CategoriaDAO {
	void create(Categoria categoria) throws BaseDeDatosException;

	void update(Categoria categoria) throws BaseDeDatosException;

	void remove(Long id) throws BaseDeDatosException;

	void remove(Categoria categoria) throws BaseDeDatosException;

	Categoria find(Integer id) throws BaseDeDatosException;

	List<Categoria> findAll() throws BaseDeDatosException, DataEmptyException;
}
