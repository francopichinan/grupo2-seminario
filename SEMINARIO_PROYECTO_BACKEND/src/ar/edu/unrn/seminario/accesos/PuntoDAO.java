package ar.edu.unrn.seminario.accesos;

import java.util.List;

import ar.edu.unrn.seminario.exception.BaseDeDatosException;
import ar.edu.unrn.seminario.modelo.Punto;
import ar.edu.unrn.seminario.modelo.PuntoFijo;

public interface PuntoDAO {
	void create(Punto punto) throws BaseDeDatosException;

	void update(Punto punto) throws BaseDeDatosException;

	void remove(Long id) throws BaseDeDatosException;

	void remove(Punto punto) throws BaseDeDatosException;

	Punto find(Integer id) throws BaseDeDatosException;

	List<Punto> findAllPuntoInteligente() throws BaseDeDatosException;

	List<PuntoFijo> findAllPuntoFijo() throws BaseDeDatosException;

	String obtenerDireccion(Punto punto) throws BaseDeDatosException;

	PuntoFijo findPuntoFijo(Long id) throws BaseDeDatosException;

	void updatePuntoFijo(PuntoFijo puntoFijo) throws BaseDeDatosException;
}
