package ar.edu.unrn.seminario.accesos;

import java.sql.Connection;
import java.sql.Date;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

import ar.edu.unrn.seminario.exception.BaseDeDatosException;
import ar.edu.unrn.seminario.modelo.RAEE;
import ar.edu.unrn.seminario.modelo.RSU;
import ar.edu.unrn.seminario.modelo.Visita;

public class VisitaDAOJDBC implements VisitaDAO {

	private final String TABLA = "visitas";
	private final String INSERT = "INSERT INTO " + TABLA + " (fecha, observacion, id_orden)" + "VALUES (?, ?, ?)";
	private final String UPDATE = "UPDATE " + TABLA + " SET fecha=?, observacion=?, id_orden=? WHERE id_visita=?";
	private final String DELETE = "DELETE from " + TABLA + " WHERE id_visita=?";
	private final String GETONE = "SELECT * from " + TABLA + " WHERE id_visita=?";
	private final String GETALL = "SELECT * from " + TABLA;

	private Visita resultAEntidad(ResultSet rs) throws SQLException {
		return new Visita(rs.getLong(1), rs.getDate(2).toLocalDate(), rs.getString(3));
	}

	@Override
	public void create(Visita visita) throws BaseDeDatosException {
		Properties prop = ConnectionManager.getProperties();
		try (Connection conn = DriverManager.getConnection(prop.getProperty("connection"), prop.getProperty("username"),
				prop.getProperty("password"));
				PreparedStatement statement = conn.prepareStatement(INSERT, Statement.RETURN_GENERATED_KEYS);
				PreparedStatement statementAltaRSU = conn
						.prepareStatement("INSERT INTO rsu (nombre,descripcion, peso, valorenpuntosporkg, id_categoria)"
								+ "VALUES (?,?,?,?,?)", Statement.RETURN_GENERATED_KEYS);
				PreparedStatement statementAltaRAEE = conn.prepareStatement(
						"INSERT INTO raee (identificador,nombre,descripcion,id_categoria)" + "VALUES (?,?,?,?)");
				PreparedStatement statementAltaVisitaRSU = conn
						.prepareStatement("INSERT INTO residuo_rsu_visita (id_visita_fk, id_rsu_fk)" + "VALUES (?,?)");
				PreparedStatement statementAltaVisitaRAEE = conn
						.prepareStatement("INSERT INTO residuo_raee_visita (id_raee_fk,id_visita_fk)" + "VALUES (?,?)");

		) {

			statement.setDate(1, new java.sql.Date(Date.valueOf(visita.getFecha()).getTime()));
			statement.setString(2, visita.getObservacion());
			statement.setLong(3, visita.getOrden().getId_orden());
			statement.executeUpdate();
			ResultSet idVisitaGenerada = statement.getGeneratedKeys();
			idVisitaGenerada.next();

			ResultSet idRSUGenerada;
			List<RSU> listaRSU = visita.listarResiduosRSU();
			for (RSU r : listaRSU) {
				statementAltaRSU.setString(1, r.verNombre());
				statementAltaRSU.setString(2, r.verDescripcion());
				statementAltaRSU.setFloat(3, r.devolverPesoRSU());
				statementAltaRSU.setInt(4, r.devolverPuntosPorKgRSU());
				statementAltaRSU.setLong(5, r.verCategoria().devolverIdCategoria());
				statementAltaRSU.executeUpdate();
				idRSUGenerada = statementAltaRSU.getGeneratedKeys();
				idRSUGenerada.next();

				statementAltaVisitaRSU.setLong(1, idVisitaGenerada.getLong(1));
				statementAltaVisitaRSU.setLong(2, idRSUGenerada.getLong(1));
				statementAltaVisitaRSU.executeUpdate();
			}

			ResultSet idRAEEGenerada;
			List<RAEE> listaRAEE = visita.listarResiduosRAEE();
			for (RAEE r : listaRAEE) {
				statementAltaRAEE.setString(1, r.devolverIdentificador());
				statementAltaRAEE.setString(2, r.verNombre());
				statementAltaRAEE.setString(3, r.verDescripcion());
				statementAltaRAEE.setLong(4, r.verCategoria().devolverIdCategoria());
				statementAltaRAEE.executeUpdate();

				statementAltaVisitaRAEE.setString(1, r.devolverIdentificador());
				statementAltaVisitaRAEE.setLong(2, idVisitaGenerada.getLong(1));
				statementAltaVisitaRAEE.executeUpdate();
			}

		} catch (SQLException e) {
			e.printStackTrace();
			throw new BaseDeDatosException("Error accediendo a la base de datos.");
		} catch (Exception e) {
			throw new BaseDeDatosException("Error de conectividad.");
		}
	}

	@Override
	public void update(Visita visita) throws BaseDeDatosException {
		Properties prop = ConnectionManager.getProperties();
		try (Connection conn = DriverManager.getConnection(prop.getProperty("connection"), prop.getProperty("username"),
				prop.getProperty("password")); PreparedStatement statement = conn.prepareStatement(UPDATE)) {
			statement.setDate(1, new java.sql.Date(Date.valueOf(visita.getFecha()).getTime()));
			statement.setString(2, visita.getObservacion());
			statement.setLong(3, visita.getOrden().getId_orden());
			statement.execute();
		} catch (SQLException e) {
			throw new BaseDeDatosException("Error accediendo a la base de datos.");
		}
	}

	@Override
	public void remove(Long id) throws BaseDeDatosException {
		Properties prop = ConnectionManager.getProperties();
		try (Connection conn = DriverManager.getConnection(prop.getProperty("connection"), prop.getProperty("username"),
				prop.getProperty("password")); PreparedStatement statement = conn.prepareStatement(DELETE)) {
			statement.setLong(1, id);
			statement.executeUpdate();
		} catch (SQLException e) {
			throw new BaseDeDatosException("Error accediendo a la base de datos.");
		}
	}

	@Override
	public void remove(Visita visita) throws BaseDeDatosException {
		this.remove(visita.getId());
	}

	@Override
	public Visita find(Integer id) throws BaseDeDatosException {
		Visita visita = null;
		Properties prop = ConnectionManager.getProperties();
		try (Connection conn = DriverManager.getConnection(prop.getProperty("connection"), prop.getProperty("username"),
				prop.getProperty("password")); PreparedStatement statement = conn.prepareStatement(GETONE)) {
			statement.setInt(1, id);
			ResultSet rs = statement.executeQuery();
			if (rs.next())
				visita = this.resultAEntidad(rs);
		} catch (SQLException e) {
			throw new BaseDeDatosException("Error accediendo a la base de datos.");
		} catch (Exception e) {
			throw new BaseDeDatosException("Error de conectividad.");
		}
		return visita;
	}

	@Override
	public List<Visita> findAll() throws BaseDeDatosException {
		List<Visita> listado = new ArrayList<Visita>();
		Properties prop = ConnectionManager.getProperties();
		try (Connection conn = DriverManager.getConnection(prop.getProperty("connection"), prop.getProperty("username"),
				prop.getProperty("password"));
				Statement sentencia = conn.createStatement();
				ResultSet resultado = sentencia.executeQuery(GETALL)) {
			while (resultado.next()) {
				Visita visita = this.resultAEntidad(resultado);
				listado.add(visita);
			}
		} catch (SQLException e) {
			throw new BaseDeDatosException("Error accediendo a la base de datos.");
		} catch (IllegalArgumentException e) {
			e.printStackTrace();
		}
		return listado;
	}

	@Override
	public List<Visita> findAllByOrdenId(Long id) throws BaseDeDatosException {
		List<Visita> listado = new ArrayList<Visita>();
		Properties prop = ConnectionManager.getProperties();
		try (Connection conn = DriverManager.getConnection(prop.getProperty("connection"), prop.getProperty("username"),
				prop.getProperty("password"));
				PreparedStatement sentencia = conn.prepareStatement("select * from visitas where id_orden=?");) {
			sentencia.setLong(1, id);
			ResultSet resultado = sentencia.executeQuery();
			while (resultado.next()) {
				Visita visita = this.resultAEntidad(resultado);
				listado.add(visita);
			}
		} catch (SQLException e) {
			throw new BaseDeDatosException("Error accediendo a la base de datos.");
		} catch (IllegalArgumentException e) {
			e.printStackTrace();
		}
		return listado;
	}
}