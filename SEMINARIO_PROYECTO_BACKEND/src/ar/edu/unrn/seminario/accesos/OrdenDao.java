package ar.edu.unrn.seminario.accesos;

import java.util.List;

import ar.edu.unrn.seminario.exception.BaseDeDatosException;
import ar.edu.unrn.seminario.exception.DataEmptyException;
import ar.edu.unrn.seminario.exception.DniInvalidException;
import ar.edu.unrn.seminario.modelo.*;

public interface OrdenDao {

	void create(Orden orden) throws BaseDeDatosException;

	void update(Orden orden) throws BaseDeDatosException;

	void remove(Long id) throws BaseDeDatosException;

	void remove(Orden orden) throws BaseDeDatosException;

	Orden find(Integer id) throws BaseDeDatosException;

	Orden findByPedido(Pedido pedido) throws BaseDeDatosException;

	List<Orden> findAll() throws BaseDeDatosException;

	List<Orden> findAllRecolectores() throws BaseDeDatosException, DataEmptyException, DniInvalidException;

	List<Orden> findAllConRecolectoresYDireccionDePunto()
			throws BaseDeDatosException, DataEmptyException, DniInvalidException;
}
