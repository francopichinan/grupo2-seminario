package ar.edu.unrn.seminario.accesos;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.SQLIntegrityConstraintViolationException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

import ar.edu.unrn.seminario.exception.BaseDeDatosException;
import ar.edu.unrn.seminario.exception.DataEmptyException;
import ar.edu.unrn.seminario.exception.DniInvalidException;
import ar.edu.unrn.seminario.modelo.Recolector;

public class RecolectorDAOJDBC implements IGenericDao<Recolector> {

	private final String TABLA = "recolectores";
	private final String INSERT = "INSERT INTO " + TABLA + " (dni, nombre, apellido)" + "VALUES (?, ?, ?)";
	private final String UPDATE = "UPDATE " + TABLA + " SET dni=?, nombre=?, apellido=? WHERE id_recolector=?";
	private final String DELETE = "DELETE from " + TABLA + " WHERE id_recolector=?";
	private final String GETONE = "SELECT * from " + TABLA + " WHERE id_recolector=?";
	private final String GETALL = "SELECT * from " + TABLA;

	private Recolector resultAEntidad(ResultSet rs) throws SQLException, DataEmptyException, DniInvalidException {
		return new Recolector(rs.getLong(1), rs.getString(2), rs.getString(3), rs.getString(4));
	}
	
	@Override
	public void create(Recolector recolector) throws BaseDeDatosException {
		Properties prop = ConnectionManager.getProperties();
		try (Connection conn = DriverManager.getConnection(prop.getProperty("connection"), prop.getProperty("username"),
				prop.getProperty("password")); 
				PreparedStatement statement = conn.prepareStatement(INSERT)) {
			statement.setString(1, recolector.getDni());
			statement.setString(2, recolector.getNombre());
			statement.setString(3, recolector.getApellido());
			statement.executeUpdate();
		} catch (SQLException e) {
			throw new BaseDeDatosException("Error accediendo a la base de datos.");
		} catch (Exception e) {
			throw new BaseDeDatosException("Error de conectividad.");
		}	
	}

	@Override
	public void update(Recolector recolector) throws BaseDeDatosException {
		Properties prop = ConnectionManager.getProperties();
		try (Connection conn = DriverManager.getConnection(prop.getProperty("connection"), prop.getProperty("username"),
				prop.getProperty("password")); PreparedStatement statement = conn.prepareStatement(UPDATE)) {
			statement.setString(1, recolector.getDni());
			statement.setString(2, recolector.getNombre());
			statement.setString(3, recolector.getApellido());
			statement.setLong(4, recolector.getId());
			statement.execute();
		} catch (SQLException e) {
			throw new BaseDeDatosException("Error accediendo a la base de datos.");
		}
	}

	@Override
	public void remove(Long id) throws BaseDeDatosException {
		Properties prop = ConnectionManager.getProperties();
		try (Connection conn = DriverManager.getConnection(prop.getProperty("connection"), prop.getProperty("username"),
				prop.getProperty("password")); PreparedStatement statement = conn.prepareStatement(DELETE)) {
			statement.setLong(1, id);
			statement.executeUpdate();
		} catch (SQLIntegrityConstraintViolationException e) {
			throw new BaseDeDatosException("El recolector tiene asociado una orden");
		} catch (SQLException e) {
			throw new BaseDeDatosException("Error accediendo a la base de datos.");
		}
	}

	@Override
	public void remove(Recolector recolector) throws BaseDeDatosException{
		this.remove(recolector.getId());
	}

	@Override
	public Recolector find(Integer id) throws BaseDeDatosException {
		Recolector recolector = null;
		Properties prop = ConnectionManager.getProperties();
		try (Connection conn = DriverManager.getConnection(prop.getProperty("connection"), prop.getProperty("username"),
				prop.getProperty("password")); PreparedStatement statement = conn.prepareStatement(GETONE)) {
			statement.setInt(1, id);
			ResultSet rs = statement.executeQuery();
			if (rs.next())
				recolector = this.resultAEntidad(rs);
		} catch (SQLException e) {
			throw new BaseDeDatosException("Error accediendo a la base de datos.");
		} catch (Exception e) {
			throw new BaseDeDatosException("Error de conectividad.");
		}
		return recolector;	
	}

	@Override
	public List<Recolector> findAll() throws BaseDeDatosException, DataEmptyException, DniInvalidException {
		List<Recolector> listado = new ArrayList<Recolector>();
		Properties prop = ConnectionManager.getProperties();
		try (Connection conn = DriverManager.getConnection(prop.getProperty("connection"), prop.getProperty("username"),
				prop.getProperty("password"));
				Statement sentencia = conn.createStatement();
				ResultSet resultado = sentencia.executeQuery(GETALL)) {
			while (resultado.next()) {
				Recolector recolector = this.resultAEntidad(resultado);
				listado.add(recolector);
			}
		} catch (SQLException e) {
			throw new BaseDeDatosException("Error accediendo a la base de datos.");
		} catch (IllegalArgumentException e) {
			e.printStackTrace();
		}
		return listado;
	}
}
