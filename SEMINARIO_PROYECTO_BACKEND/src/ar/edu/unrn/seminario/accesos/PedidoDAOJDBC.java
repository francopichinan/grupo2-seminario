package ar.edu.unrn.seminario.accesos;

import java.sql.Connection;
import java.sql.Date;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

import ar.edu.unrn.seminario.exception.BaseDeDatosException;
import ar.edu.unrn.seminario.exception.NotNullException;
import ar.edu.unrn.seminario.modelo.Mapa;
import ar.edu.unrn.seminario.modelo.Pedido;
import ar.edu.unrn.seminario.modelo.Punto;
import ar.edu.unrn.seminario.modelo.PuntoFijo;
import ar.edu.unrn.seminario.modelo.Residuo;

public class PedidoDAOJDBC implements PedidoDAO {

	private final String TABLA = "pedidos";
	private final String INSERT = "INSERT INTO " + TABLA + " (fecha,estado,carga_pesada,observacion,id_punto)"
			+ "VALUES (?, ?, ?, ?, ?)";
	// private final String UPDATE = "UPDATE " + TABLA + " SET fecha=?, estado=?,
	// carga_pesada=?, observacion=?, id_punto=? WHERE id=?";
	private final String UPDATE = "UPDATE " + TABLA + " SET estado=? WHERE id_pedido=?";
	private final String DELETE = "DELETE from " + TABLA + " WHERE id_pedido=?";
	private final String GETONE = "SELECT * from " + TABLA + " WHERE id_pedido=?";
	private final String GETALL = "SELECT * from " + TABLA;
	private final String GETALLConPUNTO = "SELECT * from pedidos p join  punto pu on p.id_punto = pu.id_punto join  mapa m on m.id_mapa = pu.mapa_id";
	private final String GETSinOrdenConPunto = "SELECT * from pedidos p join  punto pu on p.id_punto = pu.id_punto join  mapa m on m.id_mapa = pu.mapa_id where p.estado='ORDEN NO GENERADA'";

	private Punto resultAEntidadPunto(ResultSet rs) throws SQLException, NotNullException {
		return new Punto(rs.getString(12), rs.getString(8), rs.getInt(9), rs.getString(13), rs.getDate(14),
				new String("COLOR"), rs.getBoolean(16), new Mapa(rs.getString(24), rs.getInt(22)));
	}

	private Pedido resultAEntidadPedido(ResultSet rs) throws SQLException {
		return new Pedido(rs.getLong(1), rs.getDate(2).toLocalDate(), rs.getBoolean(4), rs.getString(5));
	}

	// Finalizado
	public void create(Pedido pedido) throws BaseDeDatosException {
		Properties prop = ConnectionManager.getProperties();
		try (Connection conn = DriverManager.getConnection(prop.getProperty("connection"), prop.getProperty("username"),
				prop.getProperty("password"));
				PreparedStatement statement = conn.prepareStatement(INSERT, Statement.RETURN_GENERATED_KEYS);
				PreparedStatement statementResiduo = conn.prepareStatement(
						"INSERT INTO pedido_categoria (id_pedido,id_categoria, observacion)" + "VALUES (?, ?,?)");) {
			statement.setDate(1, new java.sql.Date(Date.valueOf(pedido.getFechaDeCreacion()).getTime()));
			statement.setString(2, pedido.obtenerEstado());
			statement.setBoolean(3, pedido.esCargaPesada());
			statement.setString(4, pedido.getObservacion());
			statement.setLong(5, pedido.getPunto().getId());
			statement.executeUpdate();
			
			ResultSet idPedidoGenerado = statement.getGeneratedKeys();
			idPedidoGenerado.next();
			List<Residuo> lisResiduos = pedido.obtenerListaResiduos();
			for (Residuo r : lisResiduos) {
				statementResiduo.setLong(1, idPedidoGenerado.getLong(1));
				statementResiduo.setLong(2, r.verCategoria().devolverIdCategoria());
				statementResiduo.setInt(3, r.verCantidad());
				statementResiduo.executeUpdate();
			}
		} catch (SQLException e) {
			System.out.println(e.getSQLState());
			throw new BaseDeDatosException("Error accediendo a la base de datos.\n" + e.getMessage());
		} catch (Exception e) {
			e.printStackTrace();
			throw new BaseDeDatosException("Error de conectividad.");
		}
	}

	public void update(Pedido pedido) throws BaseDeDatosException {
		Properties prop = ConnectionManager.getProperties();
		try (Connection conn = DriverManager.getConnection(prop.getProperty("connection"), prop.getProperty("username"),
				prop.getProperty("password")); PreparedStatement statement = conn.prepareStatement(UPDATE)) {
			statement.setString(1, pedido.obtenerEstado());
			statement.setLong(2, pedido.getId());
			statement.execute();
		} catch (SQLException e) {
			throw new BaseDeDatosException("Error accediendo a la base de datos.");
		}
	}

	/*
	 * public void update(Pedido pedido) throws BaseDeDatosException { Properties
	 * prop = ConnectionManager.getProperties(); try (Connection conn =
	 * DriverManager.getConnection(prop.getProperty("connection"),
	 * prop.getProperty("username"), prop.getProperty("password"));
	 * PreparedStatement statement = conn.prepareStatement(UPDATE)) {
	 * statement.setDate(1, new
	 * java.sql.Date(Date.valueOf(pedido.getFechaDeCreacion()).getTime()));
	 * statement.setString(2, pedido.obtenerEstado()); //statement.setBoolean(3,
	 * pedido.isEsCargaPesada()); statement.setString(4, pedido.getObservacion());
	 * statement.execute(); } catch (SQLException e) { throw new
	 * BaseDeDatosException("Error accediendo a la base de datos."); } }
	 */

	// Finalizado
	public void remove(Long id) throws BaseDeDatosException {
		Properties prop = ConnectionManager.getProperties();
		try (Connection conn = DriverManager.getConnection(prop.getProperty("connection"), prop.getProperty("username"),
				prop.getProperty("password")); PreparedStatement statement = conn.prepareStatement(DELETE)) {
			statement.setLong(1, id);
			statement.executeUpdate();
		} catch (SQLException e) {
			throw new BaseDeDatosException("Error accediendo a la base de datos.");
		}
	}

	public void remove(Pedido pedido) throws BaseDeDatosException {
		this.remove(pedido.getId());
	}

	public Pedido find(Integer id) throws BaseDeDatosException {
		Pedido pedido = null;
		Properties prop = ConnectionManager.getProperties();
		try (Connection conn = DriverManager.getConnection(prop.getProperty("connection"), prop.getProperty("username"),
				prop.getProperty("password")); PreparedStatement statement = conn.prepareStatement(GETONE)) {
			statement.setInt(1, id);
			ResultSet rs = statement.executeQuery();
			if (rs.next())
				pedido = this.resultAEntidadPedido(rs);
		} catch (SQLException e) {
			throw new BaseDeDatosException("Error accediendo a la base de datos.");
		} catch (Exception e) {
			throw new BaseDeDatosException("Error de conectividad.");
		}
		return pedido;
	}

	public List<Pedido> findAll() throws BaseDeDatosException {
		List<Pedido> listado = new ArrayList<Pedido>();
		Properties prop = ConnectionManager.getProperties();
		try (Connection conn = DriverManager.getConnection(prop.getProperty("connection"), prop.getProperty("username"),
				prop.getProperty("password"));
				Statement sentencia = conn.createStatement();
				ResultSet resultado = sentencia.executeQuery(GETALLConPUNTO)) {
			while (resultado.next()) {// Hay que armar el responsable utilizando el id que retorna el pedido
				// sentenciaPunto.setLong(1, rsPedidos.getLong(6));
				// ResultSet rsPunto = sentenciaPunto.executeQuery();
				// rsPunto.next();
				Punto punto = this.resultAEntidadPunto(resultado);

//id_punto,calle_id,altura_id,mapa_id,cordenada_id,titulo,descripcion,fecha,tipo,icono,estado,porcentaje_ocupacion,nivel,QR,observacion
				Pedido pedido = this.resultAEntidadPedido(resultado);
				pedido.setPunto(punto); // new Pedido(resultado.getLong(1), resultado.getBoolean(4),
										// resultado.getString(5), punto);
				listado.add(pedido);
			}
		} catch (SQLException e) {
			throw new BaseDeDatosException("Error accediendo a la base de datos.");
		} catch (IllegalArgumentException e) {
			e.printStackTrace();
		} catch (NotNullException e) {
			e.printStackTrace();
		}
		return listado;
	}

	public List<Pedido> findSinOrdenConPunto() throws BaseDeDatosException {
		List<Pedido> listado = new ArrayList<Pedido>();
		Properties prop = ConnectionManager.getProperties();
		try (Connection conn = DriverManager.getConnection(prop.getProperty("connection"), prop.getProperty("username"),
				prop.getProperty("password"));
				Statement sentencia = conn.createStatement();
				ResultSet resultado = sentencia.executeQuery(GETSinOrdenConPunto)) {
			while (resultado.next()) {
				Punto punto = this.resultAEntidadPunto(resultado);
				Pedido pedido = this.resultAEntidadPedido(resultado);
				pedido.setPunto(punto);
				listado.add(pedido);
			}
		} catch (SQLException e) {
			throw new BaseDeDatosException("Error accediendo a la base de datos.");
		} catch (IllegalArgumentException e) {
			e.printStackTrace();
		} catch (NotNullException e) {
			e.printStackTrace();
		}
		return listado;
	}

	@Override
	public List<Pedido> findAllByPuntofijo(PuntoFijo punto) throws BaseDeDatosException {
		List<Pedido> listado = new ArrayList<Pedido>();
		Properties prop = ConnectionManager.getProperties();
		try (Connection conn = DriverManager.getConnection(prop.getProperty("connection"), prop.getProperty("username"),
				prop.getProperty("password"));
				Statement sentencia = conn.createStatement();
				ResultSet rs = sentencia.executeQuery("select * from pedidos where id_punto = " + punto.getId())) {
			while (rs.next()) {
				Pedido pedido = new Pedido(rs.getLong(1), rs.getDate(2).toLocalDate(), rs.getBoolean(4),
						rs.getString(5), rs.getString(3));
				listado.add(pedido);
			}
		} catch (SQLException e) {
			throw new BaseDeDatosException("Error accediendo a la base de datos.");
		} catch (IllegalArgumentException e) {
			e.printStackTrace();
		}
		return listado;
	}
}