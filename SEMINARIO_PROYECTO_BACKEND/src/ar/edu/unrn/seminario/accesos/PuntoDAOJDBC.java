package ar.edu.unrn.seminario.accesos;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Properties;
import java.util.Set;

import ar.edu.unrn.seminario.exception.BaseDeDatosException;
import ar.edu.unrn.seminario.exception.DataEmptyException;
import ar.edu.unrn.seminario.exception.NotNullException;
import ar.edu.unrn.seminario.modelo.Categoria;
import ar.edu.unrn.seminario.modelo.Mapa;
import ar.edu.unrn.seminario.modelo.Punto;
import ar.edu.unrn.seminario.modelo.PuntoFijo;
import ar.edu.unrn.seminario.modelo.Residuo;

public class PuntoDAOJDBC implements PuntoDAO {

	private final String TABLA = "punto";
	private final String GETALL = "SELECT * from " + TABLA;
	private final String GETONE = "SELECT * from punto WHERE id_punto=? and tipo='Punto Fijo'";
	private final String UPDATEPuntoFijo = "UPDATE " + TABLA + " SET porcentaje_ocupacion=? WHERE id_punto=?";
	
	
	private PuntoFijo resultAEntidadPuntoFijo(ResultSet rs) throws SQLException, NotNullException {
		return new PuntoFijo(rs.getLong(1), rs.getFloat(11), rs.getInt(12));
	}
	
	public PuntoDAOJDBC() {
		// TODO Auto-generated constructor stub
	}

	@Override
	public void create(Punto objeto) throws BaseDeDatosException {
		// TODO Auto-generated method stub

	}

	@Override
	public void update(Punto objeto) throws BaseDeDatosException {
		// TODO Auto-generated method stub

	}

	@Override
	public void updatePuntoFijo(PuntoFijo puntoFijo) throws BaseDeDatosException {
		Properties prop = ConnectionManager.getProperties();
		try (Connection conn = DriverManager.getConnection(prop.getProperty("connection"), prop.getProperty("username"),
				prop.getProperty("password")); PreparedStatement statement = conn.prepareStatement(UPDATEPuntoFijo)) {
			statement.setFloat(1, puntoFijo.getPorcentajeOcupacion());
			statement.setLong(2, puntoFijo.getId());
			statement.executeUpdate();
		} catch (SQLException e) {
			throw new BaseDeDatosException("Error accediendo a la base de datos.");
		}

	}
	
	
	@Override
	public void remove(Long id) throws BaseDeDatosException {
		// TODO Auto-generated method stub

	}

	@Override
	public void remove(Punto objeto) throws BaseDeDatosException {
		// TODO Auto-generated method stub

	}

	@Override
	public PuntoFijo findPuntoFijo(Long id) throws BaseDeDatosException {
		PuntoFijo puntoFijo = null;
		Properties prop = ConnectionManager.getProperties();
		try (Connection conn = DriverManager.getConnection(prop.getProperty("connection"), prop.getProperty("username"),
				prop.getProperty("password")); PreparedStatement statement = conn.prepareStatement(GETONE)) {
			statement.setLong(1, id);
			ResultSet rs = statement.executeQuery();
			if (rs.next())
				puntoFijo = this.resultAEntidadPuntoFijo(rs);
		} catch (SQLException e) {
			throw new BaseDeDatosException("Error accediendo a la base de datos.");
		} catch (Exception e) {
			throw new BaseDeDatosException("Error de conectividad.");
		}
		return puntoFijo;
	}
	
	private Punto resultAEntidad(ResultSet rs) throws SQLException, NotNullException {
		return new Punto(rs.getString(6), rs.getString(2), rs.getInt(3), rs.getString(7), rs.getDate(8),
				new String("COLOR"), rs.getBoolean(10), new Mapa(rs.getString(18)));

	}

	// TODO METODO NECESARIO PARA FUNCIONAR
	public List<Punto> findAllPuntoInteligente() throws BaseDeDatosException {
		List<Punto> listado = new ArrayList<Punto>();

		Properties prop = ConnectionManager.getProperties();
		try (Connection conn = DriverManager.getConnection(prop.getProperty("connection"), prop.getProperty("username"),
				prop.getProperty("password"));
				Statement sentencia = conn.createStatement();

				// Busco todos los puntos inteligentes
				ResultSet resultado = sentencia.executeQuery(this.GETALL
						+ " join mapa on (mapa_id = id_mapa) where tipo  = '" + Punto.PuntoInteligente + "'")) {

			while (resultado.next()) {
				Punto punto;
				try {

					// Reconstruyo el objeto Punto
					punto = this.resultAEntidad(resultado);
					punto.setId(resultado.getLong(1));
					listado.add(punto);
				} catch (NotNullException e) {
					throw new BaseDeDatosException("Error interno de la Base de Datos. No se pueden traer los puntos.");
				}
			}
		} catch (SQLException e) {
			throw new BaseDeDatosException("Error accediendo a la base de datos. ");
		} catch (IllegalArgumentException e) {
			e.printStackTrace();
		}
		return listado;
	}

	@Override
	public List<PuntoFijo> findAllPuntoFijo() throws BaseDeDatosException {
		List<PuntoFijo> listado = new ArrayList<PuntoFijo>();

		Properties prop = ConnectionManager.getProperties();
		try (Connection conn = DriverManager.getConnection(prop.getProperty("connection"), prop.getProperty("username"),
				prop.getProperty("password"));
				Statement sentencia = conn.createStatement();
				ResultSet resultado = sentencia.executeQuery("select * from punto p join mapa m on (p.mapa_id = m.id_mapa) where tipo='Punto Fijo'")) {
			
			
			while (resultado.next()) {

				PuntoFijo punto = new PuntoFijo(resultado.getString(6), resultado.getString(2), resultado.getInt(3),
						resultado.getString(7), resultado.getDate(8), "COLOR", resultado.getBoolean(10),
						resultado.getInt(12), resultado.getFloat(11), new Mapa(resultado.getString(18)));
				punto.setId(resultado.getLong(1));
				listado.add(punto);

				// RECUPERAR LOS TIPOS DE RESIDUOS DEL PUNTO
				Set<Residuo> listadoResiduo = new HashSet<Residuo>();
				Statement sentenciaResiduos = conn.createStatement();
				ResultSet resulCat = sentenciaResiduos.executeQuery(
						"select cat.nombre from punto_tipo_residuo ptr join categoria cat on (ptr.tipo_id = cat.id_categoria) where ptr.punto_id = "
								+ resultado.getLong(1));
				while (resulCat.next()) {
					try {
						// TODO mejorar el manejo de las excepciones.
						listadoResiduo.add(new Residuo(0, new Categoria(resulCat.getString(1), 33)));
					} catch (DataEmptyException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}

				punto.setResiduosAlojados(listadoResiduo);

			}
		} catch (
		SQLException e) {
			throw new BaseDeDatosException("Error accediendo a la base de datos. ");
		} catch (IllegalArgumentException e) {
			e.printStackTrace();
		} catch (NotNullException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return listado;
	}

	@Override
	public String obtenerDireccion(Punto punto) throws BaseDeDatosException {
		String direccion = new String();

		Properties prop = ConnectionManager.getProperties();
		try (Connection conn = DriverManager.getConnection(prop.getProperty("connection"), prop.getProperty("username"),
				prop.getProperty("password"));
				Statement sentencia = conn.createStatement();
				ResultSet resultado = sentencia.executeQuery("select calle_id, altura_id from punto p where p.id=?")) {
			resultado.next();
			direccion = new String(resultado.getString(1) + " " + resultado.getString(2));

		} catch (SQLException e) {
			throw new BaseDeDatosException("Error accediendo a la base de datos. ");
		} catch (IllegalArgumentException e) {
			e.printStackTrace();
		}
		return direccion;
	}

	@Override
	public Punto find(Integer id) throws BaseDeDatosException {
		// TODO Auto-generated method stub
		return null;
	}

}
