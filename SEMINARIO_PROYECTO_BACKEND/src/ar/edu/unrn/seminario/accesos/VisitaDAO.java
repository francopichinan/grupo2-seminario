package ar.edu.unrn.seminario.accesos;

import java.util.List;

import ar.edu.unrn.seminario.exception.BaseDeDatosException;
import ar.edu.unrn.seminario.modelo.Visita;

public interface VisitaDAO {
	
	void create(Visita visita) throws BaseDeDatosException;

	void update(Visita visita) throws BaseDeDatosException;

	void remove(Long id) throws BaseDeDatosException;

	void remove(Visita visita) throws BaseDeDatosException;

	Visita find(Integer id) throws BaseDeDatosException;

	List<Visita> findAll() throws BaseDeDatosException;
	
	List<Visita> findAllByOrdenId(Long id) throws BaseDeDatosException;
}
