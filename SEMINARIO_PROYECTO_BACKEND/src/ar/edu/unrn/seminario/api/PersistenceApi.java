package ar.edu.unrn.seminario.api;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;

import ar.edu.unrn.seminario.accesos.CategoriaDAO;
import ar.edu.unrn.seminario.accesos.CategoriaDAOJDBC;
import ar.edu.unrn.seminario.accesos.IGenericDao;
import ar.edu.unrn.seminario.accesos.OrdenDAOJDBC;
import ar.edu.unrn.seminario.accesos.OrdenDao;
import ar.edu.unrn.seminario.accesos.PedidoDAO;
import ar.edu.unrn.seminario.accesos.PedidoDAOJDBC;
import ar.edu.unrn.seminario.accesos.PuntoDAO;
import ar.edu.unrn.seminario.accesos.PuntoDAOJDBC;
import ar.edu.unrn.seminario.accesos.RecolectorDAOJDBC;
import ar.edu.unrn.seminario.accesos.RolDAOJDBC;
import ar.edu.unrn.seminario.accesos.RolDao;
import ar.edu.unrn.seminario.accesos.UsuarioDAOJDBC;
import ar.edu.unrn.seminario.accesos.UsuarioDao;
import ar.edu.unrn.seminario.accesos.VisitaDAO;
import ar.edu.unrn.seminario.accesos.VisitaDAOJDBC;
import ar.edu.unrn.seminario.dto.CategoriaDTO;
import ar.edu.unrn.seminario.dto.OrdenDTO;
import ar.edu.unrn.seminario.dto.PedidoDTO;
import ar.edu.unrn.seminario.dto.PuntoDTO;
import ar.edu.unrn.seminario.dto.PuntoFijoDTO;
import ar.edu.unrn.seminario.dto.RAEEDTO;
import ar.edu.unrn.seminario.dto.RSUDTO;
import ar.edu.unrn.seminario.dto.RecolectorDTO;
import ar.edu.unrn.seminario.dto.ResiduoDTO;
import ar.edu.unrn.seminario.dto.RolDTO;
import ar.edu.unrn.seminario.dto.UsuarioDTO;
import ar.edu.unrn.seminario.dto.VisitaDTO;
import ar.edu.unrn.seminario.exception.BaseDeDatosException;
import ar.edu.unrn.seminario.exception.DataEmptyException;
import ar.edu.unrn.seminario.exception.DniInvalidException;
import ar.edu.unrn.seminario.exception.NotNullException;
import ar.edu.unrn.seminario.exception.RegistroVisitaException;
import ar.edu.unrn.seminario.exception.StateException;
import ar.edu.unrn.seminario.modelo.Categoria;
import ar.edu.unrn.seminario.modelo.Orden;
import ar.edu.unrn.seminario.modelo.Pedido;
import ar.edu.unrn.seminario.modelo.Punto;
import ar.edu.unrn.seminario.modelo.PuntoFijo;
import ar.edu.unrn.seminario.modelo.RAEE;
import ar.edu.unrn.seminario.modelo.RSU;
import ar.edu.unrn.seminario.modelo.Recolector;
import ar.edu.unrn.seminario.modelo.Residuo;
import ar.edu.unrn.seminario.modelo.Rol;
import ar.edu.unrn.seminario.modelo.Usuario;
import ar.edu.unrn.seminario.modelo.Visita;

public class PersistenceApi implements IApi {

	private RolDao rolDao;
	private UsuarioDao usuarioDao;
	private VisitaDAO visitaDao = new VisitaDAOJDBC();
	private IGenericDao<Recolector> recolectorDao = new RecolectorDAOJDBC();
	private PedidoDAO pedidoDAO = new PedidoDAOJDBC();
	private OrdenDao ordenDAO = new OrdenDAOJDBC();
	private CategoriaDAO categoriaDAO = new CategoriaDAOJDBC();
	private PuntoDAO puntoDAO = new PuntoDAOJDBC();

	public PersistenceApi() {
		rolDao = new RolDAOJDBC();
		usuarioDao = new UsuarioDAOJDBC();
	}

	@Override
	public void registrarUsuario(String username, String password, String email, String nombre, Integer codigoRol) {
		Rol rol = rolDao.find(codigoRol);
		Usuario usuario = new Usuario(username, password, nombre, email, rol);
		this.usuarioDao.create(usuario);
	}

	@Override
	public List<UsuarioDTO> obtenerUsuarios() {
		List<UsuarioDTO> dtos = new ArrayList<>();
		List<Usuario> usuarios = usuarioDao.findAll();
		for (Usuario u : usuarios) {
			dtos.add(new UsuarioDTO(u.getUsuario(), u.getContrasena(), u.getNombre(), u.getEmail(),
					u.getRol().getNombre(), u.isActivo(), u.obtenerEstado()));
		}
		return dtos;
	}

	@Override
	public UsuarioDTO obtenerUsuario(String username) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void eliminarUsuario(String username) {
		// TODO Auto-generated method stub
	}

	@Override
	public List<RolDTO> obtenerRoles() {
		List<Rol> roles = rolDao.findAll();
		List<RolDTO> rolesDTO = new ArrayList<>(0);
		for (Rol rol : roles) {
			rolesDTO.add(new RolDTO(rol.getCodigo(), rol.getNombre(), rol.isActivo()));
		}
		return rolesDTO;
	}

	@Override
	public List<RolDTO> obtenerRolesActivos() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void guardarRol(Integer codigo, String descripcion, boolean estado)
			throws NotNullException, DataEmptyException {
		// TODO Auto-generated method stub

	}

	@Override
	public RolDTO obtenerRolPorCodigo(Integer codigo) {
		Rol rol = rolDao.find(codigo);
		RolDTO rolDTO = new RolDTO(rol.getCodigo(), rol.getNombre(), rol.isActivo());
		return rolDTO;
	}

	@Override
	public void activarRol(Integer codigo) {
		// TODO Auto-generated method stub

	}

	@Override
	public void desactivarRol(Integer codigo) {
		// TODO Auto-generated method stub

	}

	@Override
	public void activarUsuario(String username) {
		// TODO Auto-generated method stub

	}

	@Override
	public void desactivarUsuario(String username) {
		// TODO Auto-generated method stub

	}

	@Override
	public void registrarVisita(VisitaDTO visitaDto, Long id_orden)
			throws BaseDeDatosException, DataEmptyException, RegistroVisitaException {
		Orden orden = ordenDAO.find(Integer.valueOf(id_orden.intValue()));
		List<RSU> listaRSU = new ArrayList<RSU>();
		List<RAEE> listaRAEE = new ArrayList<RAEE>();
		Categoria categoria;
		
		for (RSUDTO r : visitaDto.obtenerResiduosRSU()) {
			categoria = new Categoria(r.verCategoria().verId(), r.verCategoria().verNombre(),
					r.verCategoria().verPuntaje(), r.verCategoria().obtenerTipo());
			listaRSU.add(new RSU(r.verNombre(), r.verDescripcion(), categoria, r.devolverPesoRSU(),
					r.devolverPuntosPorKgRSU()));
		} 	
		for (RAEEDTO r : visitaDto.obtenerResiduosRAEE()) {
			categoria = new Categoria(r.verCategoria().verId(), r.verCategoria().verNombre(),
					r.verCategoria().verPuntaje(), r.verCategoria().obtenerTipo());
			listaRAEE.add(new RAEE(r.verNombre(), r.verDescripcion(), categoria, r.getIdentificador()));
		} 
		
		Visita visita = new Visita(visitaDto.getFecha(), visitaDto.getObservacion(), listaRSU, listaRAEE, orden);
		visita.getOrden().agregarVisita(visita);
		this.visitaDao.create(visita);
		this.ordenDAO.update(orden);
	}

	@Override
	public List<VisitaDTO> obtenerVisitas() throws BaseDeDatosException {
		List<Visita> visitas = visitaDao.findAll();
		List<VisitaDTO> visitasDTO = new ArrayList<>(0);
		for (Visita v : visitas)
			visitasDTO.add(new VisitaDTO(v.getFecha(), v.getObservacion()));
		return visitasDTO;
	}

	@Override
	public void registrarRecolector(String dni, String nombre, String apellido)
			throws BaseDeDatosException, DataEmptyException, DniInvalidException {
		Recolector recolector = new Recolector(dni, nombre, apellido);
		this.recolectorDao.create(recolector);
	}

	@Override
	public List<RecolectorDTO> obtenerRecolectores()
			throws BaseDeDatosException, DataEmptyException, DniInvalidException {
		List<Recolector> recolectores = recolectorDao.findAll();
		List<RecolectorDTO> recolectoresDTO = new ArrayList<>(0);
		for (Recolector r : recolectores)
			recolectoresDTO.add(new RecolectorDTO(r.getId(), r.getDni(), r.getNombre(), r.getApellido()));
		return recolectoresDTO;
	}

	public void eliminarRecolector(Long id) throws BaseDeDatosException {
		recolectorDao.remove(id);
	}

	public void modificarRecolector(Long id, String dni, String nombre, String apellido)
			throws BaseDeDatosException, DataEmptyException, DniInvalidException {
		recolectorDao.update(new Recolector(id, dni, nombre, apellido));
	}

	@Override
	public List<PedidoDTO> obtenerPedidos() throws BaseDeDatosException, DataEmptyException {
		List<Pedido> lisPedidos = this.pedidoDAO.findAll();
		List<PedidoDTO> lisPedidosDTO = new ArrayList<PedidoDTO>();
		for (Pedido pedido : lisPedidos) {
			lisPedidosDTO.add(new PedidoDTO(pedido.getFechaDeCreacion(), pedido.getPunto().getDireccion(),
					pedido.esCargaPesada(), pedido.getObservacion(), pedido.getId()));
		}
		return lisPedidosDTO;
	}

	@Override
	public List<PedidoDTO> obtenerPedidosSinOrden() throws BaseDeDatosException, DataEmptyException {
		List<Pedido> lisPedidos = this.pedidoDAO.findSinOrdenConPunto();
		List<PedidoDTO> lisPedidosDTO = new ArrayList<PedidoDTO>();
		for (Pedido pedido : lisPedidos) {
			lisPedidosDTO.add(new PedidoDTO(pedido.getFechaDeCreacion(), pedido.getPunto().getDireccion(),
					pedido.esCargaPesada(), pedido.getObservacion(), pedido.getId()));
		}
		return lisPedidosDTO;
	}

	public List<OrdenDTO> obtenerOrdenes() throws BaseDeDatosException, DataEmptyException, DniInvalidException {
		List<Orden> lisOrdenes = this.ordenDAO.findAllConRecolectoresYDireccionDePunto();
		List<OrdenDTO> listOrdenesDTO = new ArrayList<OrdenDTO>();
		for (Orden orden : lisOrdenes) {
			Recolector re = orden.getRecolector();
			Pedido pedido = orden.getPedido();
			listOrdenesDTO.add(new OrdenDTO(orden.getId_orden(), orden.getFecha(), orden.getEstado(),
					new RecolectorDTO(re.getId(), re.getDni(), re.getNombre(), re.getApellido()),
					new PedidoDTO(pedido.getFechaDeCreacion(), pedido.getPunto().getDireccion(), pedido.esCargaPesada(),
							pedido.getObservacion(), pedido.getId())));
		}
		return listOrdenesDTO;
	}

	public void registrarOrden(Long idPedido, Long idRecolector) throws BaseDeDatosException, DataEmptyException {

		Pedido pedido = new Pedido(idPedido);
		Recolector recolector = new Recolector();
		// pedido.setId(idPedido);
		recolector.setId(idRecolector);

		Orden orden = new Orden(pedido, recolector);
		this.ordenDAO.create(orden);

	}

	public void finalizarOrden(Long idOrden) throws StateException, BaseDeDatosException {
		Orden orden = ordenDAO.find(Integer.valueOf(idOrden.intValue()));
		orden.finalizar();
		PuntoFijo puntoFijo = this.puntoDAO.findPuntoFijo(orden.getPedido().getPunto().getId());
		if (this.puntoDAO.findPuntoFijo(orden.getPedido().getPunto().getId()) != null) {
			puntoFijo.actualizarPorcentaje(0);
			this.puntoDAO.updatePuntoFijo(puntoFijo);
		}
		this.ordenDAO.update(orden);
	}

	public void cancelarOrden(Long idOrden) throws BaseDeDatosException, StateException {
		Orden orden = ordenDAO.find(Integer.valueOf(idOrden.intValue()));
		orden.cancelar();
		this.ordenDAO.update(orden);
	}

	public List<CategoriaDTO> obtenerCategorias() throws BaseDeDatosException, DataEmptyException {
		List<CategoriaDTO> listaCategoria = new ArrayList<CategoriaDTO>();
		List<Categoria> listado = this.categoriaDAO.findAll();
		for (Categoria categoria : listado) {
			listaCategoria.add(new CategoriaDTO(categoria.devolverIdCategoria(), categoria.devolverNombreCategoria(),
					categoria.devolverPuntajeCategoria(), categoria.obtenerTipo()));
		}
		return listaCategoria;
	}

	@Override
	public void registrarPedido(boolean cargaPesada, String observacion, List<ResiduoDTO> residuos, PuntoDTO punto)
			throws BaseDeDatosException, DataEmptyException {
		Punto puntoAux = new Punto();
		puntoAux.setId(punto.getId());
		List<Residuo> lisResiduos = new ArrayList<Residuo>();
		Categoria categoriaAux = new Categoria();
		for (ResiduoDTO residuo : residuos) {
			categoriaAux.asignarIDCategoria(residuo.verCategoria().verId());
			lisResiduos.add(new Residuo(residuo.verCantidad(), categoriaAux));
		}
		Pedido pedido = new Pedido(cargaPesada, observacion, puntoAux, lisResiduos);
		this.pedidoDAO.create(pedido);

	}

	@Override
	public void registrarPedido(boolean cargaPesada, PuntoFijoDTO puntoFijo)
			throws BaseDeDatosException, DataEmptyException {
		PuntoFijo puntoAux = new PuntoFijo(puntoFijo.getId(), puntoFijo.getPorcentajeOcupacion(), puntoFijo.getNivel());
		Pedido pedido = new Pedido(cargaPesada, "Pedido correspondiente a un Punto Fijo", puntoAux);
		this.pedidoDAO.create(pedido);

	}

	@Override
	public List<PuntoDTO> obtenerPuntosInteligentes() throws BaseDeDatosException {
		List<PuntoDTO> lisPuntosDto = new ArrayList<PuntoDTO>();
		List<Punto> lisPuntos = this.puntoDAO.findAllPuntoInteligente();

		for (Punto p : lisPuntos) {
			lisPuntosDto.add(new PuntoDTO(p.getId(), Punto.PuntoInteligente,
					p.nombreMapa() + " - " + p.getCalle() + " : " + p.getAltura()));
		}
		return lisPuntosDto;
	}

	@Override
	public List<PuntoFijoDTO> obtenerPuntosFijosNuevoPedido() throws BaseDeDatosException {

		List<PuntoFijo> lisPuntos = this.puntoDAO.findAllPuntoFijo();

		List<PuntoFijoDTO> lisPuntosFDTO = new ArrayList<PuntoFijoDTO>();
		for (PuntoFijo p : lisPuntos) {

			if (this.validarPuntoFijoParaNuevoPedido(p)) {
				PuntoFijoDTO paux = new PuntoFijoDTO(Punto.PuntoFijo, p.getId(), p.getTitulo(), p.getDescripcion(),
						p.nombreMapa() + " - " + p.getCalle() + " : " + p.getAltura(), p.getPorcentajeOcupacion(),
						p.getNivel());

				HashSet<Residuo> residuos = (HashSet<Residuo>) p.getResiduosAlojados();
				ArrayList<CategoriaDTO> categorias = new ArrayList<CategoriaDTO>();
				for (Residuo r : residuos) {
					categorias.add(new CategoriaDTO(r.verCategoria().devolverNombreCategoria()));
				}

				paux.setTipoDeResiduo(categorias);
				lisPuntosFDTO.add(paux);
			}
		}
		return lisPuntosFDTO;
	}

	private boolean validarPuntoFijoParaNuevoPedido(PuntoFijo punto) {

		try {
			List<Pedido> pedidosDelPunto = this.pedidoDAO.findAllByPuntofijo(punto);

			if (!pedidosDelPunto.isEmpty()) {
				for (Pedido p : pedidosDelPunto) {
					if (p.poseeOrdenGenerada()) {
						Orden ordenDelPedido = this.ordenDAO.findByPedido(p);
						if (ordenDelPedido.esEnEjecucion() || ordenDelPedido.esPendiente())
							return false;
					} else
						return false;
				}
			}

		} catch (BaseDeDatosException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return true;
	}

	@Override
	public List<VisitaDTO> obtenerVisitasPorOrdenId(Long id) throws BaseDeDatosException {
		List<Visita> visitas = visitaDao.findAllByOrdenId(id);
		List<VisitaDTO> visitasDTO = new ArrayList<>();
		for (Visita v : visitas)
			visitasDTO.add(new VisitaDTO(v.getId(), v.getFecha(), v.getObservacion()));
		return visitasDTO;
	}

	public void pedidoEstadoGenerada(Long idPedido) throws StateException, BaseDeDatosException {
		Pedido pedido = pedidoDAO.find(Integer.valueOf(idPedido.intValue()));
		pedido.ordenGenerada();
		this.pedidoDAO.update(pedido);
	}

	
	//TODO borrar de interfaz
	@Override
	public void registrarVisita(String observacion, Long id_orden)
			throws BaseDeDatosException, DataEmptyException, RegistroVisitaException {
		// TODO Auto-generated method stub
		
	}

}
