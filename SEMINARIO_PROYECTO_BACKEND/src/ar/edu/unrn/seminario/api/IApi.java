package ar.edu.unrn.seminario.api;

import java.util.List;

import ar.edu.unrn.seminario.dto.CategoriaDTO;
import ar.edu.unrn.seminario.dto.OrdenDTO;
import ar.edu.unrn.seminario.dto.PedidoDTO;
import ar.edu.unrn.seminario.dto.PuntoDTO;
import ar.edu.unrn.seminario.dto.PuntoFijoDTO;
import ar.edu.unrn.seminario.dto.RecolectorDTO;
import ar.edu.unrn.seminario.dto.ResiduoDTO;
import ar.edu.unrn.seminario.dto.RolDTO;
import ar.edu.unrn.seminario.dto.UsuarioDTO;
import ar.edu.unrn.seminario.dto.VisitaDTO;
import ar.edu.unrn.seminario.exception.BaseDeDatosException;
import ar.edu.unrn.seminario.exception.DataEmptyException;
import ar.edu.unrn.seminario.exception.DniInvalidException;
import ar.edu.unrn.seminario.exception.NotNullException;
import ar.edu.unrn.seminario.exception.RegistroVisitaException;
import ar.edu.unrn.seminario.exception.StateException;

public interface IApi {

	// -------USUARIO---------
	void registrarUsuario(String username, String password, String email, String nombre, Integer rol)
			throws NotNullException, DataEmptyException;

	public UsuarioDTO obtenerUsuario(String username);

	void eliminarUsuario(String username);

	public List<UsuarioDTO> obtenerUsuarios(); // recuperar todos los usuarios

	void activarUsuario(String username) throws StateException; // recuperar el objeto Usuario, implementar el
																// comportamiento de estado.

	void desactivarUsuario(String username) throws StateException; // recuperar el objeto Usuario, implementar el
																	// comportamiento de estado.
	// -------------------------------------------------------------------------------------

	// -------ROL---------
	public List<RolDTO> obtenerRoles();

	public List<RolDTO> obtenerRolesActivos();

	void guardarRol(Integer codigo, String descripcion, boolean estado) throws NotNullException, DataEmptyException;

	public RolDTO obtenerRolPorCodigo(Integer codigo); // recuperar el rol almacenado

	void activarRol(Integer codigo); // recuperar el objeto Rol, implementar el comportamiento de estado.

	void desactivarRol(Integer codigo); // recuperar el objeto Rol, imp
	// -------------------------------------------------------------------------------------

	// -------VISITA---------
	void registrarVisita(String observacion, Long id_orden)
			throws BaseDeDatosException, DataEmptyException, RegistroVisitaException;

	public List<VisitaDTO> obtenerVisitas() throws BaseDeDatosException;

	public List<VisitaDTO> obtenerVisitasPorOrdenId(Long id) throws BaseDeDatosException;
	// -------------------------------------------------------------------------------------

	// -------VISITA---------
	void registrarRecolector(String dni, String nombre, String apellido)
			throws BaseDeDatosException, DataEmptyException, DniInvalidException;

	public List<RecolectorDTO> obtenerRecolectores()
			throws BaseDeDatosException, DataEmptyException, DniInvalidException;

	void eliminarRecolector(Long id) throws BaseDeDatosException;

	void modificarRecolector(Long id, String dni, String nombre, String apellido)
			throws BaseDeDatosException, DataEmptyException, DniInvalidException;
	// -------------------------------------------------------------------------------------

	// -------PEDIDO---------
	public List<PedidoDTO> obtenerPedidos() throws BaseDeDatosException, DataEmptyException;

	void registrarPedido(boolean cargaPesada, String observacion, List<ResiduoDTO> residuos, PuntoDTO punto)
			throws BaseDeDatosException, DataEmptyException;

	void pedidoEstadoGenerada(Long idPedido) throws StateException, BaseDeDatosException;

	public List<PedidoDTO> obtenerPedidosSinOrden() throws BaseDeDatosException, DataEmptyException;

	void registrarPedido(boolean cargaPesada, PuntoFijoDTO punto) throws BaseDeDatosException, DataEmptyException;

	// -------------------------------------------------------------------------------------

	// -------ORDEN---------
	public List<OrdenDTO> obtenerOrdenes() throws BaseDeDatosException, DataEmptyException, DniInvalidException;

	void registrarOrden(Long idPedido, Long idRecolector) throws BaseDeDatosException, DataEmptyException;

	void finalizarOrden(Long idOrden) throws StateException, BaseDeDatosException;

	void cancelarOrden(Long idOrden) throws BaseDeDatosException, StateException;
	// -------------------------------------------------------------------------------------

	// -------CATEGORIA---------
	public List<CategoriaDTO> obtenerCategorias() throws BaseDeDatosException, DataEmptyException;
	// -------------------------------------------------------------------------------------

	// -------PUNTO---------
	public List<PuntoDTO> obtenerPuntosInteligentes() throws BaseDeDatosException;

	public List<PuntoFijoDTO> obtenerPuntosFijosNuevoPedido() throws BaseDeDatosException;
	// -------------------------------------------------------------------------------------

	void registrarVisita(VisitaDTO visitaDto, Long id_orden) throws BaseDeDatosException, DataEmptyException, RegistroVisitaException;

}
