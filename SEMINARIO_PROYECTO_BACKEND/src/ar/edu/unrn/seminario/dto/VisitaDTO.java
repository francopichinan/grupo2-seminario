package ar.edu.unrn.seminario.dto;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import ar.edu.unrn.seminario.exception.DataEmptyException;

public class VisitaDTO {
	private Long id;
	private LocalDate fecha;
	private String observacion;
	private List<ResiduoDTO> residuos = new ArrayList<ResiduoDTO>();
	private List<RAEEDTO> residuosRAEE = new ArrayList<RAEEDTO>();
	private List<RSUDTO> residuosRSU = new ArrayList<RSUDTO>();

	public VisitaDTO(LocalDate fecha, String observacion) {
		this.fecha = fecha;
		this.observacion = observacion;
	}

	public VisitaDTO(Long id, LocalDate fecha, String observacion) {
		this.id = id;
		this.fecha = fecha;
		this.observacion = observacion;
	}

	public VisitaDTO(Long id, LocalDate fecha, String observacion, List<RAEEDTO> residuosRAEE,
			List<RSUDTO> residuosRSU) {
		this.id = id;
		this.fecha = fecha;
		this.observacion = observacion;
		this.residuosRAEE = residuosRAEE;
		this.residuosRSU = residuosRSU;
	}

	public VisitaDTO(LocalDate fecha, String observacion, List<RAEEDTO> residuosRAEE, List<RSUDTO> residuosRSU)
			throws DataEmptyException {

		if (fecha == null)
			throw new DataEmptyException("Se debe seleccionar una Fecha");
		if (observacion.isEmpty())
			throw new DataEmptyException("Se debe ingresar una observaci�n de la visita");
		if (residuosRAEE == null || residuosRSU == null)
			throw new DataEmptyException("Se deben ingresar el listado de residuos retirados");
		this.fecha = fecha;
		this.observacion = observacion;
		this.residuosRAEE = residuosRAEE;
		this.residuosRSU = residuosRSU;
	}

	public VisitaDTO(Long id, LocalDate fecha, String observacion, List<ResiduoDTO> listaResiduos) {
		this.id = id;
		this.fecha = fecha;
		this.observacion = observacion;
		this.residuos = listaResiduos;
	}

	public LocalDate getFecha() {
		return fecha;
	}

	public void setFecha(LocalDate fecha) {
		this.fecha = fecha;
	}

	public String getObservacion() {
		return observacion;
	}

	public void setObservacion(String observacion) {
		this.observacion = observacion;
	} 

	public Long getId() {
		return this.id;
	}

	public List<ResiduoDTO> getResiduos() {
		return residuos;
	}

	public List<RAEEDTO> obtenerResiduosRAEE() {
		return residuosRAEE;
	}

	public List<RSUDTO> obtenerResiduosRSU() {
		return residuosRSU;
	}
}
