package ar.edu.unrn.seminario.dto;

public class RecolectorDTO {
	private Long id;
	private String dni;
	private String nombre;
	private String apellido;

	public RecolectorDTO(Long id, String dni, String nombre, String apellido) {
		this.id = id;
		this.dni = dni;
		this.nombre = nombre;
		this.apellido = apellido;
	}

	public Long getId() {
		return id;
	}

	public String getDni() {
		return dni;
	}

	public void setDni(String dni) {
		this.dni = dni;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getApellido() {
		return apellido;
	}

	public void setApellido(String apellido) {
		this.apellido = apellido;
	}

	@Override
	public String toString() {
		return apellido + ", " + nombre + " - " + dni;
	}
}
