package ar.edu.unrn.seminario.dto;

import java.util.ArrayList;

public class PuntoFijoDTO {
	private String tipo;
	private Long id;
	private String titulo;
	private String descripcion;
	private String direccion; // mapa+calle+altura
	private ArrayList<CategoriaDTO> residuosAlojados;
	private float porcentajeOcupacion;
	private Integer nivel;

	public PuntoFijoDTO(String tipo, Long id, String titulo, String descripcion, String direccion,
			float porcentajeOcupacion, Integer nivel) {
		this.setTipo(tipo);
		this.setDescripcion(descripcion);
		this.setDireccion(direccion);
		this.setId(id);
		this.setTitulo(titulo);
		this.porcentajeOcupacion = porcentajeOcupacion;
		this.nivel = nivel;
	}

	public PuntoFijoDTO(Long id, String tipo, String direccion) {
		this.id = id;
		this.tipo = tipo;
		this.direccion = direccion;
	}

	public PuntoFijoDTO(Long id, String tipo, String direccion, float porcentajeOcupacion, Integer nivel) {
		this.setTipo(tipo);
		this.setDescripcion(descripcion);
		this.setDireccion(direccion);
		this.setId(id);
		this.porcentajeOcupacion = porcentajeOcupacion;
		this.nivel = nivel;
	}

	public String getTipo() {
		return tipo;
	}

	public void setTipo(String tipo) {
		this.tipo = tipo;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getTitulo() {
		return titulo;
	}

	public void setTitulo(String titulo) {
		this.titulo = titulo;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public String getDireccion() {
		return direccion;
	}

	public void setDireccion(String direccion) {
		this.direccion = direccion;
	}

	public float getPorcentajeOcupacion() {
		return this.porcentajeOcupacion;
	}

	public ArrayList<CategoriaDTO> getTipoDeResiduo() {
		return this.residuosAlojados;
	}

	public void setTipoDeResiduo(ArrayList<CategoriaDTO> categorias) {
		this.residuosAlojados = categorias;
	}

	public Integer getNivel() {
		return this.nivel;
	}
}
