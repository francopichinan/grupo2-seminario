package ar.edu.unrn.seminario.dto;

public class ResiduoDTO {
	private int cantidad;
	private CategoriaDTO categoriaResiduo;
	private String nombre;
	private String descripcion;

	public ResiduoDTO(int cantidad, CategoriaDTO unaCategoria) throws NumberFormatException {
		if (cantidad <= 0) {
			throw new NumberFormatException("El valor debe ser positivo.");
		}
		this.cantidad = cantidad;
		this.categoriaResiduo = unaCategoria;
	}

	public ResiduoDTO(String nombre, String descripcion, CategoriaDTO unaCategoria) {
		this.nombre = nombre;
		this.descripcion = descripcion;
		this.categoriaResiduo = unaCategoria;
	}

	public int verCantidad() {
		return this.cantidad;
	}

	public void asignarCategoria(CategoriaDTO unaCategoria) {
		this.categoriaResiduo = unaCategoria;
	}

	public CategoriaDTO verCategoria() {
		return this.categoriaResiduo;
	}

	public String getNombre() {
		return nombre;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public String getDescripcion() {
		return descripcion;
	}
}
