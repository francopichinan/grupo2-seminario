package ar.edu.unrn.seminario.dto;

import ar.edu.unrn.seminario.exception.DataEmptyException;

public class RAEEDTO {
	private String identificador;
	private String nombre;
	private String descripcion;
	private CategoriaDTO categoriaResiduo;
	private int cantidad;

	public RAEEDTO(String nombre, String descripcion, CategoriaDTO unaCategoria) {
		this.nombre = nombre;
		this.descripcion = descripcion;
		this.categoriaResiduo = unaCategoria;
	}

	public RAEEDTO(String identificador, String nombre, String descripcion, CategoriaDTO unaCategoria)
			throws DataEmptyException {
		if (identificador.isEmpty())
			throw new DataEmptyException("El campo 'Identificador' no puede estar vac�o.");
		if (nombre.isEmpty())
			throw new DataEmptyException("El campo 'Nombre' no puede estar vac�o.");
		if (descripcion.isEmpty())
			throw new DataEmptyException("El campo 'Descripcion' no puede estar vac�o.");
		if (unaCategoria == null)
			throw new DataEmptyException("Se debe seleccionar una categoria.");
		this.identificador = identificador;
		this.nombre = nombre;
		this.descripcion = descripcion;
		this.categoriaResiduo = unaCategoria;
	} 

	public CategoriaDTO verCategoria() {
		return this.categoriaResiduo;
	}

	public int verCantidad() {
		return this.cantidad;
	}

	public String verNombre() {
		return this.nombre;
	}

	public String verDescripcion() {
		return this.descripcion;
	}

	public String getIdentificador() {
		return identificador;
	}
}
