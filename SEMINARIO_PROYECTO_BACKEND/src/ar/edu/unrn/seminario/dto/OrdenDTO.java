package ar.edu.unrn.seminario.dto;

import java.time.LocalDate;

public class OrdenDTO {
	private Long id_orden;
	private LocalDate fecha;
	private String estado;
	private RecolectorDTO recolector;
	private PedidoDTO pedido;

	public OrdenDTO(Long id_orden, LocalDate fecha, String estado, RecolectorDTO recolector, PedidoDTO pedido) {
		this.id_orden = id_orden;
		this.fecha = fecha;
		this.estado = estado;
		this.recolector = recolector;
		this.pedido = pedido;
	}

	public Long getId_orden() {
		return id_orden;
	}

	public LocalDate getFecha() {
		return fecha;
	}

	public String getEstado() {
		return estado;
	}

	public RecolectorDTO getRecolector() {
		return recolector;
	}

	public void setRecolector(RecolectorDTO recolector) {
		this.recolector = recolector;
	}

	public PedidoDTO getPedido() {
		return pedido;
	}

	public void setPedido(PedidoDTO pedido) {
		this.pedido = pedido;
	}
}
