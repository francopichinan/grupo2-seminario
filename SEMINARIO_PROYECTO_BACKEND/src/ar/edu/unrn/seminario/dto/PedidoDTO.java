package ar.edu.unrn.seminario.dto;

import java.time.LocalDate;

public class PedidoDTO {
	private LocalDate fecha;
	private String direccion;
	private Boolean cargaPesada;
	private String observacion;
	private Long idPedido;
	
	public PedidoDTO(LocalDate fecha, String direccion, Boolean cargaPesada, String observacion, Long idPedido) {
		this.idPedido=idPedido;
		this.fecha=fecha;
		this.cargaPesada=cargaPesada;
		this.direccion=direccion;
		this.observacion=observacion;
	}

	public LocalDate getFecha() {
		return fecha;
	}

	public String getDireccion() {
		return direccion;
	}

	public Boolean getCargaPesada() {
		return cargaPesada;
	}

	public String getObservacion() {
		return observacion;
	}

	public Long getIdPedido() {
		return idPedido;
	}
}
