package ar.edu.unrn.seminario.dto;

public class PuntoDTO {
	private String tipo;
	private Long id;
	private String titulo;
	private String descripcion;
	private String direccion; // mapa+calle+altura

	public PuntoDTO(String tipo, Long id, String titulo, String descripcion, String direccion) {
		this.setTipo(tipo);
		this.setDescripcion(descripcion);
		this.setDireccion(direccion);
		this.setId(id);
	}

	public PuntoDTO(Long id,String tipo, String direccion) {
		this.id=id;
		this.tipo=tipo;
		this.direccion=direccion;
	}
	public String getTipo() {
		return tipo;
	}

	public void setTipo(String tipo) {
		this.tipo = tipo;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getTitulo() {
		return titulo;
	}

	public void setTitulo(String titulo) {
		this.titulo = titulo;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public String getDireccion() {
		return direccion;
	}

	public void setDireccion(String direccion) {
		this.direccion = direccion;
	}
	
	public String toString() {
		return direccion;
	}
	
}
