package ar.edu.unrn.seminario.dto;

public class CategoriaDTO {
	private Long id;
	private String nombre;
	private int puntaje;
	private String tipo;
	private static String RSU = "RSU";
	private static String RAEE = "RAEE";

	public CategoriaDTO(Long id, String nombre, int puntaje) {
		this.id = id;
		this.nombre = nombre;
		this.puntaje = puntaje;
	}

	public CategoriaDTO(Long id, String nombre, int puntaje, String tipo) {
		this.id = id;
		this.nombre = nombre;
		this.puntaje = puntaje;
		this.tipo = tipo;
	}

	public CategoriaDTO(String nombre) {
		this.nombre = nombre;
		this.tipo = "";
	}

	public String toString() {
		return (this.nombre);
	}

	public Long verId() {
		return this.id;
	}

	public boolean esRSU() {
		if (this.tipo.equals(CategoriaDTO.RSU))
			return true;
		return false;
	}

	public boolean esRAEE() {
		if (this.tipo.equals(CategoriaDTO.RAEE))
			return true;
		return false;
	}

	public String verNombre() {
		return this.nombre;
	}

	public int verPuntaje() {
		return puntaje;
	}

	public String obtenerTipo() {
		return this.tipo;
	}
}
