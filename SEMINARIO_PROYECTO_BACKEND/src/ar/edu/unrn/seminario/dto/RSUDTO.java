package ar.edu.unrn.seminario.dto;

import ar.edu.unrn.seminario.exception.DataEmptyException;

public class RSUDTO {
	private String nombre;
	private String descripcion;
	private CategoriaDTO categoriaResiduo;
	private int cantidad;

	private float peso;
	private int valorEnPuntosPorKg;
 
	public RSUDTO(String nombre, String descripcion, CategoriaDTO unaCategoria, float peso, int puntosPorKg) {
		this.nombre = nombre;
		this.descripcion = descripcion;
		this.categoriaResiduo = unaCategoria;
		this.peso = peso;
		this.valorEnPuntosPorKg = puntosPorKg;
	}

	public RSUDTO(String nombre, String descripcion, CategoriaDTO unaCategoria, float peso) throws DataEmptyException {
		if (nombre.isEmpty())
			throw new DataEmptyException("El campo 'Nombre' no puede estar vac�o.");
		if (descripcion.isEmpty())
			throw new DataEmptyException("El campo 'Descripcion' no puede estar vac�o.");
		if (unaCategoria == null)
			throw new DataEmptyException("Se debe seleccionar una categoria.");

		this.nombre = nombre;
		this.descripcion = descripcion;
		this.categoriaResiduo = unaCategoria;
		this.peso = peso;
	}

	public CategoriaDTO verCategoria() {
		return this.categoriaResiduo;
	}

	public int verCantidad() {
		return this.cantidad;
	}

	public String verNombre() {
		return this.nombre;
	}

	public String verDescripcion() {
		return this.descripcion;
	}

	public float devolverPesoRSU() {
		return this.peso;
	}

	public int devolverPuntosPorKgRSU() {
		return this.valorEnPuntosPorKg;
	}
}
