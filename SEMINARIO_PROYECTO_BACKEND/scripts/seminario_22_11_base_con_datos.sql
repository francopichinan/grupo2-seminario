-- phpMyAdmin SQL Dump
-- version 4.9.0.1
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 23-11-2022 a las 01:09:01
-- Versión del servidor: 10.4.6-MariaDB
-- Versión de PHP: 7.3.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `seminario`
--
CREATE DATABASE IF NOT EXISTS `seminario` DEFAULT CHARACTER SET latin1 COLLATE latin1_swedish_ci;
USE `seminario`;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `beneficio`
--

CREATE TABLE `beneficio` (
  `id_beneficio` int(11) NOT NULL,
  `nombre` varchar(255) NOT NULL,
  `descripcion` varchar(255) NOT NULL,
  `activado` tinyint(1) NOT NULL,
  `costodelbeneficio` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `canje`
--

CREATE TABLE `canje` (
  `id_canje` int(11) NOT NULL,
  `nombre` varchar(32) NOT NULL,
  `descripcion` varchar(256) DEFAULT NULL,
  `fecha` datetime NOT NULL,
  `puntos_gastados` int(11) NOT NULL,
  `id_ciudadano` int(11) NOT NULL,
  `id_beneficio` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `catalogo`
--

CREATE TABLE `catalogo` (
  `id_catalogo` int(11) NOT NULL,
  `nombre` varchar(255) NOT NULL,
  `temporada` varchar(255) NOT NULL,
  `estado` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `catalogo_beneficio`
--

CREATE TABLE `catalogo_beneficio` (
  `id_catalogo` int(11) NOT NULL,
  `id_beneficio` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `categoria`
--

CREATE TABLE `categoria` (
  `id_categoria` int(11) NOT NULL,
  `nombre` varchar(255) NOT NULL,
  `puntaje` int(11) NOT NULL,
  `tipo` varchar(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `categoria`
--

INSERT INTO `categoria` (`id_categoria`, `nombre`, `puntaje`, `tipo`) VALUES
(1, 'papel', 12, 'RSU'),
(2, 'carton', 30, 'RSU'),
(3, 'Electronica', 100, 'RAEE');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `ciudadano`
--

CREATE TABLE `ciudadano` (
  `id_ciudadano` int(11) NOT NULL,
  `dni` varchar(10) NOT NULL,
  `nombre` varchar(32) NOT NULL,
  `apellido` varchar(32) NOT NULL,
  `fecha_nacimiento` date NOT NULL,
  `puntos` int(11) NOT NULL,
  `email` varchar(45) NOT NULL,
  `id_usuario` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `ciudadano`
--

INSERT INTO `ciudadano` (`id_ciudadano`, `dni`, `nombre`, `apellido`, `fecha_nacimiento`, `puntos`, `email`, `id_usuario`) VALUES
(2, '38465210', 'Franco', 'Smith', '1995-11-01', 0, 'frankie@gmail.com', 10),
(3, '32567851', 'Laura', 'Lopez', '1995-11-01', 0, 'lauralopez@outlook.com', 11),
(4, '46893452', 'Yoana', 'Cabrera', '1995-11-01', 0, 'yoanac@hotmail.com', 12),
(5, '42354591', 'Rodrigo', 'Lopez', '1995-11-01', 0, 'rodrilop@gmail.com', 13),
(6, '34652791', 'Daniel', 'Gomez', '1995-11-01', 0, 'dani@gmail.com', 14),
(7, '61348537', 'Jisela', 'Ramirez', '1995-11-01', 0, 'jiselaram@gmail.com', 15),
(8, '453324122', 'Juan', 'Granizo', '1995-11-01', 0, 'juan@hotmail.com', 16),
(9, '431353174', 'Luciana', 'Lucero', '1995-11-01', 0, 'luciana15@gmail.com', 17),
(10, '23164231', 'Viviana', 'Gutierres', '1995-11-01', 0, 'vivigut@gmail.com', 18),
(11, '45632124', 'Francis', 'Jimenez', '1995-11-01', 0, 'francisjim@outlook.com', 19),
(12, '45715683', 'Omar', 'Duran', '1995-11-01', 0, 'omarduran@gmail.com', 20);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `coordenada`
--

CREATE TABLE `coordenada` (
  `id_coordenada` int(11) NOT NULL,
  `latitud_id` float NOT NULL DEFAULT 0,
  `longuitud_id` float NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `localidad`
--

CREATE TABLE `localidad` (
  `codigo_postal_id` int(11) NOT NULL,
  `nombre` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `localidad`
--

INSERT INTO `localidad` (`codigo_postal_id`, `nombre`) VALUES
(8500, 'Viedma'),
(8504, 'Patagones');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `mapa`
--

CREATE TABLE `mapa` (
  `id_mapa` int(11) NOT NULL,
  `localidad_id` int(11) NOT NULL,
  `nombre` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `mapa`
--

INSERT INTO `mapa` (`id_mapa`, `localidad_id`, `nombre`) VALUES
(1, 8500, 'Viedma'),
(2, 8504, 'Patagones');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `movimiento_punto`
--

CREATE TABLE `movimiento_punto` (
  `id_movimiento_punto` int(11) NOT NULL,
  `puntos` int(11) NOT NULL,
  `descripcion` varchar(256) DEFAULT NULL,
  `fecha` datetime NOT NULL,
  `id_responsable` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `ordenes`
--

CREATE TABLE `ordenes` (
  `id_orden` int(11) NOT NULL,
  `id_pedido` int(11) NOT NULL,
  `id_recolector` int(11) NOT NULL,
  `estado` text NOT NULL,
  `fecha` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `ordenes`
--

INSERT INTO `ordenes` (`id_orden`, `id_pedido`, `id_recolector`, `estado`, `fecha`) VALUES
(15, 9, 1, 'PENDIENTE', '2022-11-22'),
(16, 8, 3, 'PENDIENTE', '2022-11-22');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `pedidos`
--

CREATE TABLE `pedidos` (
  `id_pedido` int(11) NOT NULL,
  `fecha` date NOT NULL,
  `estado` text NOT NULL,
  `carga_pesada` tinyint(1) NOT NULL,
  `observacion` text NOT NULL,
  `id_punto` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `pedidos`
--

INSERT INTO `pedidos` (`id_pedido`, `fecha`, `estado`, `carga_pesada`, `observacion`, `id_punto`) VALUES
(8, '2022-11-22', 'ORDEN GENERADA', 1, 'Se requiere vehiculo', 3),
(9, '2022-11-22', 'ORDEN GENERADA', 0, 'No requiere vehiculo', 2),
(10, '2022-11-22', 'ORDEN NO GENERADA', 0, 'Para reciclar 4 teclados', 4),
(11, '2022-11-22', 'ORDEN NO GENERADA', 1, 'Fotocopias y 5 tablets', 1),
(12, '2022-11-22', 'ORDEN NO GENERADA', 0, 'Sin vehiculo', 3),
(13, '2022-11-22', 'ORDEN NO GENERADA', 0, 'Retirar carton', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `pedido_categoria`
--

CREATE TABLE `pedido_categoria` (
  `id_pedido_categoria` int(11) NOT NULL,
  `id_pedido` int(11) NOT NULL,
  `id_categoria` int(11) NOT NULL,
  `observacion` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `pedido_categoria`
--

INSERT INTO `pedido_categoria` (`id_pedido_categoria`, `id_pedido`, `id_categoria`, `observacion`) VALUES
(4, 8, 3, '3'),
(5, 8, 3, '10'),
(6, 8, 3, '1'),
(7, 9, 3, '5'),
(8, 9, 3, '23'),
(9, 10, 3, '4'),
(10, 11, 1, '5'),
(11, 11, 1, '100'),
(12, 12, 1, '5'),
(13, 13, 2, '33');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `punto`
--

CREATE TABLE `punto` (
  `id_punto` int(11) NOT NULL,
  `calle_id` varchar(111) NOT NULL,
  `altura_id` int(11) NOT NULL,
  `mapa_id` int(11) NOT NULL,
  `cordenada_id` int(11) NOT NULL,
  `titulo` varchar(50) NOT NULL,
  `descripcion` varchar(255) NOT NULL,
  `fecha` date NOT NULL,
  `tipo` varchar(50) NOT NULL,
  `estado` tinyint(1) NOT NULL,
  `porcentaje_ocupacion` float DEFAULT NULL,
  `nivel` int(11) DEFAULT NULL,
  `QR` blob DEFAULT NULL,
  `observacion` varchar(255) DEFAULT NULL,
  `id_responsable` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `punto`
--

INSERT INTO `punto` (`id_punto`, `calle_id`, `altura_id`, `mapa_id`, `cordenada_id`, `titulo`, `descripcion`, `fecha`, `tipo`, `estado`, `porcentaje_ocupacion`, `nivel`, `QR`, `observacion`, `id_responsable`) VALUES
(1, 'Los sauces', 19, 1, 1, 'Particular', 'un punto mas', '2022-11-11', 'Punto Inteligente', 1, NULL, NULL, NULL, NULL, 1),
(2, 'Sarmiento', 124, 1, 1, 'Particular', 'otra descripcion de punto 2', '2022-11-11', 'Punto Inteligente', 1, NULL, NULL, NULL, NULL, 1),
(3, 'Flores del campo', 3455, 2, 1, 'Particular', 'un punto mas 3', '2022-11-11', 'Punto Inteligente', 1, NULL, NULL, NULL, NULL, 2),
(4, 'Fresno', 134, 1, 1, 'Particular', 'otra descripcion de punto 4', '2022-11-11', 'Punto Inteligente', 1, NULL, NULL, NULL, NULL, 2),
(5, 'Universidad', 4, 2, 1, 'Publico', 'Esto es un punto Fijo', '2022-11-15', 'Punto Fijo', 1, 100, NULL, NULL, NULL, 2),
(6, 'Puente nuevo', 456, 1, 1, 'Publico', 'Esto es un punto Fijo', '2022-11-15', 'Punto Fijo', 1, 90, NULL, NULL, NULL, 2),
(7, 'Escuela', 123, 1, 1, 'Publico', 'Esto es un punto Fijo', '2022-11-15', 'Punto Fijo', 1, 90, NULL, NULL, NULL, 2),
(8, 'Hospital', 678, 1, 1, 'Publico', 'Esto es un punto Fijo', '2022-11-22', 'Punto Fijo', 1, 80, NULL, NULL, NULL, 2),
(9, 'Campana de botellas', 333, 1, 1, 'Publico', 'Esto es un punto Fijo', '2022-11-22', 'Punto Fijo', 1, 65, NULL, NULL, NULL, 2);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `punto_tipo_residuo`
--

CREATE TABLE `punto_tipo_residuo` (
  `id_punto_categoria` int(11) NOT NULL,
  `tipo_id` int(11) NOT NULL,
  `punto_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `punto_tipo_residuo`
--

INSERT INTO `punto_tipo_residuo` (`id_punto_categoria`, `tipo_id`, `punto_id`) VALUES
(1, 1, 5),
(5, 1, 6),
(2, 2, 6);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `raee`
--

CREATE TABLE `raee` (
  `identificador` varchar(255) NOT NULL,
  `nombre` varchar(255) NOT NULL,
  `descripcion` varchar(255) NOT NULL,
  `id_categoria` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `recolectores`
--

CREATE TABLE `recolectores` (
  `id_recolector` int(11) NOT NULL,
  `dni` varchar(8) NOT NULL,
  `nombre` varchar(32) NOT NULL,
  `apellido` varchar(32) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `recolectores`
--

INSERT INTO `recolectores` (`id_recolector`, `dni`, `nombre`, `apellido`) VALUES
(1, '12121212', 'Martin', 'Perez'),
(2, '40404040', 'Gabriel', 'Baltazar'),
(3, '67676767', 'Eduardo', 'Sanchez');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `residuo_raee_visita`
--

CREATE TABLE `residuo_raee_visita` (
  `id_residuo_raee_visita` int(11) NOT NULL,
  `id_raee_fk` varchar(255) CHARACTER SET utf8 NOT NULL,
  `id_visita_fk` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `residuo_rsu_visita`
--

CREATE TABLE `residuo_rsu_visita` (
  `id_residuo_rsu_visita` int(11) NOT NULL,
  `id_visita_fk` int(11) NOT NULL,
  `id_rsu_fk` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `responsable`
--

CREATE TABLE `responsable` (
  `id_responsable` int(11) NOT NULL,
  `fecha_inicio` datetime NOT NULL,
  `fecha_fin` datetime DEFAULT NULL,
  `id_ciudadano` int(11) NOT NULL,
  `id_punto_inteligente` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `responsable`
--

INSERT INTO `responsable` (`id_responsable`, `fecha_inicio`, `fecha_fin`, `id_ciudadano`, `id_punto_inteligente`) VALUES
(1, '2022-11-12 00:33:19', '2022-11-30 21:33:19', 2, 1),
(2, '2022-11-12 00:33:19', '2022-11-30 21:33:19', 10, 2);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `rol`
--

CREATE TABLE `rol` (
  `id_rol` int(11) NOT NULL,
  `nombre` varchar(16) NOT NULL,
  `descripcion` varchar(256) NOT NULL,
  `activado` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `rol`
--

INSERT INTO `rol` (`id_rol`, `nombre`, `descripcion`, `activado`) VALUES
(1, 'Ciudadano', 'Un Ciudadano puede canjear puntos por beneficios, notificar sobre puntos fijos y hacerse responsable sobre un punto inteligente', 1),
(2, 'Administrador', 'Gestiona el Sistema, consulta sobre Ciudadanos y Puntos. Banea Usuarios', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `rsu`
--

CREATE TABLE `rsu` (
  `id_rsu` int(11) NOT NULL,
  `nombre` varchar(255) NOT NULL,
  `descripcion` varchar(255) NOT NULL,
  `peso` float NOT NULL,
  `valorenpuntosporkg` int(11) NOT NULL,
  `id_categoria` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `usuario`
--

CREATE TABLE `usuario` (
  `id_usuario` int(11) NOT NULL,
  `username` varchar(24) NOT NULL,
  `password` varchar(8) NOT NULL,
  `activado` int(11) NOT NULL,
  `id_rol` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `usuario`
--

INSERT INTO `usuario` (`id_usuario`, `username`, `password`, `activado`, `id_rol`) VALUES
(10, 'frank', '1234', 1, 1),
(11, 'laura', '1234', 1, 1),
(12, 'yoana', '1234', 1, 1),
(13, 'rodri', '98764', 1, 1),
(14, 'dani', '5612', 1, 1),
(15, 'jise', '7831', 1, 1),
(16, 'juani', '1111', 1, 1),
(17, 'lucif', '23541356', 1, 1),
(18, 'vivi', '12345678', 1, 1),
(19, 'frankji', '12345678', 1, 1),
(20, 'omar', '12345678', 1, 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `visitas`
--

CREATE TABLE `visitas` (
  `id_visita` int(11) NOT NULL,
  `fecha` date NOT NULL,
  `observacion` varchar(100) NOT NULL,
  `id_orden` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `beneficio`
--
ALTER TABLE `beneficio`
  ADD PRIMARY KEY (`id_beneficio`);

--
-- Indices de la tabla `canje`
--
ALTER TABLE `canje`
  ADD PRIMARY KEY (`id_canje`),
  ADD KEY `id_ciudadano_fk_idx` (`id_ciudadano`),
  ADD KEY `id_beneficio_fk_canje_idx` (`id_beneficio`);

--
-- Indices de la tabla `catalogo`
--
ALTER TABLE `catalogo`
  ADD PRIMARY KEY (`id_catalogo`);

--
-- Indices de la tabla `catalogo_beneficio`
--
ALTER TABLE `catalogo_beneficio`
  ADD KEY `id_categoria` (`id_beneficio`),
  ADD KEY `id_catalogo` (`id_catalogo`);

--
-- Indices de la tabla `categoria`
--
ALTER TABLE `categoria`
  ADD PRIMARY KEY (`id_categoria`);

--
-- Indices de la tabla `ciudadano`
--
ALTER TABLE `ciudadano`
  ADD PRIMARY KEY (`id_ciudadano`),
  ADD UNIQUE KEY `dni_UNIQUE` (`dni`),
  ADD UNIQUE KEY `email_UNIQUE` (`email`),
  ADD KEY `id_usuario_fk_idx` (`id_usuario`);

--
-- Indices de la tabla `coordenada`
--
ALTER TABLE `coordenada`
  ADD PRIMARY KEY (`id_coordenada`);

--
-- Indices de la tabla `localidad`
--
ALTER TABLE `localidad`
  ADD PRIMARY KEY (`codigo_postal_id`);

--
-- Indices de la tabla `mapa`
--
ALTER TABLE `mapa`
  ADD PRIMARY KEY (`id_mapa`),
  ADD KEY `localidad_id` (`localidad_id`) USING BTREE;

--
-- Indices de la tabla `movimiento_punto`
--
ALTER TABLE `movimiento_punto`
  ADD PRIMARY KEY (`id_movimiento_punto`),
  ADD KEY `id_responsable_fk_idx` (`id_responsable`);

--
-- Indices de la tabla `ordenes`
--
ALTER TABLE `ordenes`
  ADD PRIMARY KEY (`id_orden`),
  ADD KEY `id_pedido_ord_fk` (`id_pedido`),
  ADD KEY `id_recolector_ord_fk` (`id_recolector`);

--
-- Indices de la tabla `pedidos`
--
ALTER TABLE `pedidos`
  ADD PRIMARY KEY (`id_pedido`),
  ADD KEY `id_punto_fk_pedidos_idx` (`id_punto`);

--
-- Indices de la tabla `pedido_categoria`
--
ALTER TABLE `pedido_categoria`
  ADD PRIMARY KEY (`id_pedido_categoria`),
  ADD KEY `id_pedido_fk_pc_idx` (`id_pedido`),
  ADD KEY `id_categoria_fk_pc_idx` (`id_categoria`);

--
-- Indices de la tabla `punto`
--
ALTER TABLE `punto`
  ADD PRIMARY KEY (`id_punto`);

--
-- Indices de la tabla `punto_tipo_residuo`
--
ALTER TABLE `punto_tipo_residuo`
  ADD PRIMARY KEY (`id_punto_categoria`),
  ADD UNIQUE KEY `UQ_punto_tipo` (`punto_id`,`tipo_id`),
  ADD KEY `id_categoria_fk_punto_categoria_idx` (`tipo_id`),
  ADD KEY `id_punto_fk_punto_categoria_idx` (`punto_id`);

--
-- Indices de la tabla `raee`
--
ALTER TABLE `raee`
  ADD PRIMARY KEY (`identificador`),
  ADD KEY `id_categoria_raee_fk` (`id_categoria`);

--
-- Indices de la tabla `recolectores`
--
ALTER TABLE `recolectores`
  ADD PRIMARY KEY (`id_recolector`),
  ADD UNIQUE KEY `dni` (`dni`);

--
-- Indices de la tabla `residuo_raee_visita`
--
ALTER TABLE `residuo_raee_visita`
  ADD PRIMARY KEY (`id_residuo_raee_visita`),
  ADD KEY `id_visita_raee_fk` (`id_visita_fk`),
  ADD KEY `id_raee_residuoraee_fk` (`id_raee_fk`);

--
-- Indices de la tabla `residuo_rsu_visita`
--
ALTER TABLE `residuo_rsu_visita`
  ADD PRIMARY KEY (`id_residuo_rsu_visita`),
  ADD KEY `id_visita_rsu_fk` (`id_visita_fk`),
  ADD KEY `id_rsu_residuorsu_fk` (`id_rsu_fk`);

--
-- Indices de la tabla `responsable`
--
ALTER TABLE `responsable`
  ADD PRIMARY KEY (`id_responsable`),
  ADD KEY `id_ciudadano_fk_idx` (`id_ciudadano`),
  ADD KEY `id_punto_fk_responsable_idx` (`id_punto_inteligente`);

--
-- Indices de la tabla `rol`
--
ALTER TABLE `rol`
  ADD PRIMARY KEY (`id_rol`),
  ADD UNIQUE KEY `nombre_UNIQUE` (`nombre`);

--
-- Indices de la tabla `rsu`
--
ALTER TABLE `rsu`
  ADD PRIMARY KEY (`id_rsu`),
  ADD KEY `id_categoria_fk` (`id_categoria`);

--
-- Indices de la tabla `usuario`
--
ALTER TABLE `usuario`
  ADD PRIMARY KEY (`id_usuario`),
  ADD UNIQUE KEY `username_UNIQUE` (`username`),
  ADD KEY `id_rol_idx` (`id_rol`);

--
-- Indices de la tabla `visitas`
--
ALTER TABLE `visitas`
  ADD PRIMARY KEY (`id_visita`),
  ADD KEY `id_orden_vis_fk` (`id_orden`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `beneficio`
--
ALTER TABLE `beneficio`
  MODIFY `id_beneficio` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `canje`
--
ALTER TABLE `canje`
  MODIFY `id_canje` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `catalogo`
--
ALTER TABLE `catalogo`
  MODIFY `id_catalogo` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `categoria`
--
ALTER TABLE `categoria`
  MODIFY `id_categoria` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT de la tabla `ciudadano`
--
ALTER TABLE `ciudadano`
  MODIFY `id_ciudadano` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT de la tabla `coordenada`
--
ALTER TABLE `coordenada`
  MODIFY `id_coordenada` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `localidad`
--
ALTER TABLE `localidad`
  MODIFY `codigo_postal_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8505;

--
-- AUTO_INCREMENT de la tabla `mapa`
--
ALTER TABLE `mapa`
  MODIFY `id_mapa` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT de la tabla `movimiento_punto`
--
ALTER TABLE `movimiento_punto`
  MODIFY `id_movimiento_punto` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `ordenes`
--
ALTER TABLE `ordenes`
  MODIFY `id_orden` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;

--
-- AUTO_INCREMENT de la tabla `pedidos`
--
ALTER TABLE `pedidos`
  MODIFY `id_pedido` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT de la tabla `pedido_categoria`
--
ALTER TABLE `pedido_categoria`
  MODIFY `id_pedido_categoria` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT de la tabla `punto`
--
ALTER TABLE `punto`
  MODIFY `id_punto` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT de la tabla `punto_tipo_residuo`
--
ALTER TABLE `punto_tipo_residuo`
  MODIFY `id_punto_categoria` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT de la tabla `recolectores`
--
ALTER TABLE `recolectores`
  MODIFY `id_recolector` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT de la tabla `residuo_raee_visita`
--
ALTER TABLE `residuo_raee_visita`
  MODIFY `id_residuo_raee_visita` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `residuo_rsu_visita`
--
ALTER TABLE `residuo_rsu_visita`
  MODIFY `id_residuo_rsu_visita` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `responsable`
--
ALTER TABLE `responsable`
  MODIFY `id_responsable` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT de la tabla `rol`
--
ALTER TABLE `rol`
  MODIFY `id_rol` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT de la tabla `rsu`
--
ALTER TABLE `rsu`
  MODIFY `id_rsu` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `usuario`
--
ALTER TABLE `usuario`
  MODIFY `id_usuario` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;

--
-- AUTO_INCREMENT de la tabla `visitas`
--
ALTER TABLE `visitas`
  MODIFY `id_visita` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `ciudadano`
--
ALTER TABLE `ciudadano`
  ADD CONSTRAINT `id_usuario_fk` FOREIGN KEY (`id_usuario`) REFERENCES `usuario` (`id_usuario`) ON DELETE CASCADE;

--
-- Filtros para la tabla `ordenes`
--
ALTER TABLE `ordenes`
  ADD CONSTRAINT `id_pedido_ord_fk` FOREIGN KEY (`id_pedido`) REFERENCES `pedidos` (`id_pedido`),
  ADD CONSTRAINT `id_recolector_ord_fk` FOREIGN KEY (`id_recolector`) REFERENCES `recolectores` (`id_recolector`);

--
-- Filtros para la tabla `pedidos`
--
ALTER TABLE `pedidos`
  ADD CONSTRAINT `id_punto_fk_pedidos` FOREIGN KEY (`id_punto`) REFERENCES `punto` (`id_punto`);

--
-- Filtros para la tabla `raee`
--
ALTER TABLE `raee`
  ADD CONSTRAINT `id_categoria_raee_fk` FOREIGN KEY (`id_categoria`) REFERENCES `categoria` (`id_categoria`);

--
-- Filtros para la tabla `residuo_raee_visita`
--
ALTER TABLE `residuo_raee_visita`
  ADD CONSTRAINT `id_raee_residuoraee_fk` FOREIGN KEY (`id_raee_fk`) REFERENCES `raee` (`identificador`),
  ADD CONSTRAINT `id_visita_raee_fk` FOREIGN KEY (`id_visita_fk`) REFERENCES `visitas` (`id_visita`);

--
-- Filtros para la tabla `residuo_rsu_visita`
--
ALTER TABLE `residuo_rsu_visita`
  ADD CONSTRAINT `id_rsu_residuorsu_fk` FOREIGN KEY (`id_rsu_fk`) REFERENCES `rsu` (`id_rsu`),
  ADD CONSTRAINT `id_visita_rsu_fk` FOREIGN KEY (`id_visita_fk`) REFERENCES `visitas` (`id_visita`);

--
-- Filtros para la tabla `responsable`
--
ALTER TABLE `responsable`
  ADD CONSTRAINT `id_ciud_fk` FOREIGN KEY (`id_ciudadano`) REFERENCES `ciudadano` (`id_ciudadano`),
  ADD CONSTRAINT `id_punto_fk_responsable` FOREIGN KEY (`id_punto_inteligente`) REFERENCES `punto` (`id_punto`);

--
-- Filtros para la tabla `rsu`
--
ALTER TABLE `rsu`
  ADD CONSTRAINT `id_categoria_fk` FOREIGN KEY (`id_categoria`) REFERENCES `categoria` (`id_categoria`);

--
-- Filtros para la tabla `usuario`
--
ALTER TABLE `usuario`
  ADD CONSTRAINT `id_rol` FOREIGN KEY (`id_rol`) REFERENCES `rol` (`id_rol`);

--
-- Filtros para la tabla `visitas`
--
ALTER TABLE `visitas`
  ADD CONSTRAINT `id_orden_vis_fk` FOREIGN KEY (`id_orden`) REFERENCES `ordenes` (`id_orden`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
